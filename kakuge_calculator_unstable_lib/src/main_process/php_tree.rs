use serde_json::{Value};
use super::json_util::get_value_string;

#[derive(Debug)]
pub enum PhpTree{
    Branch(String, Vec<PhpTree>),
    Leaf(String)
}

impl PhpTree{
    pub fn get_name(&self) -> String{
        match self{
            &PhpTree::Branch(ref name, _) => name.to_string(),
            &PhpTree::Leaf(ref name) => name.to_string()
        }
    }
}

pub fn convert_to_php_tree(name : &str, arg : &Value) -> PhpTree {

    //[a1 => [a1_1, a1_2 => [a1_2_1], a1_3 => a1_3_1]
    //key,valueをもつ場合と、valueしかない場合があり、phpの仕様によりその場合のkeyは数値となる。
    //[a1,a2]
    //key,valueを持つ要素がない場合、arrayとしてjsonコードされる。
    //抜粋:a1_2 => [a1_2_1], a1_3 => a1_3_1
    //value側に単一の要素しかない場合でも、arrayとしてくくってもよいし、生strで置いても良い。
    if let Some(v) = arg.as_object() {
        let mut vec: Vec<PhpTree> = vec![];
        for (key, value) in v {
            //キーが数字である。つまりleafである。
            if let Ok(_) = key.parse::<i32>() {
                if let Some(s) = get_value_string(value).ok() {
                    vec.push(PhpTree::Leaf(s.to_string()));
                } else {
                    println!("{}", arg);
                    println!("{} {}; {} must be string", name, value, value);
                }
            } else {
                vec.push(convert_to_php_tree(key, value));
            }
        }
        return PhpTree::Branch(name.to_string(), vec);
    } else if let Some(array) = arg.as_array() {
        //やることは全部数字Keyのオブジェクトと同じである。
        let mut vec: Vec<PhpTree> = vec![];
        for v in array {
            if let Some(s) = get_value_string(v).ok() {
                vec.push(PhpTree::Leaf(s.to_string()));
            } else {
                println!("{}", arg);
                println!("{} {}; {} must be string.", name, v, v);
            }
        }
        return PhpTree::Branch(name.to_string(), vec);
    } else if let Some(v) = get_value_string(arg).ok(){
        //arrayの要素数1と同じである。
        return PhpTree::Branch(name.to_string(), vec![PhpTree::Leaf(v.to_string())]);
    }
    unreachable!()
}