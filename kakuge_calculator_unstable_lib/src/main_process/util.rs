use linked_hash_map::LinkedHashMap;

pub fn get_top_tag_index(table : &Vec<Vec<String>>, tag : &str) -> Option<usize>{
    match table[0].iter().enumerate().skip(1).find(|&(_, s)| s == tag){
        Some((index, _)) => Some(index),
        None => None
    }
}

pub fn get_left_tag_index(table : &Vec<Vec<String>>, tag : &str) -> Option<usize>{
    for i in 1..table.len(){
        if table[i][0] == tag { return Some(i); }
    }
    return None;
}

pub fn del_col<T>(table : &mut Vec<Vec<T>>, index : usize){
    for i in 0..table.len(){
        table[i].remove(index);
    }
}

//(x,y)が返ってくるので、table[y][x]と入れて望みのものを見つける
pub fn get_indexes(table : &Vec<Vec<String>>, input : &LinkedHashMap<String, Vec<Vec<String>>>) -> Vec<(Vec<(usize, usize)>, String)> {
    let mut result: Vec<(Vec<(usize, usize)>, String)> = vec![];
    for (key, lines) in input {
        //let key = &*tuple.0;
        //let lines = &tuple.1;
        if let Some(x) = get_top_tag_index(table, &key) {
            for line in lines {
                let mut indexes: Vec<(usize, usize)> = vec![];
                for i in 0..(line.len() - 1) {
                    if let Some(y) = get_left_tag_index(table, &line[i]) {
                        indexes.push((x, y));
                    } else {
                        println!("{} is not found!", line[i]);
                    }
                }
                let value = &line[line.len() - 1];
                result.push((indexes, value.to_string()));
            }
        } else if let Some(y) = get_left_tag_index(table, &key) {
            for line in lines {
                let mut indexes: Vec<(usize, usize)> = vec![];
                for i in 0..(line.len() - 1) {
                    if let Some(x) = get_top_tag_index(table, &line[i]) {
                        indexes.push((x, y));
                    } else {
                        println!("{} is not found!", line[i]);
                    }
                }
                let value = &line[line.len() - 1];
                result.push((indexes, value.to_string()));
            }
        } else { println!("{} is not found!", key); }
    };
    result
}