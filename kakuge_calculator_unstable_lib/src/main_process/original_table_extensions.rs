use linked_hash_map::LinkedHashMap;
use super::util;

pub trait OriginalTableUtil{
    fn get_indexes(&self, input : &LinkedHashMap<String, Vec<Vec<String>>>) -> Vec<(Vec<(usize, usize)>, String)>;
}

impl OriginalTableUtil for Vec<Vec<String>> {
    fn get_indexes(&self, input : &LinkedHashMap<String, Vec<Vec<String>>>) -> Vec<(Vec<(usize, usize)>, String)>{
        util::get_indexes(self, input)
    }


}