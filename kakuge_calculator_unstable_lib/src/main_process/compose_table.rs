use super::table_structs::{TablePair, TableItem};
use std::collections::HashMap;

pub fn compose_table(exec_name : &str, arg : &Vec<(String, TablePair)>) -> TablePair {
    let mut hash: HashMap<String, TablePair> = HashMap::new();
    let mut main_key: Option<String> = None;
    for &(ref name, ref table_pair) in arg {
        let key = table_pair.original[0][0].to_string();
        if name == "main_table" {
            main_key = Some(key.to_string());
        }
        hash.insert(key, table_pair.to_copy());//to_ownedとかを使うとintelliJが誤警告してくる
    }

    if main_key.is_none() { panic!("{} : there's no main_table", exec_name); }
    let main_name = main_key.unwrap();
    let mut original = hash[&main_name].original.to_vec();
    let mut compute = hash[&main_name].compute.to_vec();
    //各tableを識別するためにつける番号。
    let mut iteration = append_numbers(&mut compute);

    let mut level_just_changed = true;
    let mut level = 1;
    loop {
        if find_and_compose(&mut original, &mut compute, &mut hash, level, &mut iteration) == false {
            if level_just_changed == false {
                level += 1;
                level_just_changed = true;
            } else {
                //レベルチェンジ直後になかったということは、探索し終わったということ。
                break;
            }
        } else { level_just_changed = false; }
    }
    TablePair { original, compute }
}

///何重に展開したかをlevelで表す。表に表をインサートするには"@表名"という要素を作る。その表をインサートするために展開した時に、その中にもさらに@表がある場合がある。
/// それは@@表、という形に書き直す。そして一段ずつ順番に展開していき、他の部分と整合させていく。
/// あるレベルの@表が見つからなかったら、次のレベルに行く。その次のレベルも見つからなかったら@表はもうないのでcompose終了である。
fn find_and_compose(original : &mut Vec<Vec<String>>, compute : &mut Vec<Vec<TableItem>>, hash : &mut HashMap<String, TablePair>, level : usize, iteration : &mut usize) -> bool{
    let y_len = compute.len();
    for y in 0..y_len{
        let x_len = compute[y].len();
        for x in 0..x_len{
            let count = count_at(&compute[y][x].sym);
            if count == level{
                let table_name = get_table_name(&compute[y][x].sym, level);

                let insert_table = hash.get_mut(&table_name).expect(&format!["table composition: {} was not found", table_name]);
                do_composition(original, compute, insert_table, x, y, level, iteration);
                return true
            }
        }
    }
    return false;
}

fn do_composition(original : &mut Vec<Vec<String>>, compute : &mut Vec<Vec<TableItem>>, insert_table : &mut TablePair, x : usize, y : usize, level : usize, iteration : &mut usize) {
    let (right, bottom) = find_rectangle(compute, x, y);
    let (rect_width, rect_height) = (right - x + 1, bottom - y + 1);
    let (insert_width, insert_height) = (insert_table.compute[0].len(), insert_table.compute.len());
    let insert_table_name = get_table_name(&compute[y][x].sym, level);

    //print(original, compute);
    if rect_height == 1 {
        expand_y(original, compute, &insert_table.original, y);
       // print(original, compute);
    } else if rect_height != insert_height{
        panic!("{}: table height is not valid", &insert_table_name);
    }

    if rect_width == 1 {
        expand_x(original, compute, &insert_table.original, x);
        //print(original, compute);
    } else if rect_width != insert_width{
        panic!("{}: table width is not valid", &insert_table_name);
    }
    sort_insert_table_y(original, insert_table, y, &insert_table_name);
    sort_insert_table_x(original, insert_table, x, &insert_table_name);
    insert(compute, &insert_table.compute, x, y, level, iteration);
}

fn insert(compute : &mut Vec<Vec<TableItem>>, ins_comp : &Vec<Vec<TableItem>>,  x : usize, y : usize, level : usize, iteration : &mut usize){
    let y_len = ins_comp.len();
    for i in 0..y_len{
        let x_len = ins_comp[0].len();
        for k in 0..x_len{
            compute[y+i][x+k] = convert(&ins_comp[i][k], level, iteration);
        }
    }
}

fn convert(item : &TableItem, level : usize, iteration : &mut usize) -> TableItem{
    if item.sym.starts_with("@"){
        let mut sym = "@".repeat(level - 1) + &item.sym;
        append_number(&mut sym, *iteration);
        *iteration += 1;
        return TableItem{
            sym,
            num : item.num
        }
    }
    else{ return item.clone(); }
}

///insertできるように領域を広げる
fn expand_y(original : &mut Vec<Vec<String>>, compute : &mut Vec<Vec<TableItem>>, ins_ori : &Vec<Vec<String>>, y : usize){
    let mut index = ins_ori.len() - 1;
    loop{
        let mut y= y;
        if index != 1 {
            //一個ずつコピーしてはタグをつけくわえていくが、最後はコピーしない
            copy_y(original, compute, y);
            y += 1;
        }
        original[y+1][0] += "|";
        original[y+1][0] += &ins_ori[index][0];
        index -= 1;
        if index == 0 { break; }
    }
}


fn copy_y(original : &mut Vec<Vec<String>>, compute : &mut Vec<Vec<TableItem>>, y : usize){
    //let new : Vec<String> = original[y+1].iter().map(|s|s.to_string()).collect();
    let new = original[y+1].to_vec();
    original.insert(y+1, new);
    let new = compute[y].to_vec();
    compute.insert(y, new);
}

fn expand_x(original : &mut Vec<Vec<String>>, compute : &mut Vec<Vec<TableItem>>, ins_ori : &Vec<Vec<String>>, x : usize){
    let mut index = ins_ori[0].len() - 1;
    loop{
        let mut x= x;
        if index != 1 {
            //一個ずつコピーしてはタグをつけくわえていくが、最後はコピーしない
            copy_x(original, compute, x);
            x += 1;
        }
        original[0][x+1] += "|";
        original[0][x+1] += &ins_ori[0][index];
        index -= 1;
        if index == 0 { break; }
    }
}


fn copy_x(original : &mut Vec<Vec<String>>, compute : &mut Vec<Vec<TableItem>>, x : usize) {
    let new = original[0][x+1].to_string();
    original[0].insert(x+1, new);
    let len = compute.len();
    for i in 0..len{
        let new = original[i+1][x+1].to_string();
        original[i+1].insert(x+1,new);
        let new = compute[i][x].clone();
        compute[i].insert(x,new);
    }
}



///展開後のtag名は hoge|hugaという形で'|'を挟んで展開したタグ名をくっつける。同名のタグを探してその内容をインサートしていくわけだが、
/// 順番が違う場合は当然入れ替えてやる必要があるし、同名のタグが見つからなければpanicする
fn sort_insert_table_y(original : &Vec<Vec<String>>, insert_table : &mut TablePair, y : usize, table_name : &str){
    let ins_comp = &mut insert_table.compute;
    let ins_ori = &mut insert_table.original;

    let len = ins_comp.len();
    'outer: for i in 0 ..len{
        let tag = get_last_level_tag(&original[i + y + 1][0]);
        for k in 0..len{
            if tag.eq(&ins_ori[k+1][0]){

                if i != k {
                    swap_y(ins_ori, ins_comp, i, k);
                }
                continue 'outer;
            }
        }
        panic!(format!("couldn't find {} tag in {}",tag, table_name))
    }
}

fn swap_y(ori : &mut Vec<Vec<String>>, comp : &mut Vec<Vec<TableItem>>, i : usize, k : usize){
    ori.swap(i + 1, k + 1);
    comp.swap(i,k)
}

fn sort_insert_table_x(original : &Vec<Vec<String>>, insert_table : &mut TablePair, x : usize, table_name : &str){
    let ins_comp = &mut insert_table.compute;
    let ins_ori = &mut insert_table.original;

    let len = ins_comp[0].len();
    'outer: for i in 0 ..len{
        let tag = get_last_level_tag(&original[0][i+x+1]);
        for k in 0..len{
            if tag.eq(&ins_ori[0][k+1]){
                if i != k {
                    swap_x(ins_ori, ins_comp, i, k);
                }
                continue 'outer;
            }
        }
        panic!(format!("couldn't find {} tag in {}",tag, table_name))
    }
}

///xの入れ替えは少し大変である
fn swap_x(ori : &mut Vec<Vec<String>>, comp : &mut Vec<Vec<TableItem>>, i : usize, k : usize){
    ori[0].swap(i+1,k+1);
    let len = comp.len();
    for i in 0..len {
        ori[i+1].swap(i + 1, k + 1);
        comp[i].swap(i, k)
    }
}

fn get_last_level_tag(full : &str) -> &str{
    let start = match full.rfind('|'){
        Some(i) => i + 1,
        None => 0
    };
    return &full[start..];
}

///同じ@表がどの範囲に広がっているかを確かめる
fn find_rectangle(compute : &Vec<Vec<TableItem>>, x : usize, y : usize) -> (usize, usize){
    let target :&str = &compute[y][x].sym;
    //println!("{}", target);

    let x_len = compute[y].len();
    let mut x_max = x;
    for i in x..x_len{
        if compute[y][i].sym == target{
            x_max = i;
            //println!("x max changed {} {} {}", x_max, compute[y][x].sym, target);
        } else {
            break;
        }
    }
    let y_len = compute.len();
    let mut y_max = y;
    for i in y..y_len{
        if compute[i][x].sym ==target{
            y_max = i;
        } else {
            break;
        }
    }
    (x_max,y_max)
}

///各@tableを識別するため、数字をふる。数字だけだと混ざってしまう可能性があるので=もつける
fn append_numbers(compute : &mut Vec<Vec<TableItem>>) -> usize{
    let y_len = compute.len();
    let mut number = 1;
    for y in 0..y_len {
        let x_len = compute[y].len();
        for x in 0..x_len {
            if compute[y][x].sym.starts_with("@") {
                append_number(&mut compute[y][x].sym, number);
                number += 1;
            }
        }
    }
    number
}

use std::ops::AddAssign;

fn append_number(str : &mut String, num : usize){
    str.add_assign("=");
    str.add_assign(&num.to_string());
}

///@@@table_name=32 みたいな感じになっているのでtable_nameを取り出す
fn get_table_name(s : &str, level : usize) -> String{
    match  s.rfind(|c : char| c =='=') {
        Some(index) => s[level..index].to_string(),
        None => unreachable!()
    }
}

///最初についている@の数で表の挿入が何回入れ子になっているかが分かる
fn count_at(s : &str) -> usize{
    let mut count = 0;
    for i in s.chars(){
        if i == '@'{
            count += 1;
        } else { break; }
    }
    count
}