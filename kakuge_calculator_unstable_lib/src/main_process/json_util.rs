use serde_json::{Value};
use serde_json::ser;
use linked_hash_map::LinkedHashMap;



///多分この辺がテキスト表記で人間に理解できる限界の構造だろう
pub fn get_object_object2(arg : &Value) -> Result<LinkedHashMap<String, LinkedHashMap<String,Vec<Vec<String>>>>, Value> {
    if let Some(obj) = arg.as_object() {
        let mut hash: LinkedHashMap<String, LinkedHashMap<String, Vec<Vec<String>>>> = LinkedHashMap::new();
        for (key, value) in obj {
            hash.insert(key.to_string(), get_basic_object2(value)?);
        }
        return Ok(hash);
    } else { return err(arg); }
}

pub fn get_object_object1(arg : &Value) -> Result<LinkedHashMap<String, LinkedHashMap<String,Vec<String>>>, Value>{
    if let Some(obj) = arg.as_object() {
        let mut hash: LinkedHashMap<String, LinkedHashMap<String, Vec<String>>> = LinkedHashMap::new();
        for (key, value) in obj {
            hash.insert(key.to_string(), get_basic_object1(value)?);
        }
        return Ok(hash);
    } else { return err(arg); }
}

pub fn get_object_object0(arg : &Value) -> Result<LinkedHashMap<String, LinkedHashMap<String,String>>, Value>{
    if let Some(obj) = arg.as_object() {
        let mut hash: LinkedHashMap<String, LinkedHashMap<String, String>> = LinkedHashMap::new();
        for (key, value) in obj {
            hash.insert(key.to_string(), get_basic_object0(value)?);
        }
        return Ok(hash);
    } else { return err(arg); }
}

pub fn get_basic_object2(arg : &Value) -> Result<LinkedHashMap<String,Vec<Vec<String>>>, Value> {
    if let Some(obj) = arg.as_object() {
        let mut hash: LinkedHashMap<String, Vec<Vec<String>>> = LinkedHashMap::new();
        for (key, value) in obj {
            hash.insert(key.to_string(), get_basic_array2(value)?);
        }
        return Ok(hash);
    } else { return err(arg);}
}

pub fn get_basic_object1(arg : &Value) -> Result<LinkedHashMap<String,Vec<String>>, Value> {
    if let Some(obj) = arg.as_object() {
        let mut hash: LinkedHashMap<String, Vec<String>> = LinkedHashMap::new();
        for (key, value) in obj {
            hash.insert(key.to_string(), get_basic_array(value)?);
        }
        return Ok(hash);
    } else { return err(arg); }
}

pub fn get_basic_object0(arg : &Value) -> Result<LinkedHashMap<String,String>, Value> {
    if let Some(obj) = arg.as_object() {
        let mut hash: LinkedHashMap<String, String> = LinkedHashMap::new();
        for (key, value) in obj {
            hash.insert(key.to_string(), get_value_string(value)?);
        }
        return Ok(hash);
    } else { return err(arg); }
}

///２次元配列の中身が一次元配列かアイテムである3次元配列
pub fn get_basic_array3(arg : &Value) -> Result<Vec<Vec<Vec<String>>>, Value> {
    if let Some(array) = arg.as_array() {
        let mut vec: Vec<Vec<Vec<String>>> = vec![];
        for item in array {
            if let Some(array2) = item.as_array() {
                let mut vec2: Vec<Vec<String>> = vec![];
                for item2 in array2 {
                    vec2.push(get_basic_array(item2)?);
                }
                vec.push(vec2);
            } else { return err(item);}
        }
        return Ok(vec);
    } else { return err(arg); }
}

///一次元配列が一個か、一次元配列が複数個の２次元配列を要求する。
pub fn get_basic_array2(arg : &Value) -> Result<Vec<Vec<String>>, Value> {
    if let Some(array) = arg.as_array() {
        if array.iter().all(|item| get_value_string(item).is_ok()){
            //二次元の要素を含まない、完全なる一次元配列
            let mut vec : Vec<String> = vec![];
            for item in array {
                vec.push(get_value_string(item)?);
            }
            return Ok(vec![vec]);
        } else {
            let mut vec: Vec<Vec<String>> = vec![];

            //２次元の要素を一つでも含む場合、
            for item in array {
                vec.push(get_basic_array(item)?);
            }
            return Ok(vec);
        }
    } else if arg.as_object().is_some() {
        return err(arg);
    } else { return Ok(vec![vec![get_value_string(arg)?]]); }
}

///アイテムでもarrayでもvecにして返す。オブジェクトだと返せないのでそれがErrで返ってくる
pub fn get_basic_array(arg : &Value) -> Result<Vec<String>, Value>{
    if let Some(array) = arg.as_array(){
        let mut vec : Vec<String> = vec![];
        for item in array{
            vec.push(get_value_string(item)?);
        }
        return Ok(vec);
    }
    else if arg.as_object().is_some(){
        return err(arg);
    }
    else{ return Ok(vec![get_value_string(arg)?]) }
}

///文字列や数値などをrust側でparse可能なStringにする。arrayやobjectは処理できない。
pub fn get_value_string(arg : &Value) -> Result<String, Value>{
    if arg.as_array().is_some() || arg.as_object().is_some(){
        return err(arg);
    }
    if let Some(s) = arg.as_str(){
        return Ok(s.to_string())
    }
    match ser::to_string(arg){
        Ok(str) => Ok(str),
        Err(_) => err(arg)
    }
}

fn err<T>(e : &Value) -> Result<T,Value>{ Err(e.to_owned()) }