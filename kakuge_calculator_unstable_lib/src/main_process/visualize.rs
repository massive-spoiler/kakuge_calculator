use super::table_structs::TableItem;

pub fn visualize_table(original : &Vec<Vec<String>>, compute : &Vec<Vec<TableItem>>) -> Vec<String>{
    let mut new_table = original.to_vec();
    for y in 0..compute.len(){
        let len = compute[y].len();
        for x in 0..len{
            new_table[y+1][x+1] = table_item_to_string(&compute[y][x]);
        }
    }

    let mut result : Vec<String> = vec![];
    for s in offset(&new_table){
        result.push(s);
    }
    result
}

fn table_item_to_string(item : &TableItem) -> String{
    let mut str = item.sym.to_string();
    str += &num_to_string(item.num);
    str
}

fn num_to_string(num : f64) -> String{
    if num >= 0.0{ format!("+{}",num) }
    else { num.to_string() }
}

#[allow(dead_code)]
pub fn print(original : &Vec<Vec<String>>, compute : &Vec<Vec<TableItem>>){
    for line in visualize_table(original, compute){
        println!("{}",line);
    }
}

#[allow(dead_code)]
pub fn print_table_items(compute : &Vec<Vec<TableItem>>){
    let vec : Vec<Vec<String>>= compute.iter().map(|line| line.iter().map(|item| table_item_to_string(item)).collect()).collect();
    for l in offset(&vec){
        println!("{}", l);
    }
}

#[allow(dead_code)]
pub fn print_strs(table : &Vec<Vec<String>>){
    for line in offset(table){
        println!("{}", line);
    }
}

pub fn offset(table : &Vec<Vec<String>>) -> Vec<String>{
    let max_lens : Vec<usize> = table[0].iter().enumerate().map(|(index,_)|{
        get_max_len(table, index)
    }).collect();

    table.iter().map(|vec : &Vec<String>|{
        let mut s = String::new();
        for i in 0..vec.len(){
            let chunk = &vec[i];
            let spaces = max_lens[i] - chunk.len() + 1;
            s += &(" ".repeat(spaces) + &chunk);
        }
        s
    }).collect()
}

fn get_max_len(table : &Vec<Vec<String>>, col_index : usize) -> usize{
    let mut max_len : usize = 0;
    for i in 0..table.len(){
        let len = table[i][col_index].len();
        if max_len < len {
            max_len = len;
        }
    }
    return max_len;
}