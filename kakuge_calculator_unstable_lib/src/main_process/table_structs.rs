use serde_json::{Value};
use std::collections::HashSet;

#[derive(Clone,Debug)]
pub struct TablePair{
    pub original : Vec<Vec<String>>,
    pub compute : Vec<Vec<TableItem>>
}

impl TablePair{
    pub fn to_copy(&self) -> TablePair{
        return TablePair{
            original : self.original.to_vec(),
            compute : self.compute.to_vec()
        }
    }
}

#[derive(Clone,Debug)]
pub struct TableItem{
    pub sym : String,
    pub num : f64
}


pub fn to_table(array : &Vec<Value>) -> TablePair {
    let original : Vec<Vec<String>> = array.iter().map(|line| {
        line.as_str().expect(&format!["table must be string : {}", line]).split_whitespace().map(|chunk| chunk.to_string()).collect()
    }).collect();
    is_valid(&original);

    let compute : Vec<Vec<TableItem>> = original.iter().skip(1).map(|line| {
        line.iter().skip(1).map(|chunk| to_table_item(chunk)).collect()
    }).collect();
    TablePair{
        original,
        compute,
    }
}

///validじゃなければpanicする
fn is_valid(table : &Vec<Vec<String>>){
    assert!(2 <=table.len(), "tables must have more than 2 rows");
    let col = table[0].len();
    assert!(2 <= col, "tables must have more than 2 cols");
    let table_name = &table[0][0];
    for line in table{
        assert_eq!(line.len(), col, "{} : tables must be rectangles", table_name);
    }
    let mut set : HashSet<String> = HashSet::new();
    for i in 1..table.len(){
        let tag = &table[i][0];
        if set.insert(tag.to_string()) == false{
            panic!("{}: the tag '{}' is duplicated", table_name, tag);
        }
    }
    for i in 1..col{
        let tag = &table[0][i];
        if set.insert(tag.to_string()) == false{
            panic!("{}: the tag '{}' is duplicated", table_name, tag);
        }
    }
}

pub fn to_table_item(str : &str) -> TableItem{
    if str.chars().next().map(|c| c == '@').unwrap_or(false){
        return TableItem{
            sym : str.to_string(),
            num : 0.0
        }
    }

    let start = match str.rfind('-'){
        Some(index) => index,
        None => match str.rfind('+'){
            Some(index) => index,
            None => 0
        }
    };
    TableItem {
        sym : str[..start].to_string(),
        num: str[start..].to_string().parse().ok().expect(&format!["table value : {} is not valid", str]),
    }
}