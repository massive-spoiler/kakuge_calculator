use serde_json;
use serde_json::{Value, Map};
use linked_hash_map::LinkedHashMap;

use super::original_table_extensions::OriginalTableUtil;
use super::util;
use super::table_structs::*;
use super::php_tree::*;
use super::json_util::*;


pub fn execute_table_modification(master : &Value) -> Vec<(String, Vec<(String, TablePair)>)>{
    let master = master.as_object().expect("the top must be a json object");
    let tables = master.get("tables").expect("please create tables");
    let mut new_map = serde_json::Map::new();
    new_map.insert("options".to_string(), tables.to_owned());
    let exec_tree = convert_to_php_tree("execs", &master["execs"]);
    let mut result : Vec<(String, Vec<(String, TablePair)>)> = vec![];
    match exec_tree{
        PhpTree::Branch(_,vec) => {
            for item in vec{
                let mut r : Vec<(String, TablePair)> =vec![];
                calc_exec(&item, &new_map, &mut r, None);
                result.push((item.get_name(), r));
            }
        }
        _ =>{ unreachable!(); }
    }
    result
}

fn calc_exec(exec_tree : &PhpTree, option : &Map<String, Value>, result : &mut Vec<(String, TablePair)>, mut current_index : Option<usize>) {
    let exec_name = &exec_tree.get_name();
    if let Some(table) = option.get("table"){
        let table = table.as_array().expect(&format!["{}'s table can't be interpreted", exec_name]);
        let table = to_table(table);
        result.push((exec_name.to_string(),table));
        current_index = Some(result.len() - 1);
    }
    if let Some(current_index) = current_index{
        let (_,ref mut table_pair) = result[current_index];
        let table = &mut table_pair.compute;
        let original_table = &mut table_pair.original;

        match apply_modifications(option, table, original_table){
            Ok(_) =>{},
            Err(v) => println!("{} : interpretation failed", v),
        }
    }

    if let &PhpTree::Branch(_, ref vec) = exec_tree{
        let options = option.get("options").expect(&format!["{} : 'options' is not found", exec_name]);
        let options = options.as_object().expect(&format!["{} : couldn't interpret 'options'", exec_name]);
        for child in vec{
            let name = &child.get_name();
            let child_option = options.get(name).expect(&format!["{} : option '{}' is not found", exec_name, name]);
            let child_option = child_option.as_object().expect(&format!["{} : option '{}' couldn't be interpreted", exec_name, name]);
            calc_exec(&child, child_option, result, current_index);
        }
    }
}



fn apply_modifications(option : &Map<String, Value>, table : &mut Vec<Vec<TableItem>>, original_table : &mut Vec<Vec<String>>) -> Result<(),Value> {
    if let Some(v) = get(option, "symbol_overwrites")? {
        symbol_overwrite(table, original_table, &v);
    }
    if let Some(v) = get(option, "v_symbol_overwrites")?{
        symbol_overwrite(table, original_table, &v)
    }
    if let Some(v) = get_symbol_nums(option)? {
        set_sym_values(table, &v);
    }
    if let Some(v) = get(option, "direct_overwrites")?{
        set_direct_value(table, original_table, &v);
    }
    if let Some(v) = get(option, "v_direct_overwrites")? {
        set_direct_value(table, original_table, &v);
    }
    if let Some(s) = option.get("deltags") {
        del_tags(table, original_table, &get_basic_array(s)?);
    }
    Ok(())
}


fn get_symbol_nums(arg : &Map<String,Value>) -> Result<Option<Vec<Vec<String>>>, Value>{
    if let Some(a) = arg.get("symbol_nums"){
        return match get_basic_array2(a){
            Ok(r) => Ok(Some(r)),
            Err(v) => Err(v)
        };
    } else{ return Ok(None); }
}

fn get(arg : &Map<String,Value>, name : &str) -> Result<Option<LinkedHashMap<String, Vec<Vec<String>>>>, Value>{
    if let Some(a) = arg.get(name){
        return match get_basic_object2(a){
            Ok(r) => Ok(Some(r)),
            Err(v) => Err(v)
        };
    } else{ return Ok(None); }
}

fn del_tags(table: &mut Vec<Vec<TableItem>>, original_table : &mut Vec<Vec<String>>, tags : &Vec<String>) {
    for tag in tags {
        if let Some(index) = util::get_top_tag_index(original_table, tag){
            util::del_col(original_table, index);
            util::del_col(table, index - 1);
        }
        else if let Some(index) =  util::get_left_tag_index(original_table, tag){
            original_table.remove(index);
            table.remove(index - 1);
        }
        else{ println!("{} : no such tag", tag)}
    }
}

fn set_direct_value(table: &mut Vec<Vec<TableItem>>, original_table : &Vec<Vec<String>>,
                    directs : &LinkedHashMap<String, Vec<Vec<String>>>) {
    let indexes = original_table.get_indexes(directs);
    for (vec, value) in indexes{
        let num : f64 = value.parse().expect(&format!["{} is not a number", value]);
        for (x,y) in vec{
            table[y-1][x-1].sym = "_".to_string();
            table[y-1][x-1].num = num;
        }
    }
}

///symがallならば全てに。_symの場合 tag-nnのようなマイナスのものを対象に、数値ならば上書き、"+=30"のような文字列ならば加算、-=ならば減算する
fn set_sym_values(table : &mut Vec<Vec<TableItem>>, arg : &Vec<Vec<String>>){
    for vec in arg{
        if vec.len() <= 1{
            println!("{:?} this must be [sym1, sym2..., new_value]", vec);
        }
        let new_value = vec.last().unwrap();
        for sym in vec.iter().take(vec.len() - 1){
            //println!("{} {}", sym, new_value);
            set_sym_value(table, sym, new_value);
        }
    }
}

fn set_sym_value(table : &mut Vec<Vec<TableItem>>, key : &str, value : &str) {
    if value.len() == 0 {
        println!("set_sym: {}'s value is not found", key);
    }
    let is_all = key == "all";
    let (is_minus, real_key) = if key[0..1].eq("_") {
        (true, &key[1..])
    } else { (false, &key[0..]) };

    for y in 0..table.len() {
        let len = table[y].len();
        for x in 0..len {
            let item = &mut table[y][x];

            let key_matched = real_key.eq(&item.sym);
            let minus_matched = (is_minus && item.num < 0.0) || (!is_minus && 0.0 <= item.num);
            if is_all || (key_matched && minus_matched) {
                //let old = item.num;
                item.num = get_new_num(item.num, value).expect(&format!["{} {}: this can't be interpreted", key, value]);
                //println!("{} updated {} to {}", real_key, old, item.num);
            }
        }
    }
}

fn get_new_num(old : f64, value : &str) -> Option<f64>{
    //panicでもよい
    if value[0..2].eq("+="){
        return value[2..].parse::<f64>().ok().map(|v| old + v);
    }
    else if value[0..2].eq("-="){
        return value[2..].parse::<f64>().ok().map(|v| old - v);
    }
    else if value[0..1].eq("="){
        return value[1..].parse::<f64>().ok();
    }
    else { return value.parse::<f64>().ok(); }
}

fn symbol_overwrite(table: &mut Vec<Vec<TableItem>>, original_table : &Vec<Vec<String>>,
                    overs : &LinkedHashMap<String, Vec<Vec<String>>>){
    let indexes = original_table.get_indexes(overs);
    for (vec, value) in indexes{
        for (x,y) in vec{
            table[y-1][x-1] = super::table_structs::to_table_item(&value);
        }
    }
}
