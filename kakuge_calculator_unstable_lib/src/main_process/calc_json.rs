use serde_json;
use serde_json::{Value};//, Map};
//use std::collections::HashMap;

//use std::collections::HashSet;
use kakuge::calc::calc;
//use super::util;
use super::visualize;
use super::calc_exec;

pub fn calc_json(str : &str) {
    let v: Value = serde_json::from_str(str).expect(str);
    let result_vec = calc_exec::execute_table_modification(&v);



    let mut table_strs : Vec<(String, Vec<String>)> = vec![];
    let mut result_strs : Vec<(f64, Vec<String>)> = vec![];
    for (ref name, ref result) in result_vec {
        let pair = super::compose_table::compose_table(name, result);
        let table = &pair.original;
        table_strs.push(
            (name.to_string(), visualize::visualize_table(table, &pair.compute))
        );
        let mut new_table = table.to_owned();
        for y in 1..new_table.len() {
            for x in 1..new_table[y].len() {
                new_table[y][x] = pair.compute[y - 1][x - 1].num.to_string();
            }
        }
        //visualize::print_strs(&new_table);

        match calc(&new_table) {
            Ok(r) => result_strs.push((r.kitaichi, r.print(name))),
            Err(e) => println!("{:?}", e)
        }
    }

    result_strs.sort_by(|(l,_),(r,_)| l.partial_cmp(r).unwrap());

    for (_, txt) in result_strs{
        for line in txt{
            println!("{}",line);
        }
        println!();
    }

    for (name, table) in table_strs{
        println!("*{}", name);
        println!();
        for line in table{
            println!("{}", line);
        }
        println!();
    }
}
