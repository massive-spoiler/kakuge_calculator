//extern crate encoding_rs;
//use encoding_rs::SHIFT_JIS;
use std::process::Command;

//fileにはkakuge/php_test.phpのような相対パスを入れる
pub fn exec_php(rel_path : &str, args : Vec<String>) -> String{
    let vec =vec!["-f".to_string(), "./src/".to_string() + rel_path];//.iter().chain(args)
    let a : Vec<String> = vec.into_iter().chain(args).map(|s| s.to_string()).collect();
    //php.exeをインストールしてパスを通す必要がある
    let output = Command::new("php")
        .args(a)
        .output()
        .expect("failed to execute process");

    let hello = output.stdout;
    String::from_utf8_lossy(&hello).to_string()
}

//pub fn cmd_test() {
//    println!("{}",exec_php("kakuge/php_test.php"));
//}
