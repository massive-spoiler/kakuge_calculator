extern crate serde_json;
extern crate linked_hash_map;
extern crate kakuge;
pub mod main_process;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
