
pub trait VecUtil{
    fn invert(&self) -> Self;
    fn pos_shift(&self) -> (f64,Self);
    fn rotate(&self) -> Self;
    fn append_constants(&self) -> Self;
    fn col_len(&self) -> usize;
    fn row_len(&self) -> usize;
    fn compare_col(&self, l_index : usize, r_index : usize, f: &dyn Fn(f64,f64) -> bool) -> bool;
    fn compare_row(&self, l_index : usize, r_index : usize, f: &dyn Fn(f64,f64) -> bool) -> bool;
}

impl VecUtil for Vec<Vec<f64>>{
    fn invert(&self) -> Self{
        self.iter().map(|vec|{
            vec.iter().map(|v| -v).collect()
        }).collect()
    }

    //最小値がプラス１になるように全体を加算する。
    fn pos_shift(&self) -> (f64, Self){
        let minvalue = self.iter().flat_map(|vec| vec).fold(0.0/0.0, |l,r| r.min(l));
        let posvalue = -minvalue + 1.0;
        (posvalue, self.iter().map(|vec|{
            vec.iter().map(|v| *v + posvalue).collect()
        }).collect())
    }

    //双対問題的な感じでクイッと回す
    fn rotate(&self) -> Self{
        let mut dual_conditions : Vec<Vec<f64>>= vec![];

        for col in 0..self.col_len(){
            let mut temp : Vec<f64> = vec![];
            for row in 0..self.row_len(){
                temp.push(self[row][col]);
            }
            dual_conditions.push(temp)
        }
        dual_conditions
    }

    ///左端に1.0をくっつける
    fn append_constants(&self) -> Self{
        self.iter().map(|vec|{
            vec![1.0].iter().chain(vec.iter()).map(|v| *v).collect()
        }).collect()
    }

    fn col_len(&self) -> usize{ self[0].len() }
    fn row_len(&self) -> usize{ self.len() }



    fn compare_col(&self, l_index : usize, r_index : usize, f: &dyn Fn(f64,f64) -> bool) -> bool{
        for i in 0..self.len(){
            if f(self[i][l_index], self[i][r_index]) == false{
                return false;
            }
        }
        return true;
    }

    fn compare_row(&self, l_index : usize, r_index : usize, f: &dyn Fn(f64,f64) -> bool) -> bool{
        let len = self[0].len();
        for i in 0..len{
            if f(self[l_index][i], self[r_index][i]) == false{
                return false;
            }
        }
        return true;
    }
}

pub fn del_col<T>(table : &mut Vec<Vec<T>>, index : usize){
    for i in 0..table.len(){
        table[i].remove(index);
    }
}