use simplex::{LpError, simplex};
use super::util::VecUtil;

pub fn calc_simplex(conditions : Vec<Vec<f64>>, shift_value : f64) -> Result<(f64, Vec<f64>), LpError>{
    let mut dest_func = vec![0.0];
    dest_func.append(&mut vec![1.0; conditions.col_len() - 1]);

    match simplex(&dest_func,  &conditions, &vec![],  &vec![]){
        Ok(re) =>{
            let kotae = 1.0 / re.max - shift_value;
            let answer : Vec<_> = re.optimal.iter().map(|v| v / re.max).collect();
            Ok((-kotae, answer))
        }
        Err(e) => Err(e)
    }
}