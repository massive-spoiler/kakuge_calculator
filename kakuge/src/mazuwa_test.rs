//use super::super::simplex::simplex;
//
//pub fn mazuwa_test(){
//    //uの最大化は標準形に直して-1をかけて最小化する
//    //z = -x1 - x2 - x3
//    //制約条件，
//    //0x1 + 2x2 - 7x3 >= 1　　　(1)
//    //-2x1 +0x2 + 4x3 >= 1　　　(2)
//    //7x1 - 4x2 + 0x3 >＝ 1　　　(3)
//    //http://www.bunkyo.ac.jp/~hotta/lab/courses/2006/2006dmt/06dmt_gt.pdf
//
//    let dest_func = vec![0.0, -1.0, -1.0, -1.0];
//
//    let greater_cond = vec![
//        vec![1.0, -8., -10., -1.],
//        vec![1.0, -6., -8., -12.],
//        vec![1.0, -15., -4., -8.],
//    ];
//
//    match simplex(&dest_func, &vec![],&greater_cond,
//                  &vec![]){
//        Ok(r) => {
//            r.print();
//            let kotae = 1.0 / r.max + 8.;
//            let answer : Vec<_> = r.optimal.iter().map(|v| -v / r.max).collect();
//            let sum : f64 =answer.iter().sum();
//            println!("{} {} {:?}", -kotae, sum, answer)
//        },
//        Err(e) => println!("{:?}",e)
//    };
//}