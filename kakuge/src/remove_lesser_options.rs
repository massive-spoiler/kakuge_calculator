use super::util::*;
//use std::collections::HashSet;

pub fn remove_lesser_options(original : &Vec<Vec<String>>, values : &Vec<Vec<f64>>) -> (Vec<Vec<String>>, Vec<Vec<f64>>, Vec<String>){
    let mut original = original.to_vec();
    let mut values = values.to_vec();
    let mut removed : Vec<String> = vec![];
    loop {
        let lesser_rows = get_lesser_rows(&values);
        //println!("{:?}", lesser_rows);

        for &index in &lesser_rows {
            //println!("a{} {} {}", index, values.len(), lesser_rows.len());
            values.remove(index);
            //println!("b");
            removed.push(original[index + 1][0].to_string());
            //println!("c");
            original.remove(index + 1);
            //println!("d");
        }

        let lesser_cols = get_lesser_cols(&values);

        for &index in &lesser_cols{
            //println!("col {} {} {}", index, values[0].len(), lesser_cols.len());
            del_col(&mut values, index);
            removed.push(original[0][index + 1].to_string());
            del_col(&mut original, index + 1);
        }
        if lesser_rows.len() == 0 && lesser_cols.len() == 0 {
            break;
        }
    }
    (original, values, removed)
}

fn get_lesser_rows(values : &Vec<Vec<f64>>) -> Vec<usize>{
    let len = values.len();
    //let mut result : HashSet<usize> = HashSet::new();
    for i in (0 .. len).rev(){
        for k in 0..len{
            if i != k{
                if values.compare_row(i,k,&|l,r| l <= r){
                    //攻撃側選択肢では、全てにおいて小さい子は存在する意味がない
                    //result.insert(i);
                    return vec![i];
                }
            }
        }
    }
//    let mut vec : Vec<usize> =result.into_iter().collect();
//    vec.sort_by(|l,r| r.cmp(l));
    return vec![];
}

fn get_lesser_cols(values : &Vec<Vec<f64>>) -> Vec<usize>{
    let len = values[0].len();
    //let mut result : HashSet<usize> = HashSet::new();
    for i in 0 .. len{
        for k in (0..len).rev(){
            if i != k{
                if values.compare_col(i,k,&|l,r| l <= r){
                    //守備側選択肢では、全てにおいて大きい子は存在する意味がない
                    //result.insert(k);
                    return vec![k];
                }
            }
        }
    }
    //let mut vec : Vec<usize> =result.into_iter().collect();
    //vec.sort_by(|l,r| r.cmp(l));
    //vec
    return vec![];
}