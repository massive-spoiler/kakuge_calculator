#[allow(dead_code)]
pub fn print_strs(table : &Vec<Vec<String>>){
    for line in offset(table){
        println!("{}", line);
    }
}

pub fn get_formated_text(table : &Vec<Vec<String>>) -> String{
    let mut result = String::new();
    for line in offset(table){
        result.push_str(&line);
        result.push('\n');
    }
    result
}

pub fn offset(table : &Vec<Vec<String>>) -> Vec<String>{
    let max_lens : Vec<usize> = table[0].iter().enumerate().map(|(index,_)|{
        get_max_len(table, index)
    }).collect();

    table.iter().map(|vec : &Vec<String>|{
        let mut s = String::new();
        for i in 0..vec.len(){
            let chunk = &vec[i];
            let spaces = max_lens[i] - chunk.len() + 1;
            s += &(" ".repeat(spaces) + &chunk);
        }
        s
    }).collect()
}

fn get_max_len(table : &Vec<Vec<String>>, col_index : usize) -> usize{
    let mut max_len : usize = 0;
    for i in 0..table.len(){
        let len = table[i][col_index].len();
        if max_len < len {
            max_len = len;
        }
    }
    return max_len;
}