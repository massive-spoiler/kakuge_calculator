use std::cmp::Ordering;

#[derive(Debug)]
pub struct KakugeCalcResult{
    pub kitaichi : f64,
    pub offence : Vec<(String, f64)>,
    pub defence : Vec<(String, f64)>,
    pub removed : Vec<String>,
    pub time : u32,
}

pub struct CalcResult{
    pub kitaichi : f64,
    pub moves : Vec<(String, f64)>,
    pub time : u32,
}

impl CalcResult{
    pub fn moves_to_str(&self) -> String{
        let mut s = String::new();
        let (sorted,_) = sort(&self.moves);
        for (ref name, val) in &sorted {
            s += &format!("{}<{:.1}%> | ", name, val * 100.0);
        }
        s
    }
}

impl KakugeCalcResult{
    pub fn print(&self, name : &str) -> Vec<String>{
        let mut r : Vec<String> = vec![];
        r.push(format!("{} Average　{:.2}", name, self.kitaichi));
        let mut s = String::new();
        s += "Offence: ";
        {
            let (sorted, _) =sort(&self.offence);
            for (ref name, val) in sorted {
                s += &format!("{}<{:.1}%> | ", name, val * 100.0);
            }
//            s += "0%:";
//            for item in &removed{
//                s += &format!("{},", item);
//            }
        }
        r.push(s);

        let mut s = String::new();
        s += "Defence: ";
        {
            let (sorted, _) = sort(&self.defence);
            for (ref name, val) in sorted {
                s += &format!("{}<{:.1}%> | ", name, val * 100.0);
            }
//            s += "0%:";
//            for item in &removed {
//                s += &format!("{},", item);
//            }
        }
        r.push(s);
        //r.push(format!("{:?} removed", self.removed));
        r
    }
}

fn sort(vec : &Vec<(String,f64)>) -> (Vec<(String,f64)>,Vec<String>){
    let mut vec = vec.to_vec();
    vec.sort_by(|&(_,l),&(_,r)| r.partial_cmp(&l).unwrap_or(Ordering::Equal));
    let removed = remove_0(&mut vec);
    (vec, removed)
}

fn remove_0(vec : &mut Vec<(String,f64)>) -> Vec<String>{
    let mut removed : Vec<String> = vec![];
    loop {
        if vec.len() != 0 {
            let index = vec.len() - 1;

            let (_, val) = vec[index];
            if val < 0.001 {
                let name = vec[index].0.to_string();
                removed.push(name.to_string());
                vec.remove(index);
                continue;
            }
        }
        break;
    }
    removed
}