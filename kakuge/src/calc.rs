
use super::util::VecUtil;
use super::calc_simplex::calc_simplex;
use super::KakugeCalcResult;
use super::LpError;
use super::remove_lesser_options::remove_lesser_options;
use crate::kakuge_calc_result::CalcResult;
use std::time::Instant;
//use super::visualize;
//use std::time::{Duration, Instant};

fn get_scores(score_table : &Vec<Vec<String>>) -> Vec<Vec<f64>>{
    score_table.iter().skip(1).map(|l|{
        l.iter().skip(1).map(|s| s.parse::<f64>().unwrap()).collect()
    }).collect()
}

fn simplex_offence(original : &Vec<Vec<f64>>) -> Result<(f64, Vec<f64>), LpError> {
    let dual = original.invert().rotate();
    let (shift, dual) = dual.pos_shift();
    let dual = dual.invert().append_constants();

    match calc_simplex(dual, shift) {
        Ok((answer, probs)) => Ok((answer, probs)),
        Err(e) => return Err(e)
    }
}

fn simplex_offence_with_names(floats : &Vec<Vec<f64>>, strs : &Vec<Vec<String>>) -> Result<(f64, Vec<(String, f64)>), LpError>{
    let (average, offence) = simplex_offence(floats)?;
    let with_names :Vec<(String, f64)>= offence.iter().enumerate().map(|(index,prob)|{
        let name = strs[index + 1][0].to_string();
        (name, *prob)
    }).collect();
    Ok((average, with_names))
}

///攻防の両方を計算するのは大変なので片方だけのも作ってみる。
pub fn calc_offence(score_table : &Vec<Vec<String>>) -> Result<CalcResult, LpError>{
    let start = Instant::now();

    let original : Vec<Vec<f64>> = get_scores(score_table);
    let (score_table, original, _removed) = remove_lesser_options(score_table, &original);
    let (kitaichi, moves) = simplex_offence_with_names(&original, &score_table)?;

    let end = start.elapsed();
    Ok(CalcResult{ kitaichi, moves,time : end.as_secs() as u32 })
}

fn simplex_defence(original : &Vec<Vec<f64>>) -> Result<(f64, Vec<f64>), LpError> {
    let (shift_value, conditions) = original.pos_shift();
    let conditions = conditions.invert();
    let conditions = conditions.append_constants();

    match calc_simplex(conditions, shift_value) {
        Ok((answer, probs)) => Ok((-answer, probs)),
        Err(e) => return Err(e)
    }
}

fn simplex_defence_with_names(floats : &Vec<Vec<f64>>, strs : &Vec<Vec<String>>) -> Result<(f64, Vec<(String, f64)>), LpError>{
    let (average, defence) = simplex_defence(floats)?;
    let with_names :Vec<(String, f64)>= defence.iter().enumerate().map(|(index, prob)|{
        let name = strs[0][index + 1].to_string();
        (name, *prob)
    }).collect();
    Ok((average, with_names))
}

///攻防の両方を計算するのは大変なので片方だけのも作ってみる。
pub fn calc_defence(score_table : &Vec<Vec<String>>) -> Result<CalcResult, LpError>{
    let start = Instant::now();

    let original : Vec<Vec<f64>> = get_scores(score_table);
    let (score_table, original, _removed) = remove_lesser_options(score_table, &original);
    let (kitaichi, moves) = simplex_defence_with_names(&original, &score_table)?;

    let end = start.elapsed();
    Ok(CalcResult{ kitaichi, moves, time : end.as_secs() as u32 })
}



pub fn calc(score_table : &Vec<Vec<String>>) -> Result<KakugeCalcResult, LpError>{
    let start = Instant::now();

    let original : Vec<Vec<f64>> = get_scores(score_table);
    let (score_table, original, removed) = remove_lesser_options(score_table, &original);

    let (_, defence) = simplex_defence_with_names(&original, &score_table)?;
    let (kitaichi, offence) = simplex_offence_with_names(&original, &score_table)?;

    let end = start.elapsed();

    return Ok(KakugeCalcResult{
        kitaichi,
        offence,
        defence,
        removed,
        time : end.as_secs() as u32,
    });

}