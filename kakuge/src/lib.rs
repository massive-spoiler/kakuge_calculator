//pub mod necalli_test;
//pub mod mazuwa_test;
//pub mod sarani_test;
extern crate simplex;

mod util;
pub mod calc;
mod calc_simplex;
pub mod kakuge_calc_result;
mod remove_lesser_options;
pub mod visualize;

pub use self::calc::calc;
pub use self::kakuge_calc_result::KakugeCalcResult;
pub use simplex::LpError;






