use bo3_calculator::json_input::JsonInput;

pub fn url_string_to_json(url : &str) -> Result<JsonInput, String>{
    let bytes = general_compaction::url_string::get_bytes(url).ok_or(format!("invalid url"))?;
    let vec = general_compaction::string_compaction::decompress(bytes);
    return to_json_input(vec);
}

fn to_json_input(vec : Vec<String>) -> Result<JsonInput, String>{
    //println!("{:?}", vec);
    let mut reader = VecReader{ vec, index : 0 };
    let len = reader.get_usize()?;
    let mut names : Vec<String> = vec![];
    for _ in 0..len{
        names.push(reader.get_string()?);
    }
    let mut rate_table : Vec<Vec<f64>> = vec![];
    for _ in 0..len{
        let mut line : Vec<f64> = vec![];
        for _ in 0..len{
            line.push(reader.get_f64()?);
        }
        rate_table.push(line);
    }
    let p1 = get_conds(&mut reader, &names)?;
    let p2 = get_conds(&mut reader, &names)?;
    let o1 = get_conds(&mut reader, &names)?;
    let o2 = get_conds(&mut reader, &names)?;
    let has_class = reader.get_bit()?;
    let limit = reader.get_usize()?;

    return Ok(JsonInput{
        table : construct_table_str(&names, &rate_table),
        p1, p2, o1, o2, has_class, limit });
}

fn construct_table_str(names : &Vec<String>, rate_table : &Vec<Vec<f64>>) -> String{
    let mut result = String::new();
    fn e(r : &mut String, s : &str){
        r.push_str(s);
    }
    let r = &mut result;
    e(r, "table|");
    for name in names{
        e(r, name);
        e(r, "|");
    }
    e(r,"\n");
    for (line, name) in rate_table.iter().zip(names.iter()){
        e(r, name);
        e(r, "|");
        for item in line{
            e(r, &item.to_string());
            e(r, "|");
        }
        e(r, "\n");
    }
    return result;
}

fn get_conds(reader : &mut VecReader, names : &Vec<String>) -> Result<Vec<String>, String>{
    let mut result : Vec<String> = vec![];
    for i in 0..names.len(){
        if reader.get_bit()?{
            result.push(names[i].to_string());
        }
    }
    return Ok(result);
}

struct VecReader{
    pub vec : Vec<String>,
    pub index : usize,
}

impl VecReader{
    fn get(&mut self) -> Result<&str, String>{
        if self.vec.len() <= self.index{
            return Err(format!("len <= index"));
        }
        let r = &self.vec[self.index];
        self.index += 1;
        return Ok(r);
    }

    pub fn get_usize(&mut self) -> Result<usize, String>{
        self.get()?.parse().or_else(|_| Err(format!("Unable to get usize")))
    }

    pub fn get_f64(&mut self) -> Result<f64, String>{
        self.get()?.parse().or_else(|_| Err(format!("unable to get f64")))
    }

    pub fn get_string(&mut self) -> Result<String, String>{
        Ok(self.get()?.to_string())
    }

    pub fn get_bit(&mut self) -> Result<bool, String>{
        let v = self.get_usize()?;
        if v == 1{
            return Ok(true);
        } else if v == 0{
            return Ok(false);
        } else{
            return Err(format!("Not a bit {}", v));
        }
    }
}