function objToTables(obj){
    deleteAllDecks();
    console.log("table " + obj.table);
    var a2 = tableStrToArray2D(obj.table);
    var names = getDeckNames(a2);
    for(var name of names){
        addDeck(name);
    }
    var len = a2.length;
    var table = getMainTable();
    for(var i = 1; i < len; ++i){
        for(var k = 1; k < len; ++k){
            var tb = getTableElement(table, i,k);
            tb.value = a2[i][k];
        }
    }
    setCheckboxes(0, obj.p1);
    setCheckboxes(1, obj.p2);
    setCheckboxes(2, obj.o1);
    setCheckboxes(3, obj.o2);
}

function setCheckboxes(ind, checkedNames){
    var table = getFilterTable();
    var rows = table.rows.length - 2;
    for(var i = 1; i < rows; ++i){
        var cell = getTableCell(table, i, ind);
        //console.log("cell " + cell);
        //console.log("cell.childNodes " + cell.childNodes);
        //console.log("cell.childnodes.length " + cell.childNodes.length);
        //console.log("cell.childNodes[1] " + cell.childNodes[1]);
        var checkName = cell.childNodes[1].textContent;
        var check = cell.children[0];
        if(checkedNames.includes(checkName)){
            check.checked = true;
        }
        else{
            check.checked = false;
        }
    }
}

function tableStrToArray2D(str){
    var lines = str.split("\n");
    lines.pop();
    var result = [];
    for(var line of lines){
        var items = line.split("|");
        items.pop();
        var resultLine = [];
        for(var item of items){
            resultLine.push(item.trim());
        }
        result.push(resultLine);
    }
    return result;
}

function getDeckNames(a2){
    var len = a2.length;
    var result = [];
    for(var i = 1; i < len; ++i){
        result.push(a2[i][0]);
    }
    return result;
}

function deleteAllDecks(){
    var mainTable = getMainTable();
    while(true){
        var rows = mainTable.rows.length;
        if(1 < rows){
            deleteDeck(1);
        }
        else{ break; }
    }
}
