pub struct StringBuilder{
    pub s : String
}

#[no_mangle]
pub extern "C" fn create_string_builder() -> *mut StringBuilder{
    let b = Box::new(StringBuilder{ s : "".to_string() });
    Box::into_raw(b)
}




#[no_mangle]
pub extern "C" fn destroy_string_builder(b : *mut StringBuilder){
    unsafe{ Box::from_raw(b); }
}

#[no_mangle]
pub extern "C" fn string_builder_append(sb : *mut StringBuilder, code_point : i32) -> i32{
    match ::std::char::from_u32(code_point as u32) {
        Some(c) => {
            unsafe { (*sb).s.push(c) };
            return 1;
        }
        _ => return 0,
    }
}