pub mod for_wasm;
pub mod logic;
pub mod json_to_url_string;
pub mod url_string_to_json;
mod test_url_string_to_json;

#[cfg(test)]
mod tests {
    use crate::test_url_string_to_json::test;

    #[test]
    fn it_works() {

    }
}