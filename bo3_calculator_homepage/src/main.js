function getMainTable() {
	return document.getElementById("main-table");
}

function getFilterTable(){
	return document.getElementById("filter-table");
}

function colPlusBtnClicked() {
	let name = prompt();
	if (name) {
		addDeck(name);
	}
}

function addDeck(deckName){
	var table = getMainTable();
	var cols = table.rows[0].cells.length;
	insertDeck(cols - 1, deckName);
}

function insertDeck(ins_index, deckName){
	insertCol(ins_index, deckName);
	insertRow(ins_index, deckName);
	insertFilterTable(ins_index, deckName);
	adjustStates();
}

function insertFilterTable(ins_index, deckName){
	var table = getFilterTable();
	var ins = ins_index;

	var insertRow = table.insertRow(ins);
	for (var i = 0; i < 4; i++) {
		var insertCell = insertRow.insertCell(i);
		insertCell.innerHTML = '<input type="checkbox" checked="true">' + deckName;
	}
	
}

function insertCol(ins_index, name) {
	var table = getMainTable();
	var rows = table.rows.length;
	var cols = table.rows[0].cells.length;
	var ins = ins_index;

	var titleCell = table.rows[0].insertCell(ins_index);
	titleCell.outerHTML = '<th><button type="button" onclick="headerClicked(this,event)">' + name + '</button></th>';
	for (var i = 1; i < rows; i++) {
		var insertCell = table.rows[i].insertCell(ins);
		insertCell.innerHTML = '<input class="inp" type="text" value="50" onchange="textBoxOnchange()">';
	}
}

function insertRow(ins_index, name) {
	var table = getMainTable();
	var rows = table.rows.length;
	var cols = table.rows[0].cells.length;
	var ins = ins_index;

	var insertRow = table.insertRow(ins);
	var titleCell = insertRow.insertCell(0);
	titleCell.outerHTML = '<td>' + name + '</td>';
	for (var i = 1; i < cols - 1; i++) {
		var insertCell = insertRow.insertCell(i);
		insertCell.innerHTML = '<input class="inp" type="text" value="50">';
	}
	var lastCell = insertRow.insertCell(cols - 1);
	lastCell.innerHTML = "-";
}

function setCheckStates(btn, checked){
	var pos = getTablePos(btn);
	var col = pos.col;
	var table = getFilterTable();
	var rows = table.rows.length;
	for(var i = 1; i < rows; i++){
		var elem = table.rows[i].cells[col].children[0];
		elem.checked = checked;
	}
}

function getTablePos(btn) {
	var td = btn.parentElement;
	var tr = td.parentElement;
	var row = $(tr)[0].rowIndex;
	var col = $(td)[0].cellIndex;

	return { row, col };
}

function getTableElement(table, row, col) {
	return table.rows[row].cells[col].children[0];
}

function getTableCell(table, row, col){
	return table.rows[row].cells[col];
}

function adjustStates(){
	var table = getMainTable();
	var rows = table.rows.length;
	var cols = table.rows[0].cells.length;
	//console.log("rows " + rows + " cols " + cols);

	for(var i = 1; i < rows; ++i){
		for(var k = 1; k < cols - 1; ++k){
			if(i < k){
				var inp = getTableElement(table, i, k);
				//console.log("inp " + inp);
				inp.disabled = false;
				var value = inp.value;
				var v = 100 - value;
				//console.log("g " + trav);
				var inpv = getTableElement(table, k,i  );
				inpv.value = v;
			} else{
				var inp = getTableElement(table, i, k);
				inp.disabled = true;
			}
		}
	}
	initTableColors();
}

function textBoxOnchange(){
	adjustStates();
}


function initTableColors() {
	//なんか色付けはうまくいかないですねえ

	// var table = getMainTable();
	// var rowLen = table.rows.length;
	
	// for (let y = 1; y < rowLen; ++y) {
	// 	for (let x = 1; x < rowLen; ++x) {
	// 		let cl = getTableElement(table, y, x).classList;
	// 		cl.remove("blue");

	// 		if (x % 2 == 0 || y % 2 == 0) {
	// 			cl.add("blue");
	// 		}
	// 	}
	// }
}