function showMenu(x, y) {
	var myMenu = document.getElementById('js-menu');
	myMenu.style.left = x + 'px';
	myMenu.style.top = y + 'px';
	myMenu.classList.add('show');
}

function hideMenu() {
	var myMenu = document.getElementById('js-menu');
	if (myMenu.classList.contains('show')) {
		myMenu.classList.remove('show');
	}
}

function headerClicked(btn, event) {
	var pos = getTablePos(btn);
	g_clickedTablePos = pos; //グローバルに突っ込む(もっときれいな書き方があるとは思うが・・・)

	showMenu(event.clientX, event.clientY)
}

function renameClicked() {
	var name = window.prompt("名前の変更");
	if (name) {
        var mainTable = getMainTable();
        var col = g_clickedTablePos.col;
		var btn = getTableElement(mainTable, 0, col);
        btn.innerHTML = name;
        var cell = getTableCell(mainTable, col, 0);
        cell.innerHTML = name;
        var filterTable = getFilterTable();
        for(var i = 0; i < 4; ++i){
            var node = getTableCell(filterTable, col, i).childNodes[1];
            
            //console.log("inpchildren = " + cell.innerText);
            node.textContent = name;
        }
	}
}

function insertClicked() {
	let name = prompt("ここに追加");
	if (name) {
		var col = g_clickedTablePos.col;
		insertDeck(col, name);
	}
}

function deleteClicked() {
    var col = g_clickedTablePos.col;
    var mainTable = getMainTable();
    var rows = mainTable.rows.length;
    if (3 < rows) {
        deleteDeck(col);
    }
}

function deleteDeck(col){
    var mainTable = getMainTable();
    var rows = mainTable.rows.length;
    for (var i = 0; i < rows; ++i) {
        mainTable.rows[i].deleteCell(col);
    }
    mainTable.deleteRow(col);
    var filterTable = getFilterTable();
    filterTable.deleteRow(col);
}

function cancelClicked(){}