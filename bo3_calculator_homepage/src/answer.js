function getBaseUrl() {
    var url = location.href;
    var hatena = url.indexOf("?");
    if (hatena != -1) {
        return url.substr(0, hatena);
    }
    else { return url; }
}

function analyzeUrl() {
    var url = window.location.href;
    var baseUrl = getBaseUrl();
    if (url.startsWith(baseUrl)) {
        var query = url.substr(baseUrl.length + 3);
        if (query) {
            try {
                return JSON.parse(decodeURIComponent(query));
            } catch{ }
        }
        return null;
    }
}


