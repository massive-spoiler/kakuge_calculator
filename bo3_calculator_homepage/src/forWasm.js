///exportをとって結果を返すcallbackを入れる。
function fetchWasm(wasmFile, callback){
	return fetch(wasmFile).then(response => response.arrayBuffer())
    .then(bytes => WebAssembly.instantiate(bytes, {}))
    .then(results => {
      return callback(results.instance.exports);
    });
}

///exportと文字列からwasmの入力に使うStringBuilderを作る。それを使うコールバックを実行し、抜ける時StringBuilderをdestroyする。
function createWasmInput(exp, text, callback){
  let sb = exp.create_string_builder();
  try{
    for (let c of text) {
      exp.string_builder_append(sb, c.codePointAt(0));
    }
    return callback(sb);
  } finally{
  	exp.destroy_string_builder(sb);
  }
}

///exportとResultForWasmから文字列を取り出す。resultのdestroyもする。
function readWasmResultAndDestroy(exp, result){
  try{
    var len = exp.result_get_length(result);
    var addr = exp.result_get_address(result);
    var decoder = new TextDecoder('utf-8');
    var mem = exp.memory.buffer;
    var bytes = new Uint8Array(mem, addr, len);
    return decoder.decode(bytes);
  } finally {
  	exp.destroy_result(result);
  }
}

function calc(obj){
	let json = JSON.stringify(obj);
	return fetchWasm('bo3_calculator_homepage.wasm',
      ex => {
      	return createWasmInput(ex, json, inp =>{
        	let r = ex.calc_from_obj(inp);
        	let result = readWasmResultAndDestroy(ex, r);
        	return result;
        });
      });
}

function json_input_to_url(obj){
	let json = JSON.stringify(obj);
  	return fetchWasm('bo3_calculator_homepage.wasm',
        ex => {
        	return createWasmInput(ex, json, inp =>{
          	let r = ex.json_input_to_url(inp);
          	let result = readWasmResultAndDestroy(ex, r);
          	return result;
          });
        });
}

function url_to_json_input(url){
  	return fetchWasm('bo3_calculator_homepage.wasm',
        ex => {
        	return createWasmInput(ex, url, inp =>{
          	let r = ex.url_to_json_input(inp);
          	let result = readWasmResultAndDestroy(ex, r);
          	return JSON.parse(result);
          });
        });
}