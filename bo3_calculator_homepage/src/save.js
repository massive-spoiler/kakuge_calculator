function getUrl(encodedJson) {
	return getBaseUrl() + "?" + encodedJson;
}

function getBaseUrl() {
	var url = location.href;
	var hatena = url.indexOf("?");
	if (hatena != -1) {
		return url.substr(0, hatena);
	}
	else { return url; }
}

async function getEncodedJson(){
    var obj = tablesToObj();
    //console.log("table " + JSON.stringify(obj));
    return await json_input_to_url(obj)
}

async function saveUrl() {
	let json = await getEncodedJson();
  let obj = await url_to_json_input(json);
  //console.log("obj " + JSON.stringify(obj));
	let url = getUrl(json);

	writeUrlandMessage(url, "このURLをテキストエディタ等に保存してください。");
}


async function shareUrl() {
	let json = await getEncodedJson();
    let url = getUrl(json);
    
	writeUrlandMessage(url, "このURLをシェアしてください。");
}

function writeUrlandMessage(url, message, copyEnabled) {
	let urlArea = document.getElementById('url_area');
	let messageArea = document.getElementById('message_area');
	let copyBtn = document.getElementById('copy_btn');

	messageArea.innerHTML = message;
	urlArea.innerHTML = url;
	copyBtn.classList.remove("mienai");
	urlArea.classList.remove("mienai");
	urlArea.select();
}

function copyClicked() {
	let urlArea = document.getElementById('url_area');
	urlArea.select();
	document.execCommand('copy');
}


