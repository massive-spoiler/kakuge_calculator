use crate::for_wasm::string_builder::StringBuilder;
use crate::for_wasm::result::*;
use bo3_calculator::json_input::JsonInput;
use crate::json_to_url_string::json_to_url_string;
use crate::url_string_to_json::url_string_to_json;

#[no_mangle]
pub extern "C" fn calc_from_obj(sb : *const StringBuilder) -> *mut ResultForWasm{
    let json : Result<JsonInput,_> = serde_json::from_str(unsafe{ &(*sb).s });
    let _t = match json{
        Err(_) => return create_err("invalid json data"),
        Ok(t) => t,
    };


    match bo3_calculator::calc_json::json_to_json(unsafe{ &(*sb).s }){
        Ok(result) => return create_ok(&serde_json::to_string(&result).unwrap()),
        Err(s) => return create_err(&s),
    }
}


#[no_mangle]
pub extern "C" fn json_input_to_url(sb : *const StringBuilder) -> *mut ResultForWasm {
    let json : Result<JsonInput, _> = serde_json::from_str(unsafe{ &(*sb).s });

    let json = match json{
        Err(_) => return create_err("invalid json data"),
        Ok(t) => t,
    };

    let url = match json_to_url_string(json){
        Ok(url) => url,
        Err(s) => return create_err(&s),
    };

    return create_ok(&url);
}

#[no_mangle]
pub extern "C" fn url_to_json_input(sb : *const StringBuilder) -> *mut ResultForWasm {
    let s =unsafe{ &(*sb).s.to_string() };


    let json = match url_string_to_json(s){
        Err(_) => return create_err("invalid url"),
        Ok(t) => t,
    };

    let json_str = match serde_json::to_string(&json){
        Ok(js) => js,
        Err(_) => return create_err("failed"),
    };

    return create_ok(&json_str);
}