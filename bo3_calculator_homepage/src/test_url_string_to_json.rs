use bo3_calculator::json_input::JsonInput;

#[test]
pub fn test(){
    let sample = bo3_calculator::test_calc::get_sample_json();
    let url = crate::json_to_url_string::json_to_url_string(sample).unwrap();
    let json = crate::url_string_to_json::url_string_to_json(&url).unwrap();
    //println!("{:?}", json);
}
#[test]
pub fn test2(){
    let hoge = r#"{"p1":["式神W"],"p2":["式神W"],"o1":["式神W"],"o2":["式神W"],"table":"相性表 | 式神W | \n式神W | 50 | \n","limit":5,"has_class":true}"#;
    let input : JsonInput = serde_json::from_str(hoge).unwrap();
    println!("{:?}", input);
    let url = crate::json_to_url_string::json_to_url_string(input).unwrap();
    let json = crate::url_string_to_json::url_string_to_json(&url).unwrap();
    println!("{:?}", json);

}