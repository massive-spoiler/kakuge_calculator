use bo3_calculator::json_input::JsonInput;
use bo3_calculator::text_to_table::text_to_table;
use bo3_calculator::rate_table::RateTable;
use bo3_calculator::calc_json::get_conds;
use bo3_calculator::calc_bo3s_conditions::CalcBo3sConditions;
use std::collections::HashSet;

pub fn json_to_url_string(json : JsonInput) -> Result<String, String>{
    let table_str = &json.table;
    let table = text_to_table(table_str, false)?;
    //println!("rate table {:?}", table);
    let conds = get_conds(&table.names, &json)?;

    let vec = to_str_vec(&table, &conds, &json);
    let bytes = general_compaction::string_compaction::compress_strings(vec);
    return Ok(general_compaction::url_string::get_url_string(&bytes));
}



fn to_str_vec(table : &RateTable, conds : &CalcBo3sConditions, obj : &JsonInput) -> Vec<String>{
    let mut vec : Vec<String> = vec![];
    let len = table.names.len();
    vec.push(len.to_string());
    //lenの長さの名前リスト、正方形、フラグリストがある
    for name in &table.names{
        vec.push(name.to_string());
    }
    for line in &table.rates{
        for item in line{
            println!("item {}", item);
            vec.push(item.to_string());
        }
    }
    set_conds(&mut vec, &conds.player1_include, len);
    set_conds(&mut vec, &conds.player2_include, len);
    set_conds(&mut vec, &conds.oppo1_include, len);
    set_conds(&mut vec, &conds.oppo2_include, len);
    push_bool(&mut vec,obj.has_class);
    vec.push(obj.limit.to_string());

    return vec;
}



fn push_bool(vec : &mut Vec<String>, b : bool){
    if b{
        vec.push("1".to_string());
    } else{
        vec.push("0".to_string());
    }
}

fn set_conds(vec : &mut Vec<String>, hash : &HashSet<usize>, names_len : usize){
    for i in 0..names_len{
        push_bool(vec, hash.contains(&i));
    }
}