async function calcClicked() {
	var obj = tablesToObj();
	//try {
		//console.log(obj);
		let ans =await calc(obj);
		//console.log(ans);
		var answer = JSON.parse(ans);
		//var answer = getDummy();
		openAnswer(answer);
	// }
	// catch{
	// 	alert("Calculation failed");
	// 	return;
	// }
}

// function openAnswer(obj){
// 	var json = JSON.stringify(obj);
// 	var q = encodeURIComponent(json);
// 	window.open('answer.html?q=' + q);
// }

function jsonToJstreeObj(obj) {
    return {
        text: "BO3計算結果",
        state : { opened : true },
        children: [
            {
                text: "平均勝率が高い順",
                children: getFirstChildren(obj.average_average),
            },
            {
                text: "当て負け時平均勝率が高い順",
                children: getFirstChildren(obj.atemake_average),
            },
            {
                text: "最悪組み合わせの勝率が高い順",
                children: getFirstChildren(obj.average_worst),
            },
            {
                text: "当て負け時の最悪組み合わせの勝率が高い順",
                children: getFirstChildren(obj.atemake_worst),
            },
        ]
    }

}

function getFirstChildren(itemList){
    var result = [];
    for(var item of itemList){
        var r = {
            text : item.desc,
            state : { opened : true },
            children : [
                {
                    text : "平均勝率の悪い組み合わせ",
                    children : getSecondChildren(item.average_enemies)
                },
                {
                    text : "当て負け時勝率の悪い組み合わせ",
                    children : getSecondChildren(item.atemake_enemies)
                }
            ]
        };
        result.push(r);
    }
    return result;
}

function getSecondChildren(array){
    var result = [];
    for(var item of array){
        result.push({ text : item });
    }
    return result;
}

// function wr(str) {
//     document.write(str);
// }

function openAnswer(obj){
	var treeObj = jsonToJstreeObj(obj);
	$('#tree1').jstree("destroy");
	$('#tree1').jstree({
                    "plugins": ["wholerow"],
                    'core': { 
                        'themes': { 'stripes': true },
                        "data" : [
                            treeObj,
                        ]
                    },
                });

}

function getDummy(){
	return {
		average_worst : [
			{ 
				desc : "hoge",
				average_enemies : ["ave_ene1", "ave_ene2" ],
				atemake_enemies : ["ate&ene1", "ate_ene2" ]
			},
			{ 
				desc : "hoge2",
				average_enemies : ["2ave_ene1", "2ave_ene2" ],
				atemake_enemies : ["2ate_ene1", "2ate_ene2" ],
			}
		],
		average_average : [
			{ 
				desc : "vhoge",
				average_enemies : ["vave_ene1", "ave_ene2" ],
				atemake_enemies : ["vate_ene1", "ate_ene2" ]
			},
			{ 
				desc : "vhoge2",
				average_enemies : ["v2ave_ene1", "2ave_ene2" ],
				atemake_enemies : ["v2ate_ene1", "2ate_ene2" ],
			}
		],
		atemake_average : [
			{ 
				desc : "vhoge",
				average_enemies : ["vave_ene1", "ave_ene2" ],
				atemake_enemies : ["vate_ene1", "ate_ene2" ]
			},
			{ 
				desc : "vhoge2",
				average_enemies : ["v2ave_ene1", "2ave_ene2" ],
				atemake_enemies : ["v2ate_ene1", "2ate_ene2" ],
			}
		],
		atemake_worst : [
			{ 
				desc : "vhoge",
				average_enemies : ["vave_ene1", "ave_ene2" ],
				atemake_enemies : ["vate_ene1", "ate_ene2" ]
			},
			{ 
				desc : "vhoge2",
				average_enemies : ["v2ave_ene1", "2ave_ene2" ],
				atemake_enemies : ["v2ate_ene1", "2ate_ene2" ],
			}
		]
	}
}

function tablesToObj(){
	var tableStr = mainTableToArray2D();
	var str = "";
	for(line of tableStr){
		for(elem of line){
			str += elem + " | ";
		}
		str += "\n";
	}
	var table = str;
	var limit = 5;
	var has_class = true;
	var p1 = getFilterStrs(0);
	var p2 = getFilterStrs(1);
	var o1 = getFilterStrs(2);
	var o2 = getFilterStrs(3);
	return {
		p1, p2, o1, o2,
		table, limit, has_class
	};
}

function getFilterStrs(index){
	var table = getFilterTable();
	var rows = table.rows.length;
	var result = [];
	
	for(var i = 1; i < rows - 2; ++i){
		var cell = getTableCell(table, i, index);
		//console.log(cell);
		
		if(cell.children[0].checked){
			var name = tableCellToString(cell);
			result.push(name);
		}
	}
	return result;
}

function mainTableToArray2D(){
	var table = getMainTable();
	var rows = table.rows.length;
	var result = [];
	for(var i = 0; i < rows; ++i){
		var line = [];
		for(var k = 0; k < rows; ++k){
			var cell = getTableCell(table, i, k);
			line.push(tableCellToString(cell));
		}
		result.push(line);
	}
	return result;
}

function tableCellToString(cell){
	if(cell.children.length != 0){
		let child = cell.children[0];
		if(child.type == "button"){
			return child.innerHTML;
		} else if(child.type == "text"){
			return child.value;
		} else if(child.type == "checkbox"){
			//console.log("this is checkbox " + cell.childNodes.length)
			return cell.childNodes[1].textContent;
		}
	} else{
		return cell.innerHTML;
	}
}