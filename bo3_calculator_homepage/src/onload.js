async function onLoad() {
    let result = await analyzeUrlAndInitTable(window.location.href);
    //console.log("analy " + result);
    if (result == AnalyzeUrlResult.BrokenData) {
        alert("Urlのデータが解析できませんでした");
    } else if(result == AnalyzeUrlResult.ObjIsInvalid){
        alert("Urlのデータによる初期化に失敗しました");
    } else if(result == AnalyzeUrlResult.Success){
        return;
    } else if(result == AnalyzeUrlResult.NoData){
        constructTable();
    }
}

function constructTable(){
    var siki = "式神W"
    var youkai = "妖怪Nc";
    var conE = "コントロE";
    var nD = "自然D";
    var amaE = "アマツE";
    var senR = "潜伏R";

    addDeck(youkai);
    addDeck(conE);
    addDeck(nD);
    addDeck(amaE);
    addDeck(senR);

    set(siki, youkai, 50);
    set(siki, conE, 60);
    set(siki, nD, 60);
    set(siki, amaE, 40);
    set(siki, senR, 40);
    set(youkai, conE, 40);
    set(youkai, nD, 50);
    set(youkai, amaE, 40);
    set(youkai, senR, 60);
    set(conE, nD, 40);
    set(conE, amaE, 60);
    set(conE, senR, 80);
    set(nD, amaE, 40);
    set(nD, senR, 40);
    set(amaE, senR, 45);
    adjustStates();
}

function set(name1, name2, value) {
    var table = getMainTable();
    var cols = table.rows[0].cells.length;
    var map = {};
    for (var i = 1; i < cols; ++i) {
        var name = getTableElement(table, 0, i).innerHTML;
        map[name] = i;
    }

    var ind1 = map[name1];
    var ind2 = map[name2];
    var big = ind1 < ind2 ? ind2 : ind1;
    var small = ind1 < ind2 ? ind1 : ind2;

    var inp = getTableElement(table, small, big);
    inp.value = value;
}

var AnalyzeUrlResult = { Success: 0, NoData: 1, BaseUrlIsNotCorrect: 2, BrokenData: 3, ObjIsInvalid: 4 };

async function analyzeUrlAndInitTable(url) {
    var baseUrl = getBaseUrl();
    //console.log("analyzing.." + baseUrl);
    //var query = url.substr(0, baseUrl.length);
    //console.log("url..." + query);
    //console.log("nokori.." + url.substr(baseUrl.length));
    if (url.startsWith(baseUrl)) {
        var query = url.substr(baseUrl.length + 1);
        if (query) {
            try {
                var obj = await url_to_json_input(query);
            } catch{
                return AnalyzeUrlResult.BrokenData;
            }

            objToTables(obj);
            try {
                //objToTables(obj);
                return AnalyzeUrlResult.Success;
            } catch{
                return AnalyzeUrlResult.ObjIsInvalid;
            }
        }
        else{
            return AnalyzeUrlResult.NoData;
        }
    }
    else {
        //なんだこれ？　ジャンプtextboxを想定するとありうるが
        return AnalyzeUrlResult.BaseUrlIsNotCorrect;
    }
}



