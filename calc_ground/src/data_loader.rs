use std::fs::File;
use std::path::Path;
use std::io::Read;

pub fn load_file(filename : &str) -> Option<String>{
    let path = Path::new(r"calc_ground\src\data\").join(filename);
    load_file_with_path(&path)
}

pub fn load_folder_file(folder : &str, filename : &str) -> Option<String>{
    let path = Path::new(r"calc_ground\src\data\").join(folder).join(filename);
    load_file_with_path(&path)
}

pub fn load_file_with_path(path : &Path) -> Option<String>{
    match File::open(path){
        Err(_e) =>{
            //dbg!(e);
            return None;
        },
        Ok(mut file) =>{
            let mut result = String::new();
            match file.read_to_string(&mut result) {
                Err(_) => return None,
                Ok(_) => return Some(result),
            }
        }
    }
}