//use serde::{Serialize, Deserialize};
use serde_json::Value;
use crate::attack_data::AttackData;
use std::collections::HashSet;

#[derive(Serialize, Deserialize, Debug)]
pub struct CharaJson{
    pub name : String,
    pub forward : f64,
    pub backward : f64,
    pub stand : f64,
    pub crouch : f64,
    pub stand_throw : f64,
    pub crouch_throw : f64,
    pub moves : Vec<MoveJson>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MoveJson{
    pub name : String,
    pub ref_file_name : Option<String>,
    pub attack_level : u8,
    pub is_low : bool,
    pub crouching : bool,
    pub whiff_on_crouch : bool,

    pub damages : Value,
    pub counter_damages : Value,
    pub guard_damages : Value,
    pub punish : Option<Value>,
    pub whiff : Option<u32>,
    pub guard : Option<Value>,
    pub guard_back : Option<f64>,
    pub special_punish_pos_diff : Option<f64>,
}

#[derive(Debug, Clone)]
pub struct Chara{
    pub name : String,
    ///前歩き速度。1フレームあたりの移動量
    pub forward : u32,
    ///後ろ歩き速度
    pub backward : u32,
    ///立ち状態のヤラレ判定（一部大きいキャラがいる)
    pub stand : u32,
    ///しゃがみ状態のヤラレ判定
    pub crouch : u32,
    ///立ち状態の投げヤラレ判定（一部大きいキャラがいる)
    pub stand_throw : u32,
    ///しゃがみ状態の投げヤラレ判定（多分立ち状態と同じ)
    pub crouch_throw : u32,
    pub moves : Vec<Move>,
}

pub struct CharaRef<'a>{
    pub name : &'a String,
    ///前歩き速度。1フレームあたりの移動量
    pub forward : u32,
    ///後ろ歩き速度
    pub backward : u32,
    ///立ち状態のヤラレ判定（一部大きいキャラがいる)
    pub stand : u32,
    ///しゃがみ状態のヤラレ判定
    pub crouch : u32,
    ///立ち状態の投げヤラレ判定（一部大きいキャラがいる)
    pub stand_throw : u32,
    ///しゃがみ状態の投げヤラレ判定（多分立ち状態と同じ)
    pub crouch_throw : u32,
    pub moves : Vec<&'a Move>,
    pub punish_moves : Vec<&'a Move>
}

pub fn disable_moves<'a>(chara : &'a Chara, disabled_moves : &[&str]) -> Vec<&'a Move>{
    let set: HashSet<&str> = disabled_moves.iter().map(|s| *s).collect();
    let moves: Vec<&'a Move> = chara.moves.iter()
        .filter(|m| !set.contains(m.name.as_str())).collect();
    moves
}

impl Chara{
    pub fn to_chara_ref<'a>(&'a self, disabled_moves : &[&str]) -> CharaRef<'a>{
        let punish_moves : Vec<&Move> = self.moves.iter()
            .filter(|m| m.punish.len() != 0).collect();
        let active_moves = disable_moves(self, disabled_moves);

        CharaRef{
            name : &self.name,
            forward : self.forward,
            backward : self.backward,
            stand : self.stand,
            crouch : self.crouch,
            stand_throw : self.stand_throw,
            crouch_throw : self.crouch_throw,
            moves : active_moves,
            punish_moves,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Move{
    pub name : String,
    ///必殺技0 弱1 中2 強3
    pub attack_level : u8,
    ///下段かどうか
    pub is_low : bool,
    ///しゃがんでいるかどうか
    pub crouching : bool,
    ///しゃがみに当たらないかどうか
    pub whiff_on_crouch : bool,

    ///distanceが小さい順にソート。ただし0は一番最後。
    pub damages : Vec<DamageItem>,
    pub counter_damages : Vec<DamageItem>,
    pub guard_damages : Vec<DamageItem>,
    pub punish : Vec<DamageItem>,

    ///空振り時のスキのフレーム数。全体-24をとりあえず使おうかと思っている
    pub whiff : Option<u32>,

    ///ガード時の隙のフレーム数。distanceが狭い順にソートして判定していく。
    pub guard : Vec<MinusFrame>,
    ///ガード時の確反判定をするために必要。guardがない場合はいらない。
    pub guard_back : Option<i32>,

    pub attack_data : AttackData,
    pub special_punish_pos_diff : Option<i32>,
}

impl Move{
    pub fn moved_position(&self) -> i32 {
        let last_pos : i32 = self.attack_data.hurt_frames.last().map(|d| d.position).unwrap_or(0);
        match self.special_punish_pos_diff {
            Some(val) => val + last_pos,
            None => last_pos,
        }
    }

    //pub fn startup(&self) -> u32{ self.attack_data.hit_frames[0].frame_num }
}

#[derive(Debug, Clone)]
pub struct DamageItem {
    ///ボタンを押す前の距離。その距離によってコンボが変わりダメージも変わる。upper_distanceが0の場合は条件なしを表す。
    pub upper_distance: u32,
    pub damage: u32,
}

#[derive(Debug, Clone)]
pub struct MinusFrame {
    ///ボタンを押す前の距離。その距離によって持続が変わりマイナスフレームも変わる。upper_distanceが0の場合は条件なしを表す。
    pub upper_distance: u32,
    pub frame: u32,
}
