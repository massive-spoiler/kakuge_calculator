use crate::search_for_the_best_pos_test::vs_forward_walk_and_poses;
use crate::chara_gen::{WalkAndPos, WalkType};
use crate::do_battle::do_battle;
use crate::{default_scale, disabled_moves};
use crate::construct_characters::construct_characters;
use crate::analyze_calc_result::{analyze_kakuge_calc_result_text};

pub fn vs_forward_test() -> (String, String){
    let scale = default_scale();
    let cs = construct_characters(Some(scale));
    let c = &cs[0];
    let c = c.to_chara_ref(disabled_moves());
    let wps = vs_forward_walk_and_poses();
    let enemy = vec![WalkAndPos{ walk_type : WalkType::Forward, rel_pos : 0.0 }];
    let result = do_battle(&wps.vec, &enemy, &c, &c, wps.duration, wps.first_distance, scale);
    let f = |speed : u32| speed as f64 / scale as f64;
    let (off, def) = analyze_kakuge_calc_result_text(result, f(c.forward), f(c.backward),
                                                f(c.forward), f(c.backward));
    let explanation : String = wps.vec.iter().map(|wp| wp.print()).collect::<Vec<String>>().join(", ");
    let off_txt = &[format!("vs_forward, offence {}", &explanation)];
    let off_html = off.to_html(off_txt);
    let def_txt = &[format!("vs_forward, forward {}", &explanation)];
    let def_html = def.to_html(def_txt);
    (off_html, def_html)
}