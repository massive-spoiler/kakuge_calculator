use super::analyze_sfvdiff::{Hitbox, Tickbox};

///地上戦で判定が突き抜けることはないと思うのでleftはなしでもいいと思う。
#[derive(Debug, Clone)]
pub struct Rect{
    //pub left : i32,
    pub top : i32,
    pub bottom : i32,
    pub right : i32,
}

#[derive(Debug, Clone)]
pub struct HurtFrame{
    ///rightが大きい順にソート。一番右すら届かないなら他は調べる必要なし
    pub hurtboxes : Vec<Rect>,
    ///投げられ
    pub throw_hurt : Vec<Rect>,
    pub position : i32,
}

#[derive(Debug, Clone)]
pub struct HitFrame{
    ///0スタートなので普通の数え方から-1する
    pub frame_num : u32,
    ///rightが大きい順にソート。一番右すら届かないならほかは調べる必要なし
    pub hitboxes : Vec<Rect>,
    ///一番右をVecにアクセスせずに取得できる、処理速度のためのメンバ
    pub right : i32,
    pub position : i32,
}

#[derive(Debug, Clone)]
pub struct AttackData{
    ///frame_numが小さい順にソート
    pub hit_frames : Vec<HitFrame>,
    pub hurt_frames : Vec<HurtFrame>,
    ///hit_startまではカウンターになる
    pub hit_start : u32,
    ///hit_endまでは後ろに下がろうとするとガードする
    pub hit_end : u32,
}

///f64をi64にして処理の簡略化を図る
fn get_rect(hitbox : &Hitbox, position : f64, scale : f64) -> Rect{
    //let left = ((hitbox.left + position) * scale) as i32;
    fn c(a : f64) -> f64{ a / 100.0 }

    let right = c(hitbox.left) + c(hitbox.width) + position;
    let right = (right * scale) as i32;
    let top = ((c(hitbox.bottom) + c(hitbox.height)) * scale) as i32;
    let bottom = (c(hitbox.bottom) * scale) as i32;
    return Rect{ //left,
        right, top, bottom };
}

fn append_to_attack_data(r : &mut AttackData, hitbox : &Hitbox, tickbox : &Tickbox, shifts : &Vec<f64>, without_interruption : usize, scale : f64){

    for i in tickbox.start..=tickbox.end {
        let i = i as usize;

        if without_interruption <= i {
            break;
        }

        if hitbox.box_attrs.iter().any(|s| s == "strike") {
            r.hurt_frames[i].hurtboxes.push(get_rect(hitbox, shifts[i], scale));
        }
        if hitbox.box_attrs.iter().any(|s| s == "throw") {
            r.hurt_frames[i].throw_hurt.push(get_rect(hitbox, shifts[i], scale));
        }
        if hitbox.box_attrs.iter().any(|s| s == "hit") {
            //dbg!(&hitbox);
            //let found = r.hit_frames.iter_mut().find(|h| h.frame_num as usize == i);
            let frame = match r.hit_frames.iter_mut().find(|h| h.frame_num as usize == i){
                Some(f) => f,
                None =>{
                    let frame = HitFrame{ frame_num : i as u32, hitboxes : vec![], position : (shifts[i] * scale) as i32, right : 0 };
                    r.hit_frames.push(frame);
                    r.hit_frames.last_mut().unwrap()
                }
            };
            frame.hitboxes.push(get_rect(hitbox, shifts[i], scale));
        }
    }
}



pub fn construct_attack_data(hitboxes : Vec<Hitbox>, tickboxes : Vec<Tickbox>, shifts : Vec<f64>, without_interruption : usize, scale : u32) -> AttackData{
    let scale = scale as f64; //f64より整数のほうが速いと思うので整数化。その際にf64をscale倍にする。


    let mut r = AttackData{ hit_frames : vec![], hurt_frames : vec![], hit_start : 0, hit_end : 0 };

    for i in 0..without_interruption{
        let position = (shifts[i] * scale) as i32;
        r.hurt_frames.push(HurtFrame{ hurtboxes : vec![], throw_hurt : vec![], position });
    }

    for tickbox in &tickboxes{
        for hitbox in &hitboxes{
            if hitbox.box_type == tickbox.box_type &&
                hitbox.box_num == tickbox.box_num {
                append_to_attack_data(&mut r, hitbox, tickbox, &shifts, without_interruption, scale);
            }
        }
    }

    for f in &mut r.hit_frames{
        f.hitboxes.sort_by_key(|item| -item.right);
        f.right = f.hitboxes[0].right;
    }
    r.hit_frames.sort_by_key(|item| item.frame_num);
    for f in &mut r.hurt_frames{
        f.hurtboxes.sort_by_key(|item| -item.right);
        f.throw_hurt.sort_by_key(|item| -item.right);
    }

    r.hit_start = r.hit_frames.first().expect("no hit boxes").frame_num;
    r.hit_end = r.hit_frames.last().expect("no hit boxes").frame_num;

    return r;
}