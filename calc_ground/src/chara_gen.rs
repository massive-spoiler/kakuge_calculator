use crate::move_def::{MoveDef};
use crate::chara_data_structs::{CharaRef};
use crate::test_hitbox::WalkHistoryState;

pub struct ForwardCharaGenerable<'a>{
    chara : &'a CharaRef<'a>,
    duration : usize,
    rel_pos : f64,
}

impl<'a> ForwardCharaGenerable<'a>{
    pub fn new(chara : &'a CharaRef, duration : usize, rel_pos:f64) -> Self{
        ForwardCharaGenerable{ chara, duration, rel_pos}
    }
}

pub struct CrouchingCharaGenerable<'a>{
    chara : &'a CharaRef<'a>,
    duration : usize,
    rel_pos : f64,
}

impl<'a> CrouchingCharaGenerable<'a> {
    pub fn new<'b>(chara: &'b CharaRef, duration: usize, rel_pos: f64) -> CrouchingCharaGenerable<'b> {
        CrouchingCharaGenerable {
            chara,
            duration,
            rel_pos,
        }
    }
}

pub struct BackwardCharaGenerable<'a> {
    chara : &'a CharaRef<'a>,
    duration : usize,
    rel_pos : f64,
}
impl<'a> BackwardCharaGenerable<'a> {
    pub fn new(chara: &'a CharaRef, duration: usize, rel_pos: f64) -> Self {
        BackwardCharaGenerable { chara, duration, rel_pos }
    }
}


pub trait MoveDefGenerator {
    fn generate(&self, call_back : &mut impl FnMut(&MoveDef<'_>));
}

impl MoveDefGenerator for CrouchingCharaGenerable<'_>{
    fn generate(&self, call_back : &mut impl FnMut(&MoveDef<'_>)){
        //generate(self, call_back);

        let mut history : Vec<WalkHistoryState> = vec![];

        let get_name = |frame : usize, move_name : &str|{
            if self.rel_pos == 0.0 { format!("c{}_{}", frame, move_name) }
            else{ format!("c{:.1}_{}_{}", self.rel_pos, frame, move_name) }
        };

        for frame in 0..self.duration {
            for &m in &self.chara.moves {
                let def = MoveDef {
                    name: get_name(frame, &m.name),
                    walk_history: &history,
                    chara: self.chara,
                    move_item: Some(m),
                    rel_start_position: self.rel_pos
                };
                call_back(&def)
            }

            call_back(&MoveDef {
                name: get_name(frame, "wait"),
                walk_history: &history,
                chara: self.chara,
                move_item: None,
                rel_start_position: self.rel_pos
            });

            history.push(WalkHistoryState::Crouched);
        }
    }
}

impl<'a> MoveDefGenerator for ForwardCharaGenerable<'a>{
    fn generate<'b>(&'b self, call_back : &'b mut impl FnMut(&MoveDef<'_>)){
        let mut history : Vec<WalkHistoryState> = vec![];

        let get_name = |frame : usize, move_name : &str|{
            if self.rel_pos == 0.0 { format!("f{}_{}", frame, move_name) }
            else{ format!("f{:.1}_{}_{}", self.rel_pos, frame, move_name) }
        };

        for frame in 0..self.duration {
            for &m in &self.chara.moves {
                call_back(&MoveDef {
                    name: get_name(frame, &m.name),
                    walk_history: &history,
                    chara: self.chara,
                    move_item: Some(m),
                    rel_start_position: self.rel_pos
                });
            }

            call_back(&MoveDef {
                name: get_name(frame, "wait"),
                walk_history: &history,
                chara: self.chara,
                move_item: None,
                rel_start_position: self.rel_pos
            });

            history.push(WalkHistoryState::Forward);
        }
    }
}


impl MoveDefGenerator for BackwardCharaGenerable<'_>{
    fn generate(&self, call_back : &mut impl FnMut(&MoveDef<'_>)){
        let mut history : Vec<WalkHistoryState> = vec![];

        let get_name = |frame : usize, move_name : &str|{
            if self.rel_pos == 0.0 { format!("b{}_{}", frame, move_name) }
            else{ format!("b{:.1}_{}_{}", self.rel_pos, frame, move_name) }
        };

        for frame in 0..self.duration {
            for &m in &self.chara.moves {
                call_back(&MoveDef {
                    name: get_name(frame, &m.name),
                    walk_history: &history,
                    chara: self.chara,
                    move_item: Some(m),
                    rel_start_position: self.rel_pos
                });
            }

            call_back(&MoveDef {
                name: get_name(frame, "wait"),
                walk_history: &history,
                chara: self.chara,
                move_item: None,
                rel_start_position: self.rel_pos
            });

            history.push(WalkHistoryState::Backward);
        }
    }

}

///15フレーム前の状態を認識していると考えると、相手が現在いる位置は見えている位置から
///だいたい+0.75から-0.45ぐらいの幅のどこかにいるというある種量子力学的な状態と考えられる。
///そのどこかから攻撃される場合に、どのように立ち回るのが効率的かが問題である。
pub struct LaggedCharaGenerable<'a>{
    chara : &'a CharaRef<'a>,
    duration : usize,
    ///min_rel_posからmax_rel_posまでunit刻みで現れることにする
    min_rel_pos : f64,
    max_rel_pos : f64,
    unit : f64,
}

impl<'a> LaggedCharaGenerable<'a>{
    pub fn new<'b>(chara : &'b CharaRef, duration : usize, min_rel_pos : f64, max_rel_pos : f64, unit : f64) -> LaggedCharaGenerable<'b>{
        LaggedCharaGenerable{
            chara, duration, min_rel_pos, max_rel_pos, unit,
        }
    }
}

impl<'a> MoveDefGenerator for LaggedCharaGenerable<'a>{
    fn generate(&self, call_back : &mut impl FnMut(&MoveDef<'_>)){
        let mut rel_pos_mut = self.min_rel_pos;

        while rel_pos_mut < self.max_rel_pos{
            let rel_pos = rel_pos_mut;
            //println!("rel pos{}", rel_pos);
            rel_pos_mut += self.unit;
            let cc =CrouchingCharaGenerable::new(self.chara, self.duration, rel_pos);


            cc.generate(call_back)
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum WalkType{
    Forward, Backward, Crouching,
}

impl WalkType{
    pub fn print(&self) -> String{
        (match self{
            WalkType::Forward => "f",
            WalkType::Backward => "b",
            WalkType::Crouching => "c",
        }).to_string()
    }
}

#[derive(Debug, Copy, Clone)]
pub struct WalkAndPos{
    pub walk_type : WalkType,
    pub rel_pos : f64,
}

impl WalkAndPos{
    pub fn new(walk_type : WalkType, rel_pos : f64) -> WalkAndPos{
        WalkAndPos{ walk_type, rel_pos }
    }

    pub fn print(&self) -> String{
        format!("{}{:.1}", self.walk_type.print(), self.rel_pos)
    }
}



pub struct LaggedWalkingCharaGenerable<'a>{
    chara : &'a CharaRef<'a>,
    duration : usize,
    poses : &'a [WalkAndPos],
}

impl<'a> LaggedWalkingCharaGenerable<'a>{
    pub fn new(chara : &'a CharaRef, duration : usize, poses : &'a [WalkAndPos]) -> LaggedWalkingCharaGenerable<'a>{
        LaggedWalkingCharaGenerable{ chara, duration, poses}
    }
}


impl<'a> MoveDefGenerator for LaggedWalkingCharaGenerable<'a>{
    fn generate(&self, call_back : &mut impl FnMut(&MoveDef<'_>)) {
        for WalkAndPos{ walk_type, rel_pos } in self.poses{
            match walk_type{
                WalkType::Forward => ForwardCharaGenerable::new(self.chara, self.duration, *rel_pos).generate(call_back),
                WalkType::Backward => BackwardCharaGenerable::new(self.chara, self.duration, *rel_pos).generate(call_back),
                WalkType::Crouching => CrouchingCharaGenerable::new(self.chara, self.duration, *rel_pos).generate(call_back),
            }
        }
    }
}

pub struct LaggedForwardCharaGenerable<'a>{
    pub chara : &'a CharaRef<'a>,
    pub duration : usize,
    pub rel_poses : &'a[f64]
}

impl<'a> MoveDefGenerator for LaggedForwardCharaGenerable<'a>{
    fn generate(&self, call_back : &mut impl FnMut(&MoveDef<'_>)) {
        let fc = ForwardCharaGenerable::new(self.chara, self.duration, 0.0);
        fc.generate(call_back);

        for &rel_pos in self.rel_poses {
            let fc = ForwardCharaGenerable::new(self.chara, self.duration, rel_pos);
            fc.generate(call_back);
        }
    }
}
