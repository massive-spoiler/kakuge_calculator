use regex::Regex;
use kakuge::kakuge_calc_result::CalcResult;
use std::collections::{BTreeMap};
use ordered_float::OrderedFloat;
use crate::analyze_calc_structs::{StrategyType, Act, AnalyzedActs};
use kakuge::KakugeCalcResult;

///backwardは正
pub fn analyze_kakuge_calc_result(kr : KakugeCalcResult, off_forward : f64, off_backward : f64,
                                  def_forward : f64, def_backward : f64) -> (AnalyzedActs, AnalyzedActs){

    let off = CalcResult{ kitaichi : kr.kitaichi, moves : kr.offence, time : kr.time };
    let def = CalcResult{ kitaichi : kr.kitaichi, moves : kr.defence, time : kr.time };

    let off_acts = parse_calc_result(&off, off_forward, off_backward);
    let off_analyzed = analyze_acts(&off_acts);
    let def_acts = parse_calc_result(&def, def_forward, def_backward);
    let def_analyzed = analyze_acts(&def_acts);
    (off_analyzed, def_analyzed)
}

pub fn analyze_kakuge_calc_result_text(kr : KakugeCalcResult, off_forward : f64, off_backward : f64,
                                        def_forward : f64, def_backward : f64) -> (AnalyzedActs, AnalyzedActs) {
    let off = CalcResult{ kitaichi : kr.kitaichi, moves : kr.offence, time : kr.time };
    let def = CalcResult{ kitaichi : kr.kitaichi, moves : kr.defence, time : kr.time };

    let off_acts = parse_text(&off.moves_to_str(), off_forward, off_backward);
    let off_analyzed = analyze_acts(&off_acts);
    let def_acts = parse_text(&def.moves_to_str(), def_forward, def_backward);
    let def_analyzed = analyze_acts(&def_acts);
    (off_analyzed, def_analyzed)
}

pub fn analyze_result_text(cr : &CalcResult, forward : f64, backward : f64) -> AnalyzedActs{
    let off_acts = parse_text(&cr.moves_to_str(), forward, backward);
    analyze_acts(&off_acts)
}


///backwardは正
pub fn parse_calc_result(cr : &CalcResult, forward : f64, backward : f64) -> Vec<Act>{
    let rx = Regex::new(r"(?x)
    (?P<first_char>[fbc])((?P<start_pos>-?\d+\.\d+)_)?(?P<frames>\d+)_(?P<move_name>.+)").unwrap();

    let mut vec : Vec<Act> = vec![];

    for (name, percent) in &cr.moves{
        let percent = *percent * 100.0;
        let cap = rx.captures(name).unwrap();
        let strategy = match cap["first_char"].chars().next().unwrap(){
            'f' => StrategyType::Forward,
            'b' => StrategyType::Backward,
            'c' => StrategyType::Crouching,
            _ => unreachable!(),
        };
        let start_pos : f64 = cap.name("start_pos").map(|s| s.as_str().parse().unwrap()).unwrap_or(0.0);
        let move_name = cap["move_name"].to_string();
        let frames : u32 = cap["frames"].parse().unwrap();
        let current_pos = start_pos + match strategy{
            StrategyType::Crouching => 0.0,
            StrategyType::Forward => forward * frames as f64,
            StrategyType::Backward => -backward * frames as f64,
        };
        vec.push(Act{
            frames, start_pos, move_name, percent, strategy, current_pos
        });
    }
    vec
}

///backwardは正
pub fn parse_text(text : &str, forward : f64, backward : f64) -> Vec<Act>{
    let rx = Regex::new(r"(?x)
    (?P<first_char>[fbc])((?P<start_pos>-?\d+\.\d+)_)?(?P<frames>\d+)_(?P<move_name>[^<]+)<(?P<percent>\d+(\.\d+)?)%>").unwrap();

    let mut vec : Vec<Act> = vec![];

    for cap in rx.captures_iter(text){
        let strategy = match cap["first_char"].chars().next().unwrap(){
            'f' => StrategyType::Forward,
            'b' => StrategyType::Backward,
            'c' => StrategyType::Crouching,
            _ => unreachable!(),
        };
        let start_pos : f64 = cap.name("start_pos").map(|s| s.as_str().parse().unwrap()).unwrap_or(0.0);
        let move_name = cap["move_name"].to_string();
        let frames : u32 = cap["frames"].parse().unwrap();
        let percent : f64 = cap["percent"].parse().unwrap();
        let current_pos = start_pos + match strategy{
            StrategyType::Crouching => 0.0,
            StrategyType::Forward => forward * frames as f64,
            StrategyType::Backward => -backward * frames as f64,
        };

        vec.push(Act{
            frames, start_pos, move_name, percent, strategy, current_pos
        });
    }
    vec
}



fn to_pos(pos : f64) -> i32{ (pos * 10.0).round() as i32 }



pub fn analyze_acts(acts : &Vec<Act>) -> AnalyzedActs{
    let mut move_map : BTreeMap<String, (f64, Vec<Act>)> = BTreeMap::new();
    let mut real_pos_map : BTreeMap<i32, (f64, Vec<Act>)> = BTreeMap::new();
    let mut frame_map : BTreeMap<u32, (f64, Vec<Act>)> = BTreeMap::new();
    let mut start_pos_move_map : BTreeMap<i32, (f64, BTreeMap<String, (f64, Vec<Act>)>)> = BTreeMap::new();
    let mut start_pos_map : BTreeMap<i32, (f64, BTreeMap<i32, (f64, Vec<Act>)>)> = BTreeMap::new();

    //let mut wait_real_pos_map : BTreeMap<i32, Vec<u32>> = BTreeMap::new();
    let mut total = 0.0;
    //let mut wait_total = 0.0;
    let mut atk_advance = 0.0;
    let mut atk_advance_plus = 0.0;
    let mut wait_advance = 0.0;
    let mut wait_advance_plus = 0.0;
    let mut total_advance = 0.0;
    let mut total_advance_plus = 0.0;
    for  act in acts{
        let _pos = (act.current_pos * 10.0).round() as i32;

        let (percent, move_map_vec) = move_map.entry(act.move_name.to_string()).or_insert((0.0, Vec::new()));
        *percent += act.percent;
        move_map_vec.push(act.clone());
        let (percent, real_pos_vec) = real_pos_map.entry(to_pos(act.current_pos)).or_insert((0.0, Vec::new()));
        *percent += act.percent;
        real_pos_vec.push(act.clone());

        let (percent, frame_vec) = frame_map.entry(act.frames).or_insert((0.0, Vec::new()));
        *percent += act.percent;
        frame_vec.push(act.clone());

        let (percent, pos_map) = start_pos_move_map.entry(to_pos(act.start_pos)).or_insert((0.0, BTreeMap::new()));
        *percent += act.percent;
        let (percent, vec) = pos_map.entry(act.move_name.to_string()).or_insert((0.0, Vec::new()));
        *percent += act.percent;
        vec.push(act.clone());

        let (percent, pos_map) = start_pos_map.entry(to_pos(act.start_pos)).or_insert((0.0, BTreeMap::new()));
        *percent += act.percent;
        let (percent, vec) = pos_map.entry(to_pos(act.current_pos)).or_insert((0.0, Vec::new()));
        *percent += act.percent;
        vec.push(act.clone());


        if act.move_name == "wait"{
            let advance = act.current_pos * (act.percent / 100.0);
            wait_advance += advance;
            total_advance += advance;
            if 0.0 < advance{
                wait_advance_plus += advance;
                total_advance_plus += advance;
            }
        } else{
            let advance = act.current_pos * (act.percent / 100.0);
            atk_advance += advance;
            total_advance += advance;
            if 0.0 < advance{
                atk_advance_plus += advance;
                total_advance_plus += advance;
            }
        }
        total += act.percent;
    }

    fn sort(vec : &mut Vec<Act>){
        vec.sort_by_key(|item| OrderedFloat(-item.percent));
    }

    let mut move_map_vec : Vec<(String, f64, Vec<Act>)> = Vec::new();
    for (name, (per, mut vec)) in move_map{
        sort(&mut vec);
        move_map_vec.push((name, per, vec));
    }
    move_map_vec.sort_by_key(|(_, per, _)| OrderedFloat(-*per));
    let mut real_pos_map_vec : Vec<(i32, f64, Vec<Act>)> = Vec::new();
    for (pos,(per, mut vec))  in real_pos_map{
        sort(&mut vec);
        real_pos_map_vec.push((pos, per, vec));
    }
    let mut frame_map_vec : Vec<(u32, f64, Vec<Act>)> = Vec::new();
    for (frame,(per, mut vec))  in frame_map{
        sort(&mut vec);
        frame_map_vec.push((frame, per, vec));
    }
    let mut start_pos_move_map_vec : Vec<(i32, f64, Vec<(String, f64, Vec<Act>)>)> = Vec::new();
    for (pos,(per, tree))  in start_pos_move_map{
        let mut inner_vec : Vec<(String, f64, Vec<Act>)> = Vec::new();
        for (name, (in_per, mut vec)) in tree{
            sort(&mut vec);
            inner_vec.push((name, in_per, vec));
        }
        inner_vec.sort_by_key(|(_,per,_)| OrderedFloat(-*per));
        start_pos_move_map_vec.push((pos, per, inner_vec));
    }

    let mut start_pos_map_vec : Vec<(i32, f64, Vec<(i32, f64, Vec<Act>)>)> = Vec::new();
    for (pos,(per, tree))  in start_pos_map{
        let mut inner_vec : Vec<(i32, f64, Vec<Act>)> = Vec::new();
        for (pos, (in_per, mut vec)) in tree{
            sort(&mut vec);
            inner_vec.push((pos, in_per, vec));
        }
        inner_vec.sort_by_key(|(_,per,_)| OrderedFloat(-*per));
        start_pos_map_vec.push((pos, per, inner_vec));
    }
    AnalyzedActs{
        move_map : move_map_vec,
        real_pos_map : real_pos_map_vec,
        frame_map : frame_map_vec,
        start_pos_move_map : start_pos_move_map_vec,
        start_pos_map : start_pos_map_vec,
        total,
        atk_advance, wait_advance, total_advance,
        atk_advance_plus, wait_advance_plus, total_advance_plus,
    }
}