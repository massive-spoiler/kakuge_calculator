use crate::search_for_the_best_pos_test::{best_dual_walk_and_poses};
use crate::chara_gen::{WalkAndPos, WalkType};
use crate::do_battle::{do_battle, do_battle_off};
use crate::{default_scale};
use crate::construct_characters::construct_characters;
use crate::analyze_calc_result::{analyze_kakuge_calc_result_text, analyze_result_text};
use std::time::Instant;


pub fn best_disabled_moves() -> &'static [&'static str]{
    //&["2HP", "2LK", "2MP", "3HK", "4HP", "5HK", "5MK", "6HK", "6LK", "HSoK", "5LK"]
    &["3HK","6HK","6LK","HSoK"]
}

pub fn super_best_disabled_moves() -> &'static [&'static str]{
    &["2HP", "2LK", "2MP", "3HK", "4HP", "5HK", "5MK", "6HK", "6LK", "HSoK", "5LK", "5HP"]
}


pub fn vs_best_test() -> String{
    let scale = default_scale();
    let cs = construct_characters(Some(scale));
    let c = &cs[0];
    let c = c.to_chara_ref(best_disabled_moves());
    let wps = best_dual_walk_and_poses();
    //let enemy = vec![WalkAndPos{ walk_type : WalkType::Forward, rel_pos : 0.0 }];
    let result = do_battle(&wps.vec, &wps.vec, &c, &c, wps.duration, wps.first_distance, scale);
    let f = |speed : u32| speed as f64 / scale as f64;
    let (off, _def) = analyze_kakuge_calc_result_text(result, f(c.forward), f(c.backward),
                                                f(c.forward), f(c.backward));
    let explanation : String = wps.vec.iter().map(|wp| wp.print()).collect::<Vec<String>>().join(", ");
    let off_txt = &[format!("vs_best, offence {}", &explanation)];
    let off_html = off.to_html(off_txt);
    //let def_txt = &[format!("vs_best, forward {}", &explanation)];
    //let def_html = def.to_html(def_txt);
    off_html
}

pub fn vs_best_test_off() -> String{
    let now = Instant::now();
    let scale = default_scale();
    let cs = construct_characters(Some(scale));
    let c = &cs[0];
    let c = c.to_chara_ref(best_disabled_moves());

    let wps = best_dual_walk_and_poses();
    let result = do_battle_off(&wps.vec, &wps.vec, &c, &c, wps.duration, wps.first_distance, scale);
    println!("kitaichi {}", result.kitaichi);
    let f = |speed : u32| speed as f64 / scale as f64;
    let off = analyze_result_text(&result, f(c.forward), f(c.backward));
    let explanation : String = wps.vec.iter().map(|wp| wp.print()).collect::<Vec<String>>().join(", ");
    let off_txt = &[format!("vs_best, offence {}", &explanation)];
    let off_html = off.to_html(off_txt);
    println!("vs_best_test_off {}", (Instant::now() - now).as_secs());
    off_html
}

pub fn to_walks(poses : &[f64]) -> Vec<WalkAndPos>{
    return poses.iter().map(|v| WalkAndPos::new(WalkType::Forward, *v)).collect();
}

pub fn vs_best_test_off1() -> String{
    return vs_best_test_off2("Dict", &[0.0, -0.2,-0.4], &[],"Guile", &[0.0, -0.2, -0.4,-0.6,-0.8], best_disabled_moves(),    50, 2.0)
}

pub fn vs_best_test_off2_rev(def_name : &str, def_poses : &[f64], def_disabled : &[&str], off_name : &str, off_poses : &[f64], off_disabled : &[&str], duration : usize, first_distance : f64) -> String {
    vs_best_test_off2(off_name, off_poses, off_disabled, def_name, def_poses, def_disabled, duration, first_distance)
}

pub fn vs_best_test_off2(off_name : &str, off_poses : &[f64], off_disabled : &[&str], def_name : &str, def_poses : &[f64], def_disabled : &[&str], duration : usize, first_distance : f64) -> String{
    let now = Instant::now();
    let scale = default_scale();
    let cs = construct_characters(Some(scale));

    let off = cs.iter().find(|c| c.name == off_name).unwrap();
    let off = off.to_chara_ref(off_disabled);
    let off_walks = to_walks(off_poses);

    let def = cs.iter().find(|c| c.name == def_name).unwrap();
    let def = def.to_chara_ref(def_disabled);
    let def_walks = to_walks(def_poses);
    //let c = &cs[0];
    //let c = c.to_chara_ref(best_disabled_moves());

    //let wps = best_dual_walk_and_poses();
    let result = do_battle_off(&off_walks, &def_walks, &off, &def, duration, first_distance, scale);
    println!("kitaichi {}", result.kitaichi);
    let f = |speed : u32| speed as f64 / scale as f64;
    let off = analyze_result_text(&result, f(off.forward), f(off.backward));
    let off_explanation : String = off_walks.iter().map(|wp| wp.print()).collect::<Vec<String>>().join(", ");
    let def_explanation : String = def_walks.iter().map(|wp| wp.print()).collect::<Vec<String>>().join(", ");
    let off_txt = &[format!("vs_best, {} {} offence {} {} defence {} {} {}", first_distance, duration, off_name, &off_explanation, def_name, &def_explanation, result.kitaichi)];
    let off_html = off.to_html(off_txt);
    println!("vs_best_test_off {}", (Instant::now() - now).as_secs());
    off_html
}