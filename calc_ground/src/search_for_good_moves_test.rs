use crate::search_for_good_moves::{search_for_good_moves, SFGM};


pub fn search_for_good_moves_test() -> SFGM{

    let scale = crate::default_scale();
    let charas = crate::construct_characters::construct_characters(Some(scale));
    let c1 = charas[0].clone();
    let c2 = c1.clone();

    //1.5にすると止まるな・・・
    search_for_good_moves(&c1, &c2, 1.50, 2.8, 0.1, scale)


}