use regex::Regex;
use serde_json::Value;

///Hitbox Hurtbox などのboxを総称してHitboxとする。確認した限りではHit,Hurt,Pushボックスが有り、Hurtには投げ、打撃、飛び道具の判定がある。
#[derive(Debug)]
pub struct Hitbox{
    pub box_type: String,
    pub box_num: u32,
    pub box_attrs: Vec<String>,
    pub left : f64,
    pub bottom : f64,
    pub width : f64,
    pub height : f64,
}

///type,num の Hitbox が startからendまでのフレームで有効になる
#[derive(Debug)]
pub struct Tickbox{
    pub box_type: String,
    pub box_num: u32,
    pub start : u32,
    pub end : u32,
}

pub fn analyze_sfvdiff(html : &str) -> (Vec<Hitbox>, Vec<Tickbox>, Vec<f64>, u32){

    let hitboxes = {
        //<div id='hurtboxes-0' class='hurt strike projectile box' style='left:-35px;bottom:145px;width:70px;height:40px;'></div>
        let rg = Regex::new(r"(?x)
      <div\sid='(?P<box_name>\w+)boxes-(?P<box_num>\d+)'\s
      class='(?P<box_attr>(\w+\s)+)box'\s
      style='left:(?P<left>[-]?[0-9]+(\.[0-9]+)?)px;bottom:(?P<bottom>[-]?[0-9]+(\.[0-9]+)?)px;
      width:(?P<width>[-]?[0-9]+(\.[0-9]+)?)px;height:(?P<height>[-]?[0-9]+(\.[0-9]+)?)px;'>
      </div>").unwrap();

        let mut hitboxes: Vec<Hitbox> = vec![];

        for cap in rg.captures_iter(html) {
            let sp = cap["box_attr"].split_whitespace();
            let attrs: Vec<String> = sp.map(|s| s.to_string()).collect();

            let hitbox = Hitbox {
                box_type: cap["box_name"].to_string(),
                box_num: cap["box_num"].parse().unwrap(),
                box_attrs: attrs,
                left: cap["left"].parse().unwrap(),
                bottom: cap["bottom"].parse().unwrap(),
                width: cap["width"].parse().unwrap(),
                height: cap["height"].parse().unwrap(),
            };
            //dbg!(&hitbox);
            hitboxes.push(hitbox);
        }
        hitboxes
    };


    let shift_x = {
//    var positions = {
//        "Y": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
//        "ShiftX": [0.0018909999635070562, 0.06515199691057205, 0.13602599501609802, 0.21162399649620056, 0.28905999660491943, 0.3654470145702362, 0.43789801001548767, 0.5035269856452942, 0.5594459772109985, 0.6027690172195435, 0.6306080222129822, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.6405490040779114, 0.6454650163650513, 0.6588000059127808, 0.679252028465271, 0.70551997423172, 0.7363029718399048, 0.7702990174293518, 0.8062090277671814, 0.8427299857139587, 0.8785619735717773, 0.9124019742012024, 0.9429510235786438, 0.9689069986343384, 0.988968014717102, 1.0018349885940552, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585],
//        "X": [0.0018909999635070562, 0.06515199691057205, 0.13602599501609802, 0.21162399649620056, 0.28905999660491943, 0.3654470145702362, 0.43789801001548767, 0.5035269856452942, 0.5594459772109985, 0.6027690172195435, 0.6306080222129822, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.640408992767334, 0.6405490040779114, 0.6454650163650513, 0.6588000059127808, 0.679252028465271, 0.70551997423172, 0.7363029718399048, 0.7702990174293518, 0.8062090277671814, 0.8427299857139587, 0.8785619735717773, 0.9124019742012024, 0.9429510235786438, 0.9689069986343384, 0.988968014717102, 1.0018349885940552, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585, 1.006356954574585]
//    };
        let rg = Regex::new(r#"(?s)var positions = (.*?);"#).unwrap();
        let captures = rg.captures(&html).expect("couldn't find 'positions'");
        let positions = captures.get(1).unwrap().as_str();
        let pos_json: Value = serde_json::from_str(positions).unwrap();
        let shift_x: Vec<f64> = pos_json.as_object().unwrap().get("X").unwrap().as_array().unwrap()
            .into_iter().map(|item| item.as_f64().unwrap()).collect();
        shift_x
    };

    let without_interruption = {
        // <dt class="">Without Interruption</dt>
        //                    <dd><span>34</span></dd>
        let rg = Regex::new(r#"<dt class="[^"]*">Without Interruption</dt>\s*<dd><span[^>]*>(\d+)</span></dd>"#).unwrap();
        let captures = rg.captures(&html).expect("couldn't find 'without interruption'");
        let without_interruption: u32 = captures.get(1).unwrap().as_str().parse().unwrap();
        without_interruption
    };

    let tickboxes = {
        //var ticksBox = { "hurtboxes-2": [0, 70], "hurtboxes-4": [11, 12], "hitboxes-1": [13, 16], "hurtboxes-6": [12, 25], "hitboxes-0": [0, 16], "hurtboxes-0": [0, 70], "hurtboxes-3": [0, 70], "hurtboxes-1": [0, 70], "hurtboxes-5": [7, 12], "pushboxes-0": [0, 70] };
        let rg = Regex::new(r#"(?s)var ticksBox = (.*?);"#).unwrap();
        let captures = rg.captures(&html).expect("couldn't find 'ticksBox'");
        let ticks_box = captures.get(1).unwrap().as_str();
        let tickbox_json: Value = serde_json::from_str(ticks_box).unwrap();
        let obj = tickbox_json.as_object().unwrap();
        let box_rg = Regex::new(r"(\w+)boxes-(\d+)").unwrap();
        let mut vec: Vec<Tickbox> = vec![];
        for (key, value) in obj {
            let cap = box_rg.captures(key).expect("ticksBox:analyzing failed");
            let box_type = cap.get(1).unwrap().as_str().to_string();
            let box_num: u32 = cap.get(2).unwrap().as_str().parse().unwrap();
            let array = value.as_array().unwrap();
            let start = array[0].as_u64().unwrap() as u32;
            let end = array[1].as_u64().unwrap() as u32;
            vec.push(Tickbox { box_type, box_num, start, end });
        }
        vec
    };

    (hitboxes,tickboxes, shift_x, without_interruption)
}