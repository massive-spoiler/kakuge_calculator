use std::collections::{HashMap, HashSet};
use crate::chara_data_structs::Chara;
use regex::Regex;
use ordered_float::OrderedFloat;
use crate::crouching_battle::crouching_battle;
use std::time::Instant;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SFGM{
    pub offence : Vec<MoveSpec>,
    pub defence : Vec<MoveSpec>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MoveSpec{
    pub name : String,
    pub distances : Vec<MoveSpecItem>
}

impl MoveSpec{
    pub fn count(&self) -> u32{
        let mut set : HashSet<OrderedFloat<f64>> = HashSet::new();
        for d in &self.distances{
            set.insert(d.distance.into());
        }
        return set.len() as u32;
    }
    pub fn score(&self) -> f64{ self.distances.iter()
        .fold(0.0, |val,item| val + item.percent) }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MoveSpecItem{
    pub distance : f64,
    pub frame : u32,
    pub percent : f64,
}

pub fn search_for_good_moves(c1 : &Chara, c2 : &Chara, min : f64, max : f64, advance : f64, scale : u32) -> SFGM{
    let mut loop_var = min;
    let mut offence : HashMap<String, MoveSpec> = HashMap::new();
    let mut defence : HashMap<String, MoveSpec> = HashMap::new();

    let regex = Regex::new(r"(\d+)_(.+)").unwrap();
    let to_name_and_frame = |s : &str| -> (String, u32){
        if let Some(caps) = regex.captures(s) {
            (caps[2].to_string(), caps[1].parse().unwrap())
        }
        else{ (s.to_string(), 0) }
    };

    loop{
        let start = Instant::now();
        let distance = loop_var;
        if max < distance{ break; }
        loop_var += advance;

        let table = crouching_battle(c1, c2, 20, distance, scale);
        //kakuge::visualize::print_strs(&table);

        let result = kakuge::calc(&table).unwrap();
        accumulate(&mut offence, result.offence, distance, to_name_and_frame);
        accumulate(&mut defence, result.defence, distance, to_name_and_frame);

        println!("time {} {} < {}", (Instant::now() - start).as_millis(), distance, max);
    }

    SFGM{
        offence : offence.into_iter().map(|(_k,v)| v).collect(),
        defence : defence.into_iter().map(|(_k,v)|v).collect() }
}

fn accumulate(map : &mut HashMap<String, MoveSpec>, r : Vec<(String,f64)>, distance : f64, to_name_and_frame : impl Fn(&str) -> (String,u32)){
    for (title, percent) in r{
        if 0.01 <= percent{
            let (name, frame) = to_name_and_frame(&title);
            let spec = map.entry(name.to_string()).or_insert_with(|| MoveSpec { name, distances: vec![] });
            spec.distances.push(MoveSpecItem { distance, frame, percent })
        }
    }
}