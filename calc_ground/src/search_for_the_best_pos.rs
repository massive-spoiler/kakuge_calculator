use crate::chara_gen::{MoveDefGenerator, WalkAndPos, LaggedWalkingCharaGenerable, WalkType};
use crate::move_def::create_table;
use kakuge::calc::calc_offence;
use crate::chara_data_structs::CharaRef;
use std::time::Instant;

#[derive(Debug)]
pub struct Calced{
    pub kitaichi : f64,
    pub pos : f64,
    pub walk_type : WalkType,
}

pub fn search_for_the_best_pos(chara : &CharaRef, other_walks : &[WalkAndPos],
                               first_distance : f64, rel_pos_min : f64, rel_pos_max : f64, pos_unit : f64,
                               duration : usize, scale : u32,
                               enemy : &mut impl MoveDefGenerator, walk_types : &[WalkType]) -> Vec<Calced>{
    let mut others = other_walks.to_vec();
    let mut rel_pos_mut = rel_pos_min;
    let mut vec : Vec<Calced> = vec![];
    let mut time = Instant::now();
    while rel_pos_mut <= rel_pos_max{
        let rel_pos = rel_pos_mut;
        rel_pos_mut += pos_unit;
        let mut test = |walk_type : WalkType| {
            others.push(WalkAndPos { walk_type, rel_pos: rel_pos });
            let mut lcg = LaggedWalkingCharaGenerable::new(chara, duration, &others);
            let kitaichi = get_kitaichi(&mut lcg, enemy, first_distance, scale);
            others.pop();
            println!("{} {}", rel_pos, (Instant::now() - time).as_secs());
            time = Instant::now();

            vec.push(Calced{
                walk_type,
                pos : first_distance - rel_pos,
                kitaichi,
            });
        };
        for walk_type in walk_types{
            test(*walk_type);
        }
    }
    vec
}

fn get_kitaichi(offence : &mut impl MoveDefGenerator, defence : &mut impl MoveDefGenerator, first_distance : f64, scale : u32) -> f64{
    let table = create_table("hoge", offence, defence, first_distance, scale);
    calc_offence(&table).unwrap().kitaichi
}