use std::time::Instant;
use crate::default_scale;
use crate::construct_characters::construct_characters;
use crate::vs_best_test::best_disabled_moves;
use crate::search_for_the_best_pos_test::best_dual_walk_and_poses;
use crate::chara_gen::LaggedWalkingCharaGenerable;
use crate::move_def::{create_table_test};

pub fn two_attacks_test(atk : &str, def : &str){
    let _now = Instant::now();
    let scale = default_scale();
    let cs = construct_characters(Some(scale));
    let c = &cs[0];
    let c = c.to_chara_ref(best_disabled_moves());

    let wps = best_dual_walk_and_poses();
    let mut off_cg = LaggedWalkingCharaGenerable::new(&c, wps.duration, &wps.vec);
    let mut def_cg = LaggedWalkingCharaGenerable::new(&c, wps.duration, &wps.vec);
    let answer = create_table_test(atk,def, &mut off_cg, &mut def_cg, wps.first_distance, scale);
    dbg!(answer);
}