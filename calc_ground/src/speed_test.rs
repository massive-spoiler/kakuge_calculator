use kakuge::calc;
use rand::Rng;
use std::time::{Instant};

///x:500 y:500 13秒
/// x:100 y:5000 5秒 逆も同じ
pub fn motto_test() {
    let x_len = 5000;
    let y_len = 100;
    let mut rng = rand::thread_rng();

    let mut input : Vec<Vec<String>> = Vec::new();
    let mut first : Vec<String> = Vec::new();
    first.push("test".to_string());
    for i in 0..x_len{
        first.push(format!("x{}", i));
    }
    input.push(first);


    for i in 0..y_len{
        let mut line : Vec<String> = Vec::new();
        line.push(format!("y{}", i));
        for _ in 0..x_len{
            line.push(format!("{}", rng.gen_range(0.0,1.0)));
        }
        input.push(line);
    }

    let start = Instant::now();

    match calc(&input) {
        Ok(_) =>{
            let end = start.elapsed();
            println!("{}.{:03}秒経過しました。", end.as_secs(), end.subsec_nanos() / 1_000_000);

//            let output = r.print("hoge");
//            for l in output{
//                println!("{}",l);
//            }
        },
        Err(e) =>{ println!("error"); println!("{:?}", e); return },
    }
}

//pub fn motto_test() {
//    let a = vec![
//        vec!["test", "P", "S", "R"],
//        vec!["P", "0", "1", "-2"],
//        vec!["S", "-3", "0", "4"],
//        vec!["R", "5", "-6", "0"],
//    ];
//    let a : Vec<Vec<String>> = a.into_iter().map(|v| v.into_iter().map(|s| s.to_string()).collect()).collect();
//    //https://www.princeton.edu/~rvdb/542/lectures/lec8.pdf
//    match calc(&a) {
//        Ok(r) =>{
//            let output = r.print("hoge");
//            for l in output{
//                println!("{}",l);
//            }
//        },
//        Err(e) =>{ println!("error"); println!("{:?}", e); return },
//    }
//}