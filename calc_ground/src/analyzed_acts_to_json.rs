use crate::analyze_calc_structs::{JsTreeNode, AnalyzedActs, Act};

fn fpos(i : i32) -> f64{
    i as f64 / 10.0
}

#[allow(dead_code)]
fn fpos16(i : i16) -> f64{
    i as f64 / 10.0
}

pub fn to_html(ac : AnalyzedActs, lines : &[String]) -> String {
    let vec = ac.to_json_node_vec();
    vec_to_html(lines, &vec)
}

pub fn to_json_node_vec(ac : AnalyzedActs) -> Vec<JsTreeNode> {
    vec![JsTreeNode::text_node(
        format!("total {:.2}", ac.total)),
         JsTreeNode::text_node(
             format!("advance {:.2} wait_advance {:.2} atk_advance {:.2}", ac.total_advance, ac.wait_advance, ac.atk_advance)
         ),
         JsTreeNode::text_node(
            format!("advance {:.2} {:.2} wait {:.2} {:.2} atk {:.2} {:.2}",
                    ac.total_advance_plus, ac.total_advance - ac.total_advance_plus,
                ac.wait_advance_plus, ac.wait_advance - ac.wait_advance_plus,
                ac.atk_advance_plus, ac.atk_advance - ac.atk_advance_plus,
            )
        ),
         JsTreeNode::new(
             format!("攻撃% {}", ac.move_map.len()),
             ac.move_map.into_iter()
                 .map(|(name, percent, vec)| get_move(name, percent, vec)).collect()
         ),
         JsTreeNode::new(
             format!("pos {}", ac.real_pos_map.len()),
             ac.real_pos_map.into_iter()
                 .map(|(pos, percent, vec)| get_real_pos(pos, percent, vec)).collect()),
         JsTreeNode::new(
             format!("フレーム {}", ac.frame_map.len()),
             ac.frame_map.into_iter()
                 .map(|(frame, percent, vec)| get_frame(frame, percent, vec)).collect()),
         JsTreeNode::new(
             format!("StartPos {}", ac.start_pos_map.len()),
             ac.start_pos_move_map.into_iter()
                 .map(|(pos, percent, vec)| get_start_pos_move(pos, percent, vec)).collect()),
         JsTreeNode::new(
             format!("StartPos Pos {}", ac.start_pos_map.len()),
             ac.start_pos_map.into_iter()
                 .map(|(pos, percent, vec)| get_start_pos_pos(pos, percent, vec)).collect()),
    ]
}



fn get_move(name : String, percent : f64, vec : Vec<Act>) -> JsTreeNode{
    JsTreeNode::new(
        format!("{} {:.2}% {}", name, percent, vec.len()),
        vec.into_iter().map(|act| get_node(&act)).collect()
    )
}

fn get_real_pos(pos : i32, percent : f64, vec : Vec<Act>) -> JsTreeNode{
    JsTreeNode::new(
        format!("{:.1} {:.2}% {}", fpos(pos), percent, vec.len()),
        vec.into_iter().map(|act| get_node(&act)).collect()
    )
}

fn get_frame(frame : u32, percent : f64, vec : Vec<Act>) -> JsTreeNode{
    JsTreeNode::new(
        format!("{} {:.2}% {}", frame, percent, vec.len()),
        vec.into_iter().map(|act| get_node(&act)).collect()
    )
}

fn get_start_pos_move(pos : i32, percent : f64, vec : Vec<(String, f64, Vec<Act>)>) -> JsTreeNode{
    JsTreeNode::new(
        format!("{:.1} {:.2}% {}", fpos(pos), percent, vec.len()),
        vec.into_iter().map(|(name, per, vec)| get_move(name, per, vec)).collect()
    )
}

fn get_start_pos_pos(pos : i32, percent : f64, vec : Vec<(i32, f64, Vec<Act>)>) -> JsTreeNode{
    JsTreeNode::new(
        format!("{:.1} {:.2}% {}", fpos(pos), percent, vec.len()),
        vec.into_iter().map(|(pos, per, vec)| get_real_pos(pos, per, vec)).collect()
    )
}



fn get_node(act : &Act) -> JsTreeNode{
    JsTreeNode::text_node(format!("{:.1}% {} {}F {:.1} -> {:.1}", act.percent, &act.move_name, act.frames, act.start_pos, act.current_pos))
}


fn vec_to_html(lines : &[String], nodes : &Vec<JsTreeNode>) -> String{
    let mut s = String::new();
    s.push_str(r#"
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>HTML 5 complete</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.2/themes/default/style.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.2/jstree.min.js"></script>
</head>
<body>"#);
    for line in lines{
        s.push_str(&format!("<div>{}</div>", line));
    }

    s.push_str(r#"<div id="tree1">
</div>
<script>
$(function(){$('#tree1').jstree({"plugins" : ["wholerow"],'core' : {
'themes':{'stripes':true},
'data' : "#);
    s.push_str(&serde_json::to_string(nodes).unwrap());
    s.push_str(&r#"
}});})();
</script>
</body>
</html>"#);
    s
}