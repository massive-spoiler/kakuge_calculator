use std::collections::{HashMap};
use crate::analyzed_acts_to_json::{to_json_node_vec, to_html};

#[derive(Debug, Clone)]
pub enum StrategyType{
    Forward,
    Backward,
    Crouching,
}

#[derive(Debug, Clone)]
pub struct Act{
    pub frames : u32,
    pub start_pos : f64,
    pub move_name : String,
    pub percent : f64,
    pub strategy : StrategyType,
    pub current_pos : f64,
}

pub struct AnalyzedActs{
    pub move_map : Vec<(String, f64, Vec<Act>)>,
    pub real_pos_map : Vec<(i32, f64, Vec<Act>)>,
    pub frame_map : Vec<(u32, f64, Vec<Act>)>,
    pub start_pos_move_map : Vec<(i32, f64, Vec<(String, f64, Vec<Act>)>)>,
    pub start_pos_map : Vec<(i32, f64, Vec<(i32, f64, Vec<Act>)>)>,

    pub total : f64,
    pub atk_advance : f64,
    pub atk_advance_plus : f64,
    pub wait_advance : f64,
    pub wait_advance_plus : f64,
    pub total_advance : f64,
    pub total_advance_plus : f64,
}

///これはこのクレートのどこにでも書けるようだ。structをuseすればどこに書いてあっても使える。
/// おそらくimplブロックはmodと同レベルであり、structをスコープに入れることでimplもuseされるのだろう。
/// modは専用のファイルで宣言しなければならないのに、やはりおかしなことではある・・・
impl AnalyzedActs{
    pub fn to_json_node_vec(self) -> Vec<JsTreeNode>{
        to_json_node_vec(self)
    }

    pub fn to_html(self, lines : &[String]) -> String{
        to_html(self, lines)
    }
}

#[derive(Serialize, Deserialize)]
pub struct JsTreeNode{
    pub text : String,
    pub state : HashMap<String, String>,
    pub children : Vec<JsTreeNode>,
}

impl JsTreeNode{
    pub fn text_node(text : String) -> JsTreeNode{
        JsTreeNode{ text, children : vec![], state : HashMap::new() }
    }

    pub fn new(text : String, children : Vec<JsTreeNode>) -> JsTreeNode{
        JsTreeNode{
            text, children, state : HashMap::new(),
        }
    }

    pub fn new_opened(text : String, children : Vec<JsTreeNode>) -> JsTreeNode{
        let mut map = HashMap::new();
        map.insert("opened".to_string(), "true".to_string());
        JsTreeNode{
            text, children, state : map,
        }
    }
}