use crate::chara_gen::{CrouchingCharaGenerable, LaggedCharaGenerable,   LaggedForwardCharaGenerable};
use crate::move_def::{create_table};
use std::time::Instant;
use crate::disabled_moves;


pub fn crouching_battle_test() {
    let scale = crate::default_scale();
    let charas = crate::construct_characters::construct_characters(Some(scale));
    let c = charas[0].clone();
    let c2 = c.clone();
    //let c1_moves = disable_moves(&c, &[]);
    let c = c.to_chara_ref(disabled_moves());
    let c2 = c2.to_chara_ref(disabled_moves());

    let mut ops1 = CrouchingCharaGenerable::new(&c, 20, 0.0);
    let mut ops2 = CrouchingCharaGenerable::new(&c2, 20, 0.0);

    let table = create_table("ct", &mut ops1, &mut ops2, 2.0, scale);
    let lines = kakuge::calc(&table).unwrap().print("hoge");
    for line in lines{
        println!("{}", line);
    }
}

pub fn lagged_battle_test(){
    let start = Instant::now();
    let scale = crate::default_scale();
    let charas = crate::construct_characters::construct_characters(Some(scale));
    let c = charas[0].clone();
    let c2 = c.clone();
    let c = c.to_chara_ref(&["2LK",  "3HK", "HSoK", "5LK", "2LP","2MP"]);
    let c2 = c2.to_chara_ref(&["2LK", "2LP", "3HK","5LK", "HSoK", "2MP"]);
    //let c = c.to_chara_ref(&[]);
    //let c2 = c2.to_chara_ref(&[]);

    //let mut crouch = CrouchingCharaGenerable::new(&c2, 20,
      //                                          0.0);
    let mut lagged = LaggedCharaGenerable::new(&c, 20,
                                             -0.2, 0.7, 0.2);
    let mut lagged2 = LaggedCharaGenerable::new(&c2, 20,
                                               -0.2, 0.7, 0.2);


    let table = create_table("ct", &mut lagged, &mut lagged2, 2.3, scale);

    println!("of_len {} def_len {}", table.len(), table[0].len());

    let end = Instant::now();
    println!("time {} {}", (end - start).as_secs(), (end-start).as_millis());
    let start = Instant::now();
    let of = kakuge::calc::calc_offence(&table);
    println!("time offence {}", (Instant::now() - start).as_millis());
    let of = of.unwrap();
    println!("kitaichi {}", of.kitaichi);
    println!("{}", of.moves_to_str());

    let start = Instant::now();
    let de = kakuge::calc::calc_defence(&table);
    println!("time defence {}", (Instant::now() - start).as_millis());
    let de = de.unwrap();
    println!("kitaichi {}", de.kitaichi);
    println!("{}", de.moves_to_str());
    //kakuge::visualize::print_strs(&table);
}

pub fn moving_battle_test(){
    let start = Instant::now();
    let scale = crate::default_scale();
    let charas = crate::construct_characters::construct_characters(Some(scale));
    let c = charas[0].clone();
    let c2 = c.clone();
    let c = c.to_chara_ref(&["2LK",  "3HK", "HSoK", "5LK", "2LP","2MP"]);
    let c2 = c2.to_chara_ref(&["2LK", "2LP", "3HK","5LK", "HSoK", "2MP"]);
    //let c = c.to_chara_ref(&[]);
    //let c2 = c2.to_chara_ref(&[]);

    //let mut crouch = CrouchingCharaGenerable::new(&c2, 20,
    //                                          0.0);
    let mut lagged = LaggedForwardCharaGenerable{ chara : &c, duration : 20, rel_poses : &[-0.2,-0.4,-0.6] };
    let mut lagged2 = LaggedForwardCharaGenerable{ chara : &c2, duration : 20, rel_poses : &[-0.2,-0.4,-0.6] };


    let table = create_table("ct", &mut lagged, &mut lagged2, 2.0, scale);

    println!("of_len {} def_len {}", table.len(), table[0].len());

    let end = Instant::now();
    println!("time {} {}", (end - start).as_secs(), (end-start).as_millis());
    let start = Instant::now();
    let of = kakuge::calc::calc_offence(&table);
    println!("time offence {}", (Instant::now() - start).as_millis());
    let of = of.unwrap();
    println!("kitaichi {}", of.kitaichi);
    println!("{}", of.moves_to_str());

    let start = Instant::now();
    let de = kakuge::calc::calc_defence(&table);
    println!("time defence {}", (Instant::now() - start).as_millis());
    let de = de.unwrap();
    println!("kitaichi {}", de.kitaichi);
    println!("{}", de.moves_to_str());
}