
extern crate rand;
extern crate regex;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

mod data_loader;
mod analyze_sfvdiff;
mod chara_data_structs;
mod test_hitbox;
mod construct_characters;
mod attack_data;
pub mod search_for_good_moves;
mod move_def;
pub mod chara_gen;
mod crouching_battle;
pub mod analyze_calc_result;
pub mod analyze_calc_structs;
mod analyzed_acts_to_json;
pub mod search_for_the_best_pos;
pub mod do_battle;

pub mod speed_test;
pub mod search_for_good_moves_test;
pub mod battle_tests;
pub mod analyze_calc_test;
pub mod search_for_the_best_pos_test;
pub mod vs_forward_test;
pub mod vs_best_test;
pub mod two_attacks_test;

pub fn default_scale() -> u32{ 10_0000 }
pub fn disabled_moves() -> &'static [&'static str]{
    &["2HP","2LK","3HK", "HSoK"]
}