use super::data_loader::load_file;
use crate::chara_data_structs::{CharaJson, Chara, MoveJson, Move, MinusFrame};
use serde_json::Value;
use crate::data_loader::load_folder_file;
use json5;


pub fn construct_characters(scale : Option<u32>) -> Vec<Chara>{
    let scale : u32 = scale.unwrap_or(100_000);
    let charas_text = load_file("characters.json").expect("failed to load 'characters.json'");
    let cs : Value = json5::from_str(&charas_text).unwrap();

    let names : Vec<String> = cs.as_array().unwrap().into_iter().map(|name| name.as_str().unwrap().to_string()).collect();
    let charas : Vec<Chara> = names.into_iter().map(|name|{
        let chara_text = load_folder_file(&name, "character.json").unwrap();
        //dbg!(&chara_text);
        let chara_json : CharaJson = json5::from_str(&chara_text).unwrap();
        construct_chara(chara_json, scale)
    }).collect();
    charas
}

fn construct_chara(c : CharaJson, scale : u32) -> Chara{
    fn adj(v : f64, scale : u32) -> u32{
        (v * scale as f64) as u32
    }

    let name = c.name.to_string();
    Chara{
        name : c.name,
        forward : adj(c.forward, scale),
        backward : adj(c.backward, scale),
        stand : adj(c.stand, scale),
        crouch : adj(c.crouch, scale),
        stand_throw : adj(c.stand_throw, scale),
        crouch_throw : adj(c.crouch_throw, scale),
        moves : c.moves.into_iter().map(|m| construct_move(&name, m, scale)).collect(),
    }
}

fn construct_move(chara_name : &str, m : MoveJson, scale : u32) -> Move{
    use crate::chara_data_structs::DamageItem;
    let scaled = |val : f64|{ (val * scale as f64) as u32 };
    let scaled_i = |val : f64|{ (val * scale as f64) as i32 };



    let to_minus_frames = |s : Value| -> Vec<MinusFrame>{
        return match s{
            Value::Number(v) => vec![MinusFrame{ frame : v.as_u64().unwrap() as u32, upper_distance : 0 }],
            Value::Array(array) =>{
                let to_minus_frame = |a : &Vec<Value>| -> MinusFrame{
                    MinusFrame{ upper_distance : scaled(a[0].as_f64().unwrap()), frame : a[1].as_u64().unwrap() as u32 }
                };
                let vec : Vec<MinusFrame> = array.into_iter().map(|item| to_minus_frame(item.as_array().unwrap())).collect();

                //先に出てくる順に調べるようにすると、「遠いと大ダメージだが近いとダメージ減」も「近いと大ダメージだが遠いとダメージ減」も表せる。
                //データ記述者がどう比べるかを理解している必要はあるが・・・
                //とにかく出てくる順番は重要なのでソートしたりしない
                vec
            },
            _ => unreachable!(),
        };
    };

    let to_damage_items = |s : Value| -> Vec<DamageItem>{
        fn to_damage_item(m : MinusFrame) -> DamageItem{ DamageItem{ upper_distance : m.upper_distance, damage : m.frame }}
        to_minus_frames(s).into_iter().map(|m| to_damage_item(m)).collect()
    };

    let to_punish = |op : Option<Value>, damages : &Vec<DamageItem>| -> Vec<DamageItem>{
        match op {
            Some(s) => s.as_bool().map(|_| damages.to_vec()).unwrap_or_else(|| { to_damage_items(s) }),
            None => Vec::new(),
        }
    };

    fn ref_file_name(m : &MoveJson) -> String{
        m.ref_file_name.as_ref().unwrap_or(&m.name).to_string()
    }

    let file = format!("{}_{}.html",chara_name, ref_file_name(&m));
    println!("{}",&file);

    let sfvdiff = crate::data_loader::load_folder_file(chara_name, &file).expect(&format!("couldn't find {}", &file));
    let (hitboxes, tickboxes, shifts, without_interruption) =
        crate::analyze_sfvdiff::analyze_sfvdiff(&sfvdiff);
    let attack_data = crate::attack_data::construct_attack_data(hitboxes, tickboxes, shifts, without_interruption as usize, scale);
    let damages = to_damage_items(m.damages);
    let punish = to_punish(m.punish, &damages);
    Move{
        name : m.name,
        attack_level : m.attack_level,
        is_low : m.is_low,
        crouching : m.crouching,
        whiff_on_crouch : m.whiff_on_crouch,
        damages,
        counter_damages : to_damage_items(m.counter_damages),
        guard_damages : to_damage_items(m.guard_damages),
        whiff : m.whiff,
        guard : m.guard.map(|g| to_minus_frames(g)).unwrap_or(vec![]),
        punish,
        guard_back : m.guard_back.map(|g| scaled_i(g)),
        attack_data : attack_data,
        special_punish_pos_diff : m.special_punish_pos_diff.map(|g| scaled_i(g)),
    }
}