use crate::attack_data::{HitFrame, HurtFrame, Rect};
use crate::chara_data_structs::{ Move, MinusFrame, DamageItem, CharaRef};

pub enum RectTest{
    TooFar,
    False,
    True
}

///ニュートラルはいらないと思う。
#[derive(Debug, Clone)]
pub enum WalkHistoryState{
    Forward, Backward, Crouched
}


fn test_rect(l : &Rect, r : &Rect, distance : u32) -> RectTest{
    let distance = distance as i32;

    if distance <= l.right + r.right{
        if r.top < l.bottom || l.top < r.bottom{
            return RectTest::False;
        }
        else{
            return RectTest::True;
        }
    }
    else{
        return RectTest::TooFar;
    }
}

///攻撃があたっていた場合、その時のダメージ距離を返す
///ダメージ距離は自分の位置は技を出す前、相手はくらった位置で計算する。
///「通常の場合コンボが成立する距離」を入力しているので、自分は変わらないが相手の位置分計算がずれる。
pub fn test_hitbox(hit_frame : &HitFrame, hurt_frame : &HurtFrame, distance : u32) -> Option<u32>{
    fn get_len(distance : u32, position : i32) -> u32{ (distance as i32 - position).max(0) as u32 }

    for hit in &hit_frame.hitboxes {
        match hurt_frame.hurtboxes.get(0){
            Some(hurt) =>{
                match test_rect(hit, hurt, distance) {
                    RectTest::TooFar =>{ return None; },
                    RectTest::False =>{},
                    RectTest::True => return Some(get_len(distance, hurt_frame.position)),
                }
                for hurt in &hurt_frame.hurtboxes[1..] {
                    match test_rect(hit, hurt, distance) {
                        RectTest::TooFar => { break; },//hitとhurtのrightを比べて大きい順に調べていけば即Noneを返せるが・・・見合うだろうか
                        RectTest::False => {},
                        RectTest::True => return Some(get_len(distance, hurt_frame.position)),
                    }
                }
            }
            None =>{ return None; }
        }
    }
    return None;
}

///framesがなければhistoryいっぱいの長さのnatural_posのvecを返す
fn get_natural_pos(history : &[WalkHistoryState], frames : Option<usize>, forward : u32, backward : u32) -> Vec<i32>{
    let mut pos : i32 = 0;
    let mut vec : Vec<i32> = vec![];
    let len = frames.unwrap_or(history.len());
    for item in &history[0..len]{
        pos = match item{
            WalkHistoryState::Crouched => pos,
            WalkHistoryState::Forward => pos + forward as i32,
            WalkHistoryState::Backward => pos - backward as i32,
        };
        vec.push(pos);
    }
    vec
}

///相手の攻撃が始まると後ろに下がろうとしても下がれなくなるので位置を修正する。かなり事前最適化している・・・
///prior_frames: 敵の攻撃がprior_frames分自分より先行している。先行してない場合0
fn correct_history(history : &[WalkHistoryState],
                   prior_frames : u32, hit_end : u32, forward : u32, backward : u32) -> Vec<i32>{

    let len = history.len();
    //hit_startは敵の攻撃が始まるフレーム
    let hit_start = (len as i32 - prior_frames as i32).max(0) as usize;
    let hit_start = len.min(hit_start);

    let mut r : Vec<i32> = get_natural_pos(history, Some(hit_start), forward, backward);
    if len == hit_start{ return r; }

    let hit_end = (len as i32 - prior_frames as i32 + hit_end as i32).max(0) as usize;
    let hit_end = len.min(hit_end);
    let mut pos : i32 = r.last().map(|i| *i).unwrap_or(0);
    for history_item in &history[hit_start..hit_end]{
        match history_item{
            WalkHistoryState::Backward | WalkHistoryState::Crouched => r.push(pos),
            WalkHistoryState::Forward =>{
                pos += forward as i32;
                r.push(pos);
            }
        }
    }
    if len == hit_end{ return r; }
    for history_item in &history[hit_end..len]{
        match history_item{
            WalkHistoryState::Crouched => r.push(pos),
            WalkHistoryState::Backward =>{
                pos -= backward as i32;
                r.push(pos);
            }
            WalkHistoryState::Forward =>{
                pos += forward as i32;
                r.push(pos);
            }
        }
    }
    return r;
}

pub enum TestHitboxRet{
    ///ダメージ距離。このシミュレーションではヒット時には立っている。
    Hit(u32),
    ///ガード時は立ちガードとしゃがみガードがあるので、しゃがんでいるかどうかも返す
    Guard(u32, bool),
    NoHit,
}

///atk_posはhistoryのlastのpos rightは攻撃のリーチ walk中のやられ判定は長方形なのでrightとだけ判定すればOKのはず
pub fn test_hitbox_with_walk_history(right : i32, atk_pos : i32, def_chara : &CharaRef,
                                     m : &Move, item : &WalkHistoryState,
                                     pos : i32, first_distance : u32) -> TestHitboxRet{
    let dd = (first_distance as i32 - atk_pos - pos).max(0) as u32;
    let body_box = match item{
        WalkHistoryState::Forward | WalkHistoryState::Backward => def_chara.stand,
        WalkHistoryState::Crouched => def_chara.crouch,
    } as i32;
    if dd as i32 <= body_box + right{
        match item{
            WalkHistoryState::Backward =>{
                if m.is_low{ return TestHitboxRet::Hit(dd); }
                else{ return TestHitboxRet::Guard(dd, false); }
            }
            WalkHistoryState::Crouched =>{
                if m.whiff_on_crouch{ return TestHitboxRet::NoHit; }
                return TestHitboxRet::Guard(dd, true);
            }
            WalkHistoryState::Forward =>{ return TestHitboxRet::Hit(dd ); }
        }
    }
    else{ return TestHitboxRet::NoHit; }
}

//fn get_rev(vec : &Vec<WalkHistoryItem>, rev_index : usize) -> Option<&WalkHistoryItem>{
//    let hoge = vec.len() as isize - rev_index as isize;
//    let huga = hoge as usize;
//    vec.get(huga)
//}

///ガードされた時、相手は技の途中ではないのでddとdistanceは同じ
fn get_guard_damage(distance : u32, atk_chara : &CharaRef, m : &Move, def_chara : &CharaRef, def_hurt : u32, def_is_crouch : bool) -> i32{
    fn get_minus_frame(dd : u32, guard : &Vec<MinusFrame>, def_hurt : u32) -> Option<u32>{
        for minus in guard{
            if minus.upper_distance == 0 || dd <= minus.upper_distance + def_hurt{
                return Some(minus.frame)
            }
        }
        return None;
    }

    let dd = distance;
    let guard_damage =
        if def_is_crouch && m.whiff_on_crouch{ 0 }
        else{ get_damage(dd, &m.guard_damages, def_hurt) };

    if let Some(minus) = get_minus_frame(dd, &m.guard, def_hurt){
        let new_distance = (distance as i32 - m.moved_position() + m.guard_back.unwrap()).max(0) as u32;
        if let Some(punish_damage) = punish(def_chara, m, minus, new_distance, atk_chara){
            return guard_damage - punish_damage as i32;
        }
        else{ return guard_damage; }
    }
    return guard_damage;
}

///DamageItemは相手の立ってる状態に対して攻撃して当たる技の距離を入力しているので、standを足す必要がある。
fn get_damage(dd : u32, vec : &Vec<DamageItem>, hurt : u32) -> i32{
    for item in vec{
        if item.upper_distance == 0 || dd <= item.upper_distance + hurt{
            return item.damage as i32;
        }
    }
    unreachable!()
}

enum AtkWalkingOppo{
    Hit(i32),
    NoHit,
    BeforeHistory,
    AfterHistory
}

///ヒットかガードならダメージを返す
/// atk_frame: 攻撃が当たるフレーム。相手の攻撃開始フレームを0として、それより前ならばマイナス
fn attack_for_walking_opponent(
    atk_chara : &CharaRef, atk_frame : i32, atk_pos : i32, hit : &HitFrame, def_chara : &CharaRef, m : &Move,
    walk_history : &[WalkHistoryState], pos_history : &Vec<i32>, first_distance : u32) -> AtkWalkingOppo{
    if atk_frame < 0{
        let index = walk_history.len() as i32 + atk_frame;
        if 0 <= index {
            let index = index as usize;
            let item = &walk_history[index];
            let pos = pos_history[index];
            match test_hitbox_with_walk_history(hit.right, atk_pos, def_chara, m, item, pos, first_distance) {
                TestHitboxRet::Hit(dd) => return AtkWalkingOppo::Hit(get_damage(dd, &m.damages, def_chara.stand)),
                TestHitboxRet::Guard(dd, crouching) => {
                    let hurt = if crouching { def_chara.crouch } else { def_chara.stand };
                    return AtkWalkingOppo::Hit(get_guard_damage(dd, atk_chara, m, def_chara, hurt, crouching))
                },
                TestHitboxRet::NoHit => return AtkWalkingOppo::NoHit,
            }
        }
        else {
            //walk_historyより前である・・・これは本来処理しようがないのでErrorかと思うが、ある意味想定された答えでもあるのでenumでもいいんじゃないか。
            return AtkWalkingOppo::BeforeHistory;
        }
    }
    return AtkWalkingOppo::AfterHistory;
}

///ボタンを押した後、ヒットしなかった場合ひとまずしゃがみガードすることにする。そこに当たるかどうか。
/// この場合相手が動かないのでddとdistanceは同じである。
fn attack_for_stopping_opponent(right : i32, distance : u32, atk_chara : &CharaRef, m : &Move, def_chara : &CharaRef) -> Option<i32>{
    //if m.whiff_on_crouch{ return None; }
    let hurt = def_chara.crouch;
    if distance as i32 <= right + hurt as i32{ Some(get_guard_damage(distance, atk_chara, m, def_chara, hurt, true)) }
    else{ None }
}


fn simulate_atk(atk_chara : &CharaRef, atk_frame_num: i32, atk_pos: i32, atk_hit: &HitFrame, m: &Move,
                def_move : &Move, def_chara: &CharaRef, def_history: &Vec<WalkHistoryState>, def_pos_history: &Vec<i32>,
                first_distance: u32) -> Option<i32> {
    let def_atk_data = &def_move.attack_data;
    let hurt_frames = &def_move.attack_data.hurt_frames;
    let def_hurt_len = hurt_frames.len();
    let distance = (first_distance as i32 - get_last_pos(def_pos_history) - atk_pos).max(0) as u32;

    if atk_frame_num < 0 {
        if let AtkWalkingOppo::Hit(damage) = attack_for_walking_opponent(
            atk_chara,atk_frame_num, atk_pos, atk_hit, def_chara, m, def_history, def_pos_history, first_distance) {
            return Some(damage);
        }
        else{ return None; }
    }
    else if atk_frame_num < def_hurt_len as i32{
        if m.whiff_on_crouch && def_move.crouching{ return None; }
        let hurt = if def_move.crouching{ def_chara.crouch } else{ def_chara.stand };
        let atk_frame_num = atk_frame_num as usize;
        if let Some(dd) = test_hitbox(
            &atk_hit, &hurt_frames[atk_frame_num], distance) {

            if atk_frame_num < def_atk_data.hit_start as usize{
                return Some(get_damage(dd,&m.counter_damages, hurt));
            }
            else{
                return Some(get_damage(dd, &m.damages, hurt));
            }
        }
        else{ return None; }
    } else {
        //攻撃終了後はその場で防御している感じでいいんじゃないかと思う。
        if let Some(damage) = attack_for_stopping_opponent(atk_hit.right, distance, atk_chara, m, def_chara) {
            return Some(damage);
        }
        else{ return None; }
    }
}

fn get_last_pos(history : &Vec<i32>) -> i32{
    history.last().map(|his| *his).unwrap_or(0)
}

pub fn test_former_attack(
    f_chara : &CharaRef, f_move : &Move, f_history : &Vec<WalkHistoryState>,  prior_frames : u32,
    l_chara : &CharaRef, l_move : &Move, l_history : &Vec<WalkHistoryState>,  first_distance : u32) -> Option<i32> {
    let former = &f_move.attack_data;
    let latter = &l_move.attack_data;

    let l_pos_history = &correct_history(l_history, prior_frames, former.hit_end, l_chara.forward, l_chara.backward);
    let f_pos_history = &correct_history(f_history, 0, latter.hit_end, f_chara.forward, f_chara.backward);


    if let Some(d) = test_hitboxes(f_chara, f_move, f_history, f_pos_history, prior_frames,
                  l_chara, l_move, l_history, l_pos_history,  first_distance){
        return Some(d);
    }

    let prior_frames = prior_frames as i32;
    //スカ確(whiff_punish)をチェック
    let f_end = f_move.attack_data.hurt_frames.len() as i32 - prior_frames;
    let l_end = l_move.attack_data.hurt_frames.len() as i32;

    let distance = first_distance as i32 - get_last_pos(f_pos_history) - get_last_pos(l_pos_history);

    fn test_punish(punisher : &CharaRef, m : &Move, end_diff : u32, distance : u32, def_chara : &CharaRef) -> Option<u32>{
        if let Some(frame) = m.whiff{
            let plus = end_diff.min(frame);
            return punish(punisher, m, plus, distance, def_chara);
        }
        else{ return None; }
    }

    if f_end < l_end{
        return test_punish(f_chara, l_move, (l_end - f_end) as u32, (distance - f_move.moved_position()).max(0) as u32, l_chara).map(|d| d as i32);
    }
    else if l_end < f_end{
        return test_punish(l_chara, f_move, (f_end - l_end) as u32, (distance - l_move.moved_position()).max(0) as u32, f_chara).map(|d| -(d as i32));
    }
    else{ return None; }
}

fn test_hitboxes(f_chara: &CharaRef, f_move: &Move, f_history: &Vec<WalkHistoryState>, f_pos_history: &Vec<i32>, prior_frames: u32,
                 l_chara: &CharaRef, l_move: &Move, l_history: &Vec<WalkHistoryState>, l_pos_history: &Vec<i32>, first_distance: u32) -> Option<i32> {
    let former = &f_move.attack_data;
    let latter = &l_move.attack_data;

    let mut f_index: usize = 0;
    let mut l_index: usize = 0;
    loop {
        let f_hit: Option<&HitFrame> = former.hit_frames.get(f_index);
        let l_hit: Option<&HitFrame> = latter.hit_frames.get(l_index);

        if f_hit.is_none() && l_hit.is_none() { return None; }

        //ノイズが多すぎるのでクロージャにまとめる
        let simulate_atk_f = || {
            let f_hit = f_hit.unwrap();
            let f_frame_num = f_hit.frame_num as i32 - prior_frames as i32;
            let f_pos = get_last_pos(f_pos_history);
            simulate_atk(f_chara, f_frame_num, f_pos, f_hit, f_move,
                         l_move, l_chara, l_history, l_pos_history, first_distance)
        };
        let simulate_atk_l = || {
            let l_hit = l_hit.unwrap();
            let l_frame_num = l_hit.frame_num as i32 + prior_frames as i32;
            let l_pos = get_last_pos(l_pos_history);
            simulate_atk(l_chara, l_frame_num, l_pos, l_hit, l_move,
                         f_move, f_chara, f_history, f_pos_history, first_distance)
        };

        if f_hit.is_some() && l_hit.is_none() {
            f_index += 1;
            if let Some(damage) = simulate_atk_f() {
                return Some(damage);
            }
        } else if f_hit.is_none() && l_hit.is_some() {
            l_index += 1;
            if let Some(damage) = simulate_atk_l() {
                return Some(-damage);
            }
        } else if f_hit.is_some() && l_hit.is_some() {
            let f_hit = f_hit.unwrap();
            let l_hit = l_hit.unwrap();
            let f_frame_num = f_hit.frame_num as i32 - prior_frames as i32;
            let l_frame_num = l_hit.frame_num as i32;

            //フレームが早い方から攻撃開始
            if f_frame_num < l_frame_num {
                f_index += 1;
                if let Some(damage) = simulate_atk_f() {
                    return Some(damage);
                }
            } else if l_frame_num < f_frame_num {
                l_index += 1;
                if let Some(damage) = simulate_atk_l() {
                    return Some(-damage);
                }
            } else {
                f_index += 1;
                l_index += 1;
                if f_move.attack_level != 0 && l_move.attack_level != 0 && f_move.attack_level != l_move.attack_level {
                    if l_move.attack_level < f_move.attack_level {
                        let f_result = simulate_atk_f();
                        if f_result.is_some() { return f_result; }
                        let l_result = simulate_atk_l();
                        if l_result.is_some() { return l_result.map(|d| -d); }
                    } else {
                        let l_result = simulate_atk_l();
                        if l_result.is_some() { return l_result.map(|d| -d); }
                        let f_result = simulate_atk_f();
                        if f_result.is_some() { return f_result; }
                    }
                } else {
                    let f_result = simulate_atk_f();
                    let l_result = simulate_atk_l();

                    //相打ちの後はコンボも変わるしいろいろ問題があるが、ひとまず無視したい
                    if f_result.is_some() && l_result.is_some() {
                        return Some(f_result.unwrap() - l_result.unwrap());
                    } else if f_result.is_some() {
                        return f_result;
                    } else if l_result.is_some() {
                        return l_result.map(|d| -d);
                    }
                }
            }
        }
    }
}

///lが勝ったらプラス、rが勝ったらマイナス、どっちも当たらなければNone
pub fn test_two_attacks(l_chara : &CharaRef, l_move : &Move, l_history : &Vec<WalkHistoryState>,  l_prior_frames : i32,
    r_chara : &CharaRef, r_move : &Move, r_history : &Vec<WalkHistoryState>,  first_distance : u32) -> Option<i32> {
    let l_end = -l_prior_frames + l_move.attack_data.hurt_frames.len() as i32;
    if l_end < 0{
        //自分の攻撃が終わるまでに相手の攻撃が始まらない場合、その後何をするも自由なので根本的にその後の展開に点数を付けるのは無理。スカ確が取れる場合以外は。

        return attack_vs_whiff_punish(l_chara, l_move, l_history, l_prior_frames as u32, r_chara, r_history, first_distance)
    }
    let r_end = r_move.attack_data.hurt_frames.len() as i32;
    if r_end < -l_prior_frames{
        return attack_vs_whiff_punish(r_chara, r_move, r_history, (-l_prior_frames) as u32, l_chara, l_history, first_distance).map(|d| -d)
    }


    if 0 <= l_prior_frames {
        test_former_attack(l_chara, l_move, l_history,
                           l_prior_frames as u32, r_chara, r_move, r_history, first_distance)
    }
    else {
        test_former_attack(r_chara, r_move, r_history,
                           -l_prior_frames as u32, l_chara, l_move, l_history, first_distance).map(|d| -d)
    }
}

///一方は攻撃するが、一方は攻撃せず待つ。相手の攻撃が空振りし、十分に隙があればお仕置きする。
/// atkが勝てばプラス、お仕置き成功ならマイナス。
/// prior_framesは攻撃側が早く動くなら意味があるが、待ち側より遅く動き出しても相手はじっと待っているだけなので意味をなさないからu32。遅く動き出す場合は0を入れる。
pub fn attack_vs_whiff_punish(
    atk_chara : &CharaRef, m : &Move, atk_history : &Vec<WalkHistoryState>,  prior_frames : u32,
    def_chara : &CharaRef, def_history : &[WalkHistoryState],first_distance : u32) -> Option<i32> {
    let atk_data = &m.attack_data;

    let def_pos_history = correct_history(def_history, prior_frames, atk_data.hit_end, def_chara.forward, def_chara.backward);
    let atk_pos_history: Vec<i32> = get_natural_pos(atk_history, None, atk_chara.forward, atk_chara.backward);
    //let atk_pos_history : Vec<i32> = atk_history.iter().map(|item| item.natural_pos).collect();

    let atk_pos = get_last_pos(&atk_pos_history);
    let prior_frames = prior_frames as i32;

    for hit in &atk_data.hit_frames {
        let frame_num = hit.frame_num as i32 - prior_frames;
        if frame_num < 0 {
            match attack_for_walking_opponent(
                atk_chara, frame_num, atk_pos,
                hit, def_chara, m, def_history, &def_pos_history, first_distance) {
                AtkWalkingOppo::Hit(damage) => return Some(damage),
                AtkWalkingOppo::NoHit =>{},
                AtkWalkingOppo::BeforeHistory => unreachable!(),
                AtkWalkingOppo::AfterHistory => unreachable!(),
            }
        } else {
            let def_pos = get_last_pos(&def_pos_history);
            let distance = (first_distance as i32 - atk_pos - def_pos).max(0) as u32;
            if let Some(damage) = attack_for_stopping_opponent(hit.right, distance, atk_chara, m, def_chara) {
                return Some(damage);
            }
        }
    }
    if let Some(whiff_frame) = m.whiff {
        let notice =  - prior_frames + atk_data.hurt_frames.len() as i32 - whiff_frame as i32;
        let def_pos =
            if notice < 0{ 0 }
            else if notice < def_pos_history.len() as i32{ def_pos_history[notice as usize] }
            else{ get_last_pos(&def_pos_history) };


        let distance = (first_distance as i32 - atk_pos - def_pos).max(0) as u32;
        return punish(def_chara, m, whiff_frame, distance, atk_chara).map(|d| -(d as i32));
    }
    else{ return None; }
}

fn hurt(chara : &CharaRef, m : &Move) -> u32{
    if m.crouching{ chara.crouch } else { chara.stand }
}

///distanceはお互いが攻撃中の場合の、攻撃をはじめた瞬間の位置同士の距離、あるいは相手が歩くかしゃがんでいる場合は、自分の攻撃を始めた位置と、攻撃があたった時の相手の位置の差。
///ddはダメージ距離で、distanceから相手が攻撃を食らった瞬間の、相手の攻撃による移動分の距離を引いたもので、ダメージの算出に使える。相手が攻撃中じゃない場合はdistanceと同じ
///first_distanceはwalk_historyの始まりの位置同士の距離。
fn punish(punisher : &CharaRef, m : &Move, minus_frame : u32, distance : u32, def_chara : &CharaRef) -> Option<u32> {
    let mut damage : i32 = -1;
    let mut distance = distance;
    if let Some(diff) = m.special_punish_pos_diff{
        distance = (distance as i32 - diff).max(0) as u32;
    }
    for &p in &punisher.punish_moves {
        if p.punish.len() == 0{ continue; }
        if p.whiff_on_crouch && m.crouching{ continue; }

        let move_len = m.attack_data.hurt_frames.len();

        //常識的に考えると殆どの場合最初の1個で済むな・・・必要ならハートボックスが減少し続けるポイントを記録することで最適化出来ると思う。
        for hit in &p.attack_data.hit_frames{
            //frame_numは0スタート
            if minus_frame <= hit.frame_num{ break; }
            let frame =  move_len - (minus_frame - hit.frame_num) as usize;
            if let Some(dd) = test_hitbox(hit, &m.attack_data.hurt_frames[frame], distance){
                for item in &p.punish {
                    if item.upper_distance == 0 || dd <= item.upper_distance + hurt(def_chara, m){
                        if damage < item.damage as i32{
                            damage = item.damage as i32;
                        }
                        break;
                    }
                }
            }
        }
    }

    return if 0 < damage{ Some(damage as u32) } else { None }
}