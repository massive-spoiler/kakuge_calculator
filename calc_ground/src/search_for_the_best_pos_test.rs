use crate::construct_characters::construct_characters;
use crate::{default_scale, disabled_moves};
use crate::search_for_the_best_pos::search_for_the_best_pos;
use crate::chara_gen::{ForwardCharaGenerable, WalkAndPos, WalkType, LaggedWalkingCharaGenerable};
use ordered_float::OrderedFloat;
use crate::vs_best_test::best_disabled_moves;

pub struct VF{
    pub vec : Vec<WalkAndPos>,
    pub first_distance : f64,
    pub duration : usize,
}

pub fn vs_forward_walk_and_poses() -> VF{
    let forward = WalkAndPos::new(WalkType::Forward, 0.0);
    let crouching = WalkAndPos::new(WalkType::Crouching, -0.9);
    let forward2 = WalkAndPos::new(WalkType::Forward, -0.9);
    let forward3 = WalkAndPos::new(WalkType::Forward, -0.8);
    let backward = WalkAndPos::new(WalkType::Backward, -0.2);
    let forward4 = WalkAndPos::new(WalkType::Forward, -0.5);
    let forward5 = WalkAndPos::new(WalkType::Forward, -1.4);
    return VF{ vec : vec![forward, crouching, forward2, forward3, backward, forward4, forward5], first_distance : 2.6, duration : 20 }
}

pub fn search_for_the_best_pos_test_vs_forward(){
    let scale = default_scale();
    let cs = construct_characters(Some(scale));
    let c = &cs[0];
    let c = c.to_chara_ref(disabled_moves());
    let mut enemy = ForwardCharaGenerable::new(&c, 20, 0.0);
    let forward = WalkAndPos::new(WalkType::Forward, 0.0);
    let crouching = WalkAndPos::new(WalkType::Crouching, -0.9);
    let forward2 = WalkAndPos::new(WalkType::Forward, -0.9);
    let forward3 = WalkAndPos::new(WalkType::Forward, -0.8);
    let backward = WalkAndPos::new(WalkType::Backward, -0.2);
    let forward4 = WalkAndPos::new(WalkType::Forward, -0.5);
    let forward5 = WalkAndPos::new(WalkType::Forward, -1.4);
    let mut vec =search_for_the_best_pos(&c, &[forward, crouching, forward2, forward3, backward, forward4, forward5],2.6, -1.4, 0.0, 0.1, 20, scale,
                            &mut enemy, &[WalkType::Forward, WalkType::Crouching, WalkType::Backward]);

    vec.sort_by_key(|item| OrderedFloat(-item.kitaichi));
    dbg!(vec);
}



pub fn best_dual_walk_and_poses() -> VF{
    return VF{ vec : vec![
        WalkAndPos::new(WalkType::Forward, 0.0),
        //WalkAndPos::new(WalkType::Forward, -1.4),
        WalkAndPos::new(WalkType::Forward, -1.0),
        WalkAndPos::new(WalkType::Forward, -0.2),
        WalkAndPos::new(WalkType::Forward, -0.4),
        WalkAndPos::new(WalkType::Forward, -0.6),
        WalkAndPos::new(WalkType::Forward, -0.8),
        //WalkAndPos::new(WalkType::Forward, -1.2),
    ], first_distance : 2.0, duration : 30 }
}

pub fn search_for_the_best_pos_dual_test(){
    let scale = default_scale();
    let cs = construct_characters(Some(scale));
    let c = &cs[0];
    let c = c.to_chara_ref(best_disabled_moves());

    //let c = c.to_chara_ref(&[]);
    let h = best_dual_walk_and_poses();

    let mut enemy = LaggedWalkingCharaGenerable::new(&c, h.duration, &h.vec);
    let mut vec =search_for_the_best_pos(&c, &h.vec, h.first_distance,
                                         -1.3, -0.01, 0.1, h.duration, scale,
                                         &mut enemy,
//                                         &[WalkType::Forward, ]
                                             &[WalkType::Forward, WalkType::Crouching]
    );

    vec.sort_by_key(|item| OrderedFloat(-item.kitaichi));
    dbg!(vec);
}