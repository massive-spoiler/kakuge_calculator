use crate::test_hitbox::{test_two_attacks, attack_vs_whiff_punish, WalkHistoryState};
use crate::chara_data_structs::{Move,  CharaRef};


pub struct MoveDef<'a>{
    pub name : String,
    pub rel_start_position : f64,
    pub walk_history : &'a Vec<WalkHistoryState>,
    pub chara : &'a CharaRef<'a>,
    ///Noneの場合waitする
    pub move_item : Option<&'a Move>,
}

use crate::chara_gen::MoveDefGenerator;

pub fn create_table(title : &str, offence : &mut impl MoveDefGenerator, defence : &mut impl MoveDefGenerator,
                    first_distance : f64, scale : u32) -> Vec<Vec<String>>{
    let scaled = |f : f64| -> u32{ (f.max(0.0) * scale as f64) as u32 };

    let prior_frames = |atk : &MoveDef, def : &MoveDef| -> i32{
        def.walk_history.len() as i32 - atk.walk_history.len() as i32
    };

    let atk_vs_whiff_pusnish = |atk : &MoveDef, wp : &MoveDef| -> i32{
        let prior_frames = prior_frames(atk, wp).max(0) as u32;
        let distance = scaled(first_distance - atk.rel_start_position - wp.rel_start_position);

        attack_vs_whiff_punish(atk.chara, atk.move_item.unwrap(), &atk.walk_history, prior_frames,
                               wp.chara, &wp.walk_history, distance).unwrap_or(0)
    };

    let mut r : Vec<Vec<String>> = vec![];

    let mut first : Vec<String> = vec![title.to_string()];
    defence.generate(&mut |def|{
        first.push(def.name.to_string());
    });

    r.push(first);

    offence.generate(&mut |off|{
        let mut line : Vec<String> = vec![off.name.to_string()];
        defence.generate(&mut |def|{
            let r : i32 = if off.move_item.is_none() && def.move_item.is_none(){
                0
            }
            else if off.move_item.is_none() && def.move_item.is_some() {
                atk_vs_whiff_pusnish(def, off) * -1
            }
            else if off.move_item.is_some() && def.move_item.is_none(){
                atk_vs_whiff_pusnish(off, def)
            }
            else{
                let prior_frames = prior_frames(off, def);
                let distance = scaled(first_distance - off.rel_start_position - def.rel_start_position);
                //println!("dis {} {} {}", first_distance, off.rel_start_position, def.rel_start_position);
                test_two_attacks(off.chara, off.move_item.unwrap(), &off.walk_history, prior_frames,
                                 def.chara, def.move_item.unwrap(), &def.walk_history, distance).unwrap_or(0)
            };
            line.push(r.to_string());
        });
        r.push(line);
    });

    return r;
}


pub fn create_table_test(atk_name : &str, def_name : &str, offence : &mut impl MoveDefGenerator, defence : &mut impl MoveDefGenerator,
                    first_distance : f64, scale : u32) -> Option<i32>{
    let scaled = |f : f64| -> u32{ (f.max(0.0) * scale as f64) as u32 };

    let prior_frames = |atk : &MoveDef, def : &MoveDef| -> i32{
        def.walk_history.len() as i32 - atk.walk_history.len() as i32
    };

    let atk_vs_whiff_pusnish = |atk : &MoveDef, wp : &MoveDef| -> i32{
        let prior_frames = prior_frames(atk, wp).max(0) as u32;
        let distance = scaled(first_distance - atk.rel_start_position - wp.rel_start_position);

        attack_vs_whiff_punish(atk.chara, atk.move_item.unwrap(), &atk.walk_history, prior_frames,
                               wp.chara, &wp.walk_history, distance).unwrap_or(0)
    };

    let mut result : Option<i32> = None;

    offence.generate(&mut |off|{
        if off.name == atk_name {
            defence.generate(&mut |def| {
                if def.name == def_name {
                    let r: i32 = if off.move_item.is_none() && def.move_item.is_none() {
                        0
                    } else if off.move_item.is_none() && def.move_item.is_some() {
                        atk_vs_whiff_pusnish(def, off) * -1
                    } else if off.move_item.is_some() && def.move_item.is_none() {
                        atk_vs_whiff_pusnish(off, def)
                    } else {
                        let prior_frames = prior_frames(off, def);
                        let distance = scaled(first_distance - off.rel_start_position - def.rel_start_position);
                        //println!("dis {} {} {}", first_distance, off.rel_start_position, def.rel_start_position);
                        test_two_attacks(off.chara, off.move_item.unwrap(), &off.walk_history, prior_frames,
                                         def.chara, def.move_item.unwrap(), &def.walk_history, distance).unwrap_or(0)
                    };
                    result = Some(r)
                };
            });
        }
    });

    return result;
}
