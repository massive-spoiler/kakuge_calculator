//use crate::analyze_calc_result::analyze_calc_result;

use crate::construct_characters::construct_characters;
use crate::default_scale;
//use crate::analyzed_acts_to_json::AnalyzedActs;

pub fn analyze_calc_test(text : &str, description : &[String]){
    let scale = default_scale();
    let charas = construct_characters(Some(scale));
    let f = charas[0].forward as f64 / scale as f64;
    let b = charas[0].backward as f64 / scale as f64;
    let acts = crate::analyze_calc_result::parse_text(text, f, b);
    let acts = crate::analyze_calc_result::analyze_acts(&acts);


    println!("{}", acts.to_html(description));

    //let s = "f-0.4_16_4HP<1.8%>";
    //analyze_calc_result(s, 0.5, -0.3);
}