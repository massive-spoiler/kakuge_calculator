use crate::chara_gen::{CrouchingCharaGenerable};
use crate::move_def::create_table;
use crate::chara_data_structs::Chara;
use crate::disabled_moves;

pub fn crouching_battle(c1 : &Chara, c2 : &Chara, duration : usize, distance : f64, scale : u32) -> Vec<Vec<String>>{
    let c1 = c1.to_chara_ref(disabled_moves());
    let c2 = c2.to_chara_ref(disabled_moves());
    //let c2_moves = disable_moves(c2, &[]);
    let mut ops1 = CrouchingCharaGenerable::new(&c1, duration, 0.0);
    let mut ops2 = CrouchingCharaGenerable::new(&c2, duration, 0.0);

    let table = create_table("ct", &mut ops1, &mut ops2, distance, scale);
    table
}