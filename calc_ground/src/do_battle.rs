use crate::chara_gen::{WalkAndPos, LaggedWalkingCharaGenerable};
use crate::move_def::create_table;
use kakuge::KakugeCalcResult;
use crate::chara_data_structs::CharaRef;
use kakuge::kakuge_calc_result::CalcResult;

pub fn do_battle(offence: &[WalkAndPos], defence: &[WalkAndPos], off_chara : &CharaRef, def_chara : &CharaRef,
                 duration : usize, first_distance : f64, scale : u32) -> KakugeCalcResult{
    let mut off = LaggedWalkingCharaGenerable::new(off_chara, duration, offence);
    let mut def = LaggedWalkingCharaGenerable::new(def_chara, duration, defence);
    let table = create_table("hoge", &mut off, &mut def, first_distance, scale);
    kakuge::calc::calc(&table).unwrap()
}

pub fn do_battle_off(offence: &[WalkAndPos], defence: &[WalkAndPos], off_chara : &CharaRef, def_chara : &CharaRef,
                 duration : usize, first_distance : f64, scale : u32) -> CalcResult{
    let mut off = LaggedWalkingCharaGenerable::new(off_chara, duration, offence);
    let mut def = LaggedWalkingCharaGenerable::new(def_chara, duration, defence);
    let table = create_table("hoge", &mut off, &mut def, first_distance, scale);
    //println!("{}", table[0][1000]);
    let len = table.len();
    for i in 0..len{
        let title = &table[i][0];
        if title == "f0_5LK"{
            for k in 1..table[i].len(){
                if table[i][k] != "0"{
                    println!("{}, {}", &table[0][k], &table[i][k]);
                }

            }
        }
    }

    kakuge::calc::calc_offence(&table).unwrap()
}