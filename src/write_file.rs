use std::path::Path;
use std::fs::File;
use std::io::prelude::*;

pub fn _write_file(path : &str, text : &str){
    let path = Path::new(path);


    // Open a file in write-only mode, returns `io::Result<File>`
    let mut file = File::create(&path).unwrap();

    // Write the `LOREM_IPSUM` string to `file`, returns `io::Result<()>`
    file.write_all(text.as_bytes()).unwrap();
}

pub fn _read_file(path : &str) -> String{
    let path = Path::new(path);
    let mut s = String::new();
    File::open(path).unwrap().read_to_string(&mut s).unwrap();
    s
}

pub fn _read_lines(path : &str) -> Vec<String>{
    let s = _read_file(path);

    s.lines().map(|h| h.to_string()).collect()
}