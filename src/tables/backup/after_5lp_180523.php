<?php
error_reporting(E_ALL ^ E_WARNING);

//ミカで試した所、コパン中Kの後EXジャイスイで吸える。後ろ下がりすればスカるが・・・小K中Kでも（ほんの少し多く離れるものの）吸える
//4Fキャラはコパンから2MPを割れない
$lpg =[
"lpg              G      G>L3     G>Dp      Vj        Bs         M        L3        L4       Dp       Cmd",
"sMK        mkpg+20   mkpg+20  gdp-140 mkh+178    mks+72   mkc+190   lpc-154   mkc+190   dp-150   mkc+190",//G>Lはコパンの場合暴れるが中Kガードを見てから暴れをやめる、中Kの後はコパンで暴れても仕方ない。
"sMK>G       mkg+10    mkg+10 hkc2+300   mh+60    mks+72     mc+70   lpc-154     mc+72   dp-150     mc+72",
"P_Cmd       cg+168    cg+168   cg+168  jh-300    hh-238   mkc-190   lpc-154   lpc-154   dp-150    cg-168",//ネカリのコマ投げは弱いので、ここではコマ投げの打ち合いに負けることにする
"sHP         hpg+13    hpg+13   hpg+13  hpa+80   hps+192   mkc-190   lpc-154   lpc-154   dp-150   hmh+170",//単発確認突進狙い
"sLP>EX/2MP  lmg+10   lpc-154   dp-150  lph+30    lph+30   lpc+154   lpc+154   lpc+154   dp-150   lpc+154", //5LKが当たっていない場合2MPにする。5LPがカウンターしていれば確認してコンボに
"sLP>EX/Lre  lrg+13   lrd+202   dp-150  lph+30    lph+30    lrc+81    lrc+81    lrc+81   dp-150    lrc+81", //5LKが当たっていない場合小決起にする
"sLP>EX      exg+18    exg+18   exg+18 exh+138   exh+138   exc+144   exc+144   exc+144   dp-150   exc+144",//5LKがあたってなくても入れ込む。連ガである。
"sLP>G          g+0       g+0  hkc+290  p_u+30    p_u+30   pc_u+36   pc_u+36   pc_u+36   dp-150   pc_u+36",
"P_G            g+0       g+0      g+0  aa+120       g+0    mkg-17       g+0       g+0  hkc+290    cg-168"];


$tables = [
  main_table => [
    table => $lpg,
    options => [
      no_dp => [deltags => [Dp,"G>Dp"]],
      no_ex => [deltags => "sLP>EX"],
      no_cmd => [deltags => Cmd],
      no_l3 => [deltags => L3, "G>L3"]
    ],
  ]
];

$execs = [
//  main_table => [main_table],
//  no_dp => [main_table => no_dp],
  no_ex => [main_table => no_ex],
  no_ex_l3 => [main_table => [no_ex, no_l3]],
  no_dp_ex => [main_table => [no_dp, no_ex]],
  no_dp_ex_l3 => [main_table => [no_dp, no_ex,no_l3]],
//  no_cmd => [main_table => no_cmd],
  no_cmd_ex => [main_table => [no_cmd, no_ex]],
  no_cmd_ex_l3 => [main_table => [no_cmd, no_ex,no_l3]],
  no_dp_cmd_ex => [main_table => [no_dp, no_cmd, no_ex]],
  no_dp_cmd_ex_l3 => [main_table => [no_dp, no_cmd, no_ex, no_l3]]
];
$master = [tables => $tables, execs => $execs];

echo json_encode($master);

/*
3Fコパンの有無で期待値がとんでもなく変わる・・・

no_dp_ex 期待値　27.19
攻撃側: sLP>EX/2MP<35.4%> | sLP>EX/Lre<30.7%> | sMK<15.3%> | sHP<9.4%> | P_Cmd<9.1%> |
防御側: G<63.4%> | Bs<13.2%> | Vj<11.5%> | L3<10.3%> | G>L<1.6%> |

no_dp_ex_l3 期待値　44.02
攻撃側: sMK<61.6%> | sHP<21.2%> | P_Cmd<17.2%> |
防御側: G<68.7%> | Bs<24.8%> | M<6.6%> |

相手に昇竜がない場合、3Fがない相手には中Kでいいが、3Fがあるとコパン->中Kはカウンターでコンボを食らうリスク行動になる。
なのでコパンをつなげてそこから小決起と中Pで択にいくことになる。ゲージが余ってるならEX決起入れ込んでも別にいい。

no_cmd_ex 期待値　15.32
攻撃側: P_G<37.6%> | sLP>EX/2MP<17.0%> | sLP>EX/Lre<14.8%> | sLP>G<12.8%> | sHP<6.8%> | P_Cmd<6.1%> | sMK>G<4.9%> |
防御側: G<51.9%> | Bs<21.2%> | L3<8.8%> | M<5.8%> | Dp<5.6%> | G>Dp<4.2%> | G>L<2.5%> |

no_cmd_ex_l3 期待値　23.92
攻撃側: P_G<39.5%> | sMK<27.8%> | sMK>G<15.3%> | P_Cmd<9.4%> | sHP<7.9%> |
防御側: G<58.5%> | Bs<22.3%> | Dp<8.7%> | M<7.3%> | G>Dp<3.3%> |

相手に昇竜がある場合、やはり1/3はガードになる。しかしコパンガード後に最速で昇竜というのは、
見てからではまず無理なはずなので、「なにかガードしてから昇竜」という入れこみ行動になる。
コパンガード後は7Fのヒットストップの後（1Fの持続)?と9Fの戻りモーションがある。相手はマイナス1なのでそこから1F動けない。
昇竜を6Fで入力したとしてもガード後11-18Fぐらいに隙が出来るはずである。
なのでコパンガードさせず15F攻撃すれば当たるはずだ。その選択肢はこの表では表現できないので、上の表を直すべきだろう。
コパンガード後は昇竜、中Kではガード、という入力も出来るが、同じように隙ができるはず。

3Fがない場合は中Kだが、中Kの後は1/3ガード、2/3中パン

no_dp_cmd_ex 期待値　27.19
攻撃側: sLP>EX/2MP<35.4%> | sLP>EX/Lre<30.7%> | sMK<15.3%> | sHP<9.4%> | P_Cmd<9.1%> |
防御側: G<63.4%> | Bs<13.2%> | Vj<11.5%> | L3<10.3%> | G>L<1.6%> |

no_dp_cmd_ex_l3 期待値　44.02
攻撃側: sMK<61.6%> | sHP<21.2%> | P_Cmd<17.2%> |
防御側: G<68.7%> | Bs<24.8%> | M<6.6%> |



no_ex 期待値　8.11 //昇竜もコマ投げも3fもあるネカリは起き攻めの期待値が最も低い　逆択のコマ投げ、昇竜を撃ちまくれ
攻撃側: P_G<35.9%> | sLP>G<17.0%> | sHP<15.0%> | sLP>EX/2MP<14.5%> | sLP>EX/Lre<12.6%> | sMK<3.5%> | P_Cmd<1.4%> |
防御側: G<37.7%> | Cmd<23.5%> | Dp<16.5%> | L3<7.9%> | G>Dp<7.3%> | G>L<6.3%> | M<0.9%> |

no_ex_l3 期待値　16.80
攻撃側: P_G<37.9%> | sMK<27.0%> | sHP<15.5%> | sMK>G<14.9%> | P_Cmd<4.7%> |
防御側: G<46.0%> | Cmd<24.8%> | Dp<20.2%> | G>Dp<8.0%> | M<1.1%> |

*no_cmd_ex

        lpg       G     G>L     G>Dp      Vj      Bs       M      L3      L4      Dp
        sMK mkpg+20 mkpg+20  gdp-140 mkh+178  mks+72 mkc+190 lpc-154 mkc+190  dp-150
      sMK>G  mkg+10  mkg+10 hkc2+300   mh+60  mks+72   mc+70 lpc-154   mc+72  dp-150
      P_Cmd  cg+168  cg+168   cg+168  jh-300  hh-238 mkc-190 lpc-154 lpc-154  dp-150
        sHP  hpg+13  hpg+13   hpg+13  hpa+80 hps+192 mkc-190 lpc-154 lpc-154  dp-150
 sLP>EX/2MP  lmg+10 lpc-154   dp-150  lph+30  lph+30 lpc+154 lpc+154 lpc+154  dp-150
 sLP>EX/Lre  lrg+13 lrd+202   dp-150  lph+30  lph+30  lrc+81  lrc+81  lrc+81  dp-150
      sLP>G     g+0     g+0  hkc+290  p_u+30  p_u+30 pc_u+36 pc_u+36 pc_u+36  dp-150
        P_G     g+0     g+0      g+0  aa+120     g+0  mkg-17     g+0     g+0 hkc+290

*no_cmd_ex_l3

        lpg       G     G>L     G>Dp      Vj      Bs       M      L4      Dp
        sMK mkpg+20 mkpg+20  gdp-140 mkh+178  mks+72 mkc+190 mkc+190  dp-150
      sMK>G  mkg+10  mkg+10 hkc2+300   mh+60  mks+72   mc+70   mc+72  dp-150
      P_Cmd  cg+168  cg+168   cg+168  jh-300  hh-238 mkc-190 lpc-154  dp-150
        sHP  hpg+13  hpg+13   hpg+13  hpa+80 hps+192 mkc-190 lpc-154  dp-150
 sLP>EX/2MP  lmg+10 lpc-154   dp-150  lph+30  lph+30 lpc+154 lpc+154  dp-150
 sLP>EX/Lre  lrg+13 lrd+202   dp-150  lph+30  lph+30  lrc+81  lrc+81  dp-150
      sLP>G     g+0     g+0  hkc+290  p_u+30  p_u+30 pc_u+36 pc_u+36  dp-150
        P_G     g+0     g+0      g+0  aa+120     g+0  mkg-17     g+0 hkc+290

*no_dp_cmd_ex

        lpg       G     G>L      Vj      Bs       M      L3      L4
        sMK mkpg+20 mkpg+20 mkh+178  mks+72 mkc+190 lpc-154 mkc+190
      sMK>G  mkg+10  mkg+10   mh+60  mks+72   mc+70 lpc-154   mc+72
      P_Cmd  cg+168  cg+168  jh-300  hh-238 mkc-190 lpc-154 lpc-154
        sHP  hpg+13  hpg+13  hpa+80 hps+192 mkc-190 lpc-154 lpc-154
 sLP>EX/2MP  lmg+10 lpc-154  lph+30  lph+30 lpc+154 lpc+154 lpc+154
 sLP>EX/Lre  lrg+13 lrd+202  lph+30  lph+30  lrc+81  lrc+81  lrc+81
      sLP>G     g+0     g+0  p_u+30  p_u+30 pc_u+36 pc_u+36 pc_u+36
        P_G     g+0     g+0  aa+120     g+0  mkg-17     g+0     g+0

*no_dp_cmd_ex_l3

        lpg       G     G>L      Vj      Bs       M      L4
        sMK mkpg+20 mkpg+20 mkh+178  mks+72 mkc+190 mkc+190
      sMK>G  mkg+10  mkg+10   mh+60  mks+72   mc+70   mc+72
      P_Cmd  cg+168  cg+168  jh-300  hh-238 mkc-190 lpc-154
        sHP  hpg+13  hpg+13  hpa+80 hps+192 mkc-190 lpc-154
 sLP>EX/2MP  lmg+10 lpc-154  lph+30  lph+30 lpc+154 lpc+154
 sLP>EX/Lre  lrg+13 lrd+202  lph+30  lph+30  lrc+81  lrc+81
      sLP>G     g+0     g+0  p_u+30  p_u+30 pc_u+36 pc_u+36
        P_G     g+0     g+0  aa+120     g+0  mkg-17     g+0

*no_dp_ex

        lpg       G     G>L      Vj      Bs       M      L3      L4     Cmd
        sMK mkpg+20 mkpg+20 mkh+178  mks+72 mkc+190 lpc-154 mkc+190 mkc+190
      sMK>G  mkg+10  mkg+10   mh+60  mks+72   mc+70 lpc-154   mc+72   mc+72
      P_Cmd  cg+168  cg+168  jh-300  hh-238 mkc-190 lpc-154 lpc-154  cg-168
        sHP  hpg+13  hpg+13  hpa+80 hps+192 mkc-190 lpc-154 lpc-154 hmh+170
 sLP>EX/2MP  lmg+10 lpc-154  lph+30  lph+30 lpc+154 lpc+154 lpc+154 lpc+154
 sLP>EX/Lre  lrg+13 lrd+202  lph+30  lph+30  lrc+81  lrc+81  lrc+81  lrc+81
      sLP>G     g+0     g+0  p_u+30  p_u+30 pc_u+36 pc_u+36 pc_u+36 pc_u+36
        P_G     g+0     g+0  aa+120     g+0  mkg-17     g+0     g+0  cg-168

*no_dp_ex_l3

        lpg       G     G>L      Vj      Bs       M      L4     Cmd
        sMK mkpg+20 mkpg+20 mkh+178  mks+72 mkc+190 mkc+190 mkc+190
      sMK>G  mkg+10  mkg+10   mh+60  mks+72   mc+70   mc+72   mc+72
      P_Cmd  cg+168  cg+168  jh-300  hh-238 mkc-190 lpc-154  cg-168
        sHP  hpg+13  hpg+13  hpa+80 hps+192 mkc-190 lpc-154 hmh+170
 sLP>EX/2MP  lmg+10 lpc-154  lph+30  lph+30 lpc+154 lpc+154 lpc+154
 sLP>EX/Lre  lrg+13 lrd+202  lph+30  lph+30  lrc+81  lrc+81  lrc+81
      sLP>G     g+0     g+0  p_u+30  p_u+30 pc_u+36 pc_u+36 pc_u+36
        P_G     g+0     g+0  aa+120     g+0  mkg-17     g+0  cg-168

*no_ex

        lpg       G     G>L     G>Dp      Vj      Bs       M      L3      L4      Dp     Cmd
        sMK mkpg+20 mkpg+20  gdp-140 mkh+178  mks+72 mkc+190 lpc-154 mkc+190  dp-150 mkc+190
      sMK>G  mkg+10  mkg+10 hkc2+300   mh+60  mks+72   mc+70 lpc-154   mc+72  dp-150   mc+72
      P_Cmd  cg+168  cg+168   cg+168  jh-300  hh-238 mkc-190 lpc-154 lpc-154  dp-150  cg-168
        sHP  hpg+13  hpg+13   hpg+13  hpa+80 hps+192 mkc-190 lpc-154 lpc-154  dp-150 hmh+170
 sLP>EX/2MP  lmg+10 lpc-154   dp-150  lph+30  lph+30 lpc+154 lpc+154 lpc+154  dp-150 lpc+154
 sLP>EX/Lre  lrg+13 lrd+202   dp-150  lph+30  lph+30  lrc+81  lrc+81  lrc+81  dp-150  lrc+81
      sLP>G     g+0     g+0  hkc+290  p_u+30  p_u+30 pc_u+36 pc_u+36 pc_u+36  dp-150 pc_u+36
        P_G     g+0     g+0      g+0  aa+120     g+0  mkg-17     g+0     g+0 hkc+290  cg-168

*no_ex_l3

        lpg       G     G>L     G>Dp      Vj      Bs       M      L4      Dp     Cmd
        sMK mkpg+20 mkpg+20  gdp-140 mkh+178  mks+72 mkc+190 mkc+190  dp-150 mkc+190
      sMK>G  mkg+10  mkg+10 hkc2+300   mh+60  mks+72   mc+70   mc+72  dp-150   mc+72
      P_Cmd  cg+168  cg+168   cg+168  jh-300  hh-238 mkc-190 lpc-154  dp-150  cg-168
        sHP  hpg+13  hpg+13   hpg+13  hpa+80 hps+192 mkc-190 lpc-154  dp-150 hmh+170
 sLP>EX/2MP  lmg+10 lpc-154   dp-150  lph+30  lph+30 lpc+154 lpc+154  dp-150 lpc+154
 sLP>EX/Lre  lrg+13 lrd+202   dp-150  lph+30  lph+30  lrc+81  lrc+81  dp-150  lrc+81
      sLP>G     g+0     g+0  hkc+290  p_u+30  p_u+30 pc_u+36 pc_u+36  dp-150 pc_u+36
        P_G     g+0     g+0      g+0  aa+120     g+0  mkg-17     g+0 hkc+290  cg-168
*/

?>