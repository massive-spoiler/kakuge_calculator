<?php
error_reporting(E_ALL ^ E_WARNING);


//MKガードの後に昇竜もち相手には2MPで白ダメージを取りにいくかという読みあいがあり、3割ぐらいガードが正解。相手は5%ぐらいで昇竜するべき
//ネカリの足だと2フレーム後ろ歩きをしても5MK2MPは両方ガードさせられる。+2なので後ろ歩きでは外せない。
//小Kから一瞬の遅れもなくEXコマ投げすれば後ろ歩きされても当たる
$lkg =[
"lkg              G       G>L     G>Dp      Vj        Bs         M         L      Dp        Cmd",
"sMK        mkpg+20   mkpg+20  gdp-140 mkh+178    mks+72   mkc+190   mkc+190  dp-150   mkc+190",//G>Lはコパンの場合暴れるが中Kガードを見てから暴れをやめる、中Kの後はコパンで暴れても仕方ない。
"sMK>G       mkg+10    mkg+10 hkc2+300   mh+60    mks+72     mc+70     mc+72  dp-150     mc+72",
"P_Cmd       cg+168    cg+168   cg+168  jh-300    hh-238   mkc-190   lpc-154  dp-150    cg-168",//ネカリのコマ投げは弱いので、ここではコマ投げの打ち合いに負けることにする
"sHP         hpg+13    hpg+13   hpg+13  hpa+80   hps+192   mkc-190   lpc-154  dp-150   hmh+170",//単発確認突進狙い
"sLP>EX/2MP  lmg+10   lpc-154   dp-150  lph+30    lph+30   lpc+154   lpc+154  dp-150   lpc+154", //5LKが当たっていない場合2MPにする。5LPがカウンターしていれば確認してコンボに
"sLP>EX/Lre  lrg+13   lrd+202   dp-150  lph+30    lph+30    lrc+81    lrc+81  dp-150    lrc+81", //5LKが当たっていない場合小決起にする
"sLP>EX      exg+18    exg+18   exg+18 exh+138   exh+138   exc+144   exc+144  dp-150   exc+144",//5LKがあたってなくても入れ込む。連ガである。
"sLP>G          g+0       g+0  hkc+290  p_u+30    p_u+30   pc_u+36   pc_u+36  dp-150   pc_u+36",
"P_G            g+0       g+0      g+0  aa+120       g+0    mkg-17       g+0 hkc+290    cg-168"];//本当はP側は後ろ下がりでコマ投げは避けられる。相手がネカリの場合だけだが・・・



$tables = [
  main_table => [
    table => $lkg,
    options => [
      no_dp => [deltags => [Dp,"G>Dp"]],
      no_ex => [deltags => "sLP>EX"],
      no_cmd => [deltags => "Cmd"]
    ],
  ]
];

$execs = [
  main_table => [main_table],
  no_dp => [main_table => no_dp],
  no_ex => [main_table => no_ex],
  no_dp_ex => [main_table => [no_dp, no_ex]],
  no_cmd => [main_table => no_cmd],
  no_cmd_ex => [main_table => [no_cmd, no_ex]]
];
$master = [tables => $tables, execs => $execs];

echo json_encode($master);
/*
no_ex 期待値　23.92
攻撃側: P_G<39.5%> | sMK<27.8%> | sMK>G<15.3%> | P_Cmd<9.4%> | sHP<7.9%> | 0%:
防御側: G<58.5%> | Bs<22.3%> | Dp<8.7%> | M<7.3%> | G>Dp<3.3%> | 0%:Cmd,Vj,
["sLP>G", "sLP>EX/2MP", "L", "G>L", "sLP>EX/Lre"] removed

4割ガード、4割ヒザ、そのうち1/3はヒザ後ガードする。1割ずつコマ投げ、大パン。

no_dp_ex 期待値　44.02
攻撃側: sMK<61.6%> | sHP<21.2%> | P_Cmd<17.2%> | 0%:
防御側: G<68.7%> | Bs<24.8%> | M<6.6%> | 0%:Vj,
["P_G", "sLP>G", "sLP>EX/2MP", "sMK>G", "Cmd", "L", "G>L", "sLP>EX/Lre"] removed

6割ヒザ、2割ずつ大パン、コマ投げ


lkg 期待値　25.63
攻撃側: P_G<39.9%> | sLP>EX<38.7%> | P_Cmd<14.7%> | sMK<4.5%> | sHP<2.1%> | 0%:sMK>G,
防御側: Cmd<44.6%> | Dp<34.9%> | G>Dp<12.4%> | Bs<4.2%> | M<4.0%> | 0%:Vj,G,
["sLP>G", "sLP>EX/2MP", "L", "G>L", "sLP>EX/Lre"] removed

no_cmd 期待値　30.83
攻撃側: sLP>EX<44.8%> | P_G<41.1%> | P_Cmd<13.5%> | sHP<0.6%> | 0%:sMK>G,sMK,
防御側: G<63.5%> | Bs<22.9%> | Dp<10.8%> | M<2.8%> | 0%:Vj,G>Dp,
["sLP>G", "sLP>EX/2MP", "L", "G>L", "sLP>EX/Lre"] removed

no_dp 期待値　50.06
攻撃側: sLP>EX<66.4%> | P_Cmd<21.2%> | sMK<12.4%> | 0%:sHP,
防御側: G<73.3%> | Vj<15.3%> | Bs<11.5%> | 0%:M,
["P_G", "sLP>G", "sLP>EX/2MP", "sMK>G", "Cmd", "L", "G>L", "sLP>EX/Lre"] removed


*lkg

        lkg       G     G>L     G>Dp      Vj      Bs       M       L      Dp     Cmd
        sMK mkpg+20 mkpg+20  gdp-140 mkh+178  mks+72 mkc+190 mkc+190  dp-150 mkc+190
      sMK>G  mkg+10  mkg+10 hkc2+300   mh+60  mks+72   mc+70   mc+72  dp-150   mc+72
      P_Cmd  cg+168  cg+168   cg+168  jh-300  hh-238 mkc-190 lpc-154  dp-150  cg+168
        sHP  hpg+13  hpg+13   hpg+13  hpa+80 hps+192 mkc-190 lpc-154  dp-150 hmh+170
 sLP>EX/2MP  lmg+10 lpc-154   dp-150  lph+30  lph+30 lpc+154 lpc+154  dp-150 lpc+154
 sLP>EX/Lre  lrg+13 lrd+202   dp-150  lph+30  lph+30  lrc+81  lrc+81  dp-150  lrc+81
     sLP>EX  exg+18  exg+18   exg+18 exh+138 exh+138 exc+144 exc+144  dp-150 exc+144
      sLP>G     g+0     g+0  hkc+290  p_u+30  p_u+30 pc_u+36 pc_u+36  dp-150 pc_u+36
        P_G     g+0     g+0      g+0  aa+120     g+0  mkg-17     g+0 hkc+290  cg-168

*/
?>