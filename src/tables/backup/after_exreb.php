<?php
error_reporting(E_ALL ^ E_WARNING);
//ふみふみヒットの後。距離は固定である。1F後ろに歩けばEXコマ投げは当たらない。2F歩けばコパンも当たらない。
//後ろ歩きにリスクを与える中足は7Fなので、-2からでも4F歩く余裕がある。歩きコマ投げで対抗したいが、ファジーガードコパンに負ける。
//後ろ歩きの早いclaw,sakuraには中足が当たらない。
//6Fに出したファジーコパンは7F目にヒットする大パンでカウンターヒットさせることが出来る。CC確認キャンセルは簡単にできる。単発確認も出来る（俺にはまだ難しいが)。キャンセル円盤をくらわせろ。
//P側が防御する時は後ろに歩いてその場からのコマ投げをかわす事ができる。E側が防御にリスクを与えるには歩いてコマ投げに行く必要がある。
//反応速度は15Fで計算しているので、7Fまで歩いて8Fコマ投げに言っても反応されない前提(そんな入力は無理だと思うが、バッファは8Fあるから・・・？)。
//P側はバクステすれば前ステされない限り何のリスクもない。前ステの場合3f技が確定する。
$af_ex=[
"af_ex            G      Bw      FG     FGL       Vj      Bs        M        L      Dp     WCmd       Fs",
"cMP        mplg+13 mplg+13 mplg+13 mplg+13 mplh+105   wif+0 mplc+117 mplc+117  dp-150 mplc+117 mplh+105",
"P_Cmd       cg+168  hh-238  hh-238  hh-238   jh-300  hh-238  mkc-190  lpc-154  dp-150   cg+168   cg+168",//後ろに歩かれるとスカる
"WalkCg      cg+168  hh-238  cg+141 lpc-154   jh-300  hh-238  mkc-190  lpc-154  dp-150   cg+168   cg+168",//ファジーガードを投げるために4F歩いてコマ投げ
"cMK>EX      mxg+18 mxh+168  mxg+18  mxg+18   aa+120  mkk+50   mkd-10  lpc-154  dp-150  mxh+168  mxh+168",//5F歩かれると小決起が当たらない。EX決起でないとリスクを与えられない。
"5HP>H/Mdisc hpg+13  hpg+13  hpg+13 hps+192   hpk+80   wif+0  mkc-190  lpc-154  dp-150  hmh+170  hmh+170",
"P_Fs         unk+0   unk+0   unk+0   lp-30   aa+120 mkh+178  mkh-178  lph-154  dp-150   cg-168    pl2+0",
"sLP>EX      lxg+18  lxg+18  lxg+18  lxg+18  lxh+138 lxh+138  lxc+144  lxc+144  dp-150  lxc+144  lxh+138",
"P_G            g+0     g+0     g+0     g+0   aa+120     g+0      g+0      g+0 hkc+290   cg-168    unk+0",
"Bs             g+0     g+0     g+0     g+0   aa+120     g+0      g+0      g+0 hkc+290 hhhh+224  llx-153"];

$tables = [
  af_ex => [
    table => $af_ex,
    options => [
      no_dp => [deltags => [Dp]],
      no_lpex => [deltags => ["sLP>EX"]],
      no_cmd => [deltags => [WCmd]]
    ],
  ]
];

$execs = [
  af_ex => [af_ex],
  af_ex_no_dp => [af_ex => [no_dp]],//dpがないとcmdが存在する意味がないのでno_dpはcmdも消すことになる
  af_ex_no_lpex => [af_ex => [no_lpex]],
  af_ex_no_dp_lpex => [af_ex => [no_dp, no_lpex]]
];
$master = [tables => $tables, execs => $execs];

echo json_encode($master);


/*
af_ex 期待値　16.61
攻撃側: P_G<37.9%> | sLP>EX<37.7%> | cMK>EX<11.9%> | 5HP>H/Mdisc<7.8%> | WalkCg<4.7%> | 0%:Bs,P_Fs,
防御側: FG<61.4%> | FGL<13.3%> | Bw<13.0%> | L<6.5%> | Dp<5.7%> | 0%:Fs,WCmd,M,Bs,Vj,
["P_Cmd", "cMP", "G"] removed

af_ex_no_dp 期待値　26.73
攻撃側: sLP>EX<60.6%> | cMK>EX<19.2%> | 5HP>H/Mdisc<12.5%> | WalkCg<7.6%> | 0%:P_Fs,
防御側: FG<65.2%> | FGL<14.1%> | Bw<13.8%> | L<6.9%> | 0%:M,Bs,Vj,
["P_G", "P_Cmd", "cMP", "WCmd", "G", "Bs", "Fs"] removed

60%もEX決起撃ってたらゲージが足らん。

af_ex_no_dp_lpex 期待値　17.41
攻撃側: cMP<63.3%> | P_Fs<12.5%> | cMK>EX<10.8%> | 5HP>H/Mdisc<9.2%> | WalkCg<4.3%> | 0%:Bs,P_G,
防御側: FG<63.9%> | Bs<16.9%> | FGL<9.6%> | L<6.4%> | Bw<3.3%> | 0%:Fs,WCmd,M,Vj,
["P_Cmd", "G"] removed

63%中Pを重ねる。あとはバクステには前ステ,後ろ下がりには中足、
ファジーコパンには大パン、ファジーガードには歩きコマ投げ。


af_ex_no_lpex 期待値　11.04
攻撃側: cMP<40.1%> | P_G<33.3%> | P_Fs<7.9%> | cMK>EX<6.9%> | 5HP>H/Mdisc<5.8%> | Bs<3.3%> | WalkCg<2.7%> | 0%:
防御側: FG<61.4%> | Bs<16.3%> | FGL<9.3%> | L<6.1%> | Dp<3.8%> | Bw<3.2%> | 0%:Fs,WCmd,M,Vj,
["P_Cmd", "G"] removed

昇龍がある場合は中Pとガードをバランス良く置いて、残り1/4でバクステに前ステ、後ろ下がりには中足、
ファジーコパンに大パン、ファジーガードには歩きコマ投げをあわせる。といってもコマ投げは2.7%だ・・・。


*af_ex

       af_ex       G      Bw      FG     FGL       Vj      Bs        M        L      Dp     WCmd       Fs
         cMP mplg+13 mplg+13 mplg+13 mplg+13 mplh+105   wif+0 mplc+117 mplc+117  dp-150 mplc+117 mplh+105
       P_Cmd  cg+168  hh-238  hh-238  hh-238   jh-300  hh-238  mkc-190  lpc-154  dp-150   cg+168   cg+168
      WalkCg  cg+168  hh-238  cg+141 lpc-154   jh-300  hh-238  mkc-190  lpc-154  dp-150   cg+168   cg+168
      cMK>EX  mxg+18 mxh+168  mxg+18  mxg+18   aa+120  mkk+50   mkd-10  lpc-154  dp-150  mxh+168  mxh+168
 5HP>H/Mdisc  hpg+13  hpg+13  hpg+13 hps+192   hpk+80   wif+0  mkc-190  lpc-154  dp-150  hmh+170  hmh+170
        P_Fs   unk+0   unk+0   unk+0   lp-30   aa+120 mkh+178  mkh-178  lph-154  dp-150   cg-168    pl2+0
      sLP>EX  lxg+18  lxg+18  lxg+18  lxg+18  lxh+138 lxh+138  lxc+144  lxc+144  dp-150  lxc+144  lxh+138
         P_G     g+0     g+0     g+0     g+0   aa+120     g+0      g+0      g+0 hkc+290   cg-168    unk+0
          Bs     g+0     g+0     g+0     g+0   aa+120     g+0      g+0      g+0 hkc+290 hhhh+224  llx-153

*af_ex_no_dp

       af_ex       G      Bw      FG     FGL       Vj      Bs        M        L     WCmd       Fs
         cMP mplg+13 mplg+13 mplg+13 mplg+13 mplh+105   wif+0 mplc+117 mplc+117 mplc+117 mplh+105
       P_Cmd  cg+168  hh-238  hh-238  hh-238   jh-300  hh-238  mkc-190  lpc-154   cg+168   cg+168
      WalkCg  cg+168  hh-238  cg+141 lpc-154   jh-300  hh-238  mkc-190  lpc-154   cg+168   cg+168
      cMK>EX  mxg+18 mxh+168  mxg+18  mxg+18   aa+120  mkk+50   mkd-10  lpc-154  mxh+168  mxh+168
 5HP>H/Mdisc  hpg+13  hpg+13  hpg+13 hps+192   hpk+80   wif+0  mkc-190  lpc-154  hmh+170  hmh+170
        P_Fs   unk+0   unk+0   unk+0   lp-30   aa+120 mkh+178  mkh-178  lph-154   cg-168    pl2+0
      sLP>EX  lxg+18  lxg+18  lxg+18  lxg+18  lxh+138 lxh+138  lxc+144  lxc+144  lxc+144  lxh+138
         P_G     g+0     g+0     g+0     g+0   aa+120     g+0      g+0      g+0   cg-168    unk+0
          Bs     g+0     g+0     g+0     g+0   aa+120     g+0      g+0      g+0 hhhh+224  llx-153

*af_ex_no_dp_lpex

       af_ex       G      Bw      FG     FGL       Vj      Bs        M        L     WCmd       Fs
         cMP mplg+13 mplg+13 mplg+13 mplg+13 mplh+105   wif+0 mplc+117 mplc+117 mplc+117 mplh+105
       P_Cmd  cg+168  hh-238  hh-238  hh-238   jh-300  hh-238  mkc-190  lpc-154   cg+168   cg+168
      WalkCg  cg+168  hh-238  cg+141 lpc-154   jh-300  hh-238  mkc-190  lpc-154   cg+168   cg+168
      cMK>EX  mxg+18 mxh+168  mxg+18  mxg+18   aa+120  mkk+50   mkd-10  lpc-154  mxh+168  mxh+168
 5HP>H/Mdisc  hpg+13  hpg+13  hpg+13 hps+192   hpk+80   wif+0  mkc-190  lpc-154  hmh+170  hmh+170
        P_Fs   unk+0   unk+0   unk+0   lp-30   aa+120 mkh+178  mkh-178  lph-154   cg-168    pl2+0
         P_G     g+0     g+0     g+0     g+0   aa+120     g+0      g+0      g+0   cg-168    unk+0
          Bs     g+0     g+0     g+0     g+0   aa+120     g+0      g+0      g+0 hhhh+224  llx-153

*af_ex_no_lpex

       af_ex       G      Bw      FG     FGL       Vj      Bs        M        L      Dp     WCmd       Fs
         cMP mplg+13 mplg+13 mplg+13 mplg+13 mplh+105   wif+0 mplc+117 mplc+117  dp-150 mplc+117 mplh+105
       P_Cmd  cg+168  hh-238  hh-238  hh-238   jh-300  hh-238  mkc-190  lpc-154  dp-150   cg+168   cg+168
      WalkCg  cg+168  hh-238  cg+141 lpc-154   jh-300  hh-238  mkc-190  lpc-154  dp-150   cg+168   cg+168
      cMK>EX  mxg+18 mxh+168  mxg+18  mxg+18   aa+120  mkk+50   mkd-10  lpc-154  dp-150  mxh+168  mxh+168
 5HP>H/Mdisc  hpg+13  hpg+13  hpg+13 hps+192   hpk+80   wif+0  mkc-190  lpc-154  dp-150  hmh+170  hmh+170
        P_Fs   unk+0   unk+0   unk+0   lp-30   aa+120 mkh+178  mkh-178  lph-154  dp-150   cg-168    pl2+0
         P_G     g+0     g+0     g+0     g+0   aa+120     g+0      g+0      g+0 hkc+290   cg-168    unk+0
          Bs     g+0     g+0     g+0     g+0   aa+120     g+0      g+0      g+0 hkc+290 hhhh+224  llx-153

*/
?>