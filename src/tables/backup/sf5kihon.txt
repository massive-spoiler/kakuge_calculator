Forward Jump
Dhalsim 71(3+64+4) F.A.N.G 51(3+44+4) Abigail 50(8+36+6) Birdie 49(4+41+4) Zangief 49 (5+39+5) Cammy 48 (3+41+4) Menat 48 (3+41+4) alex 46(3+39+4) Balrog 46(3+39+4) Kolin 46 (3+39+4) M.Bison 46 (3+39+4) Necalli 46 (3+39+4) Vega 46 (3+39+4) Akuma 45(3+38+4) Ed 45(3+38+4) Guile 45 (3+38+4) Ibuki 45 (3+38+4) Juri 45 (3+38+4) Karin 45 (3+38+4) Ken 45 (3+38+4) Laura 45 (3+38+4) Nash 45 (3+38+4) Rashid 45 (3+38+4) R.Mika 45 (3+38+4) Ryu 45 (3+38+4) Urien 45(3+38+4)
Throw Range
alex 0.9 Abigail 0.9 Birdie 0.9 M.Bison 0.9 Zangief 0.9 Vega 0.8557 Rashid 0.852 Balrog 0.85 Dhalsim 0.85 Ed 0.85 F.A.N.G 0.85 Karin 0.85 Kolin 0.85 Laura 0.85 Menat 0.85 Nash 0.85 R.Mika 0.85 Urien 0.85 Cammy 0.8461 Necalli 0.814 Akuma 0.8 Guile 0.8 Ibuki 0.8 Juri 0.8 Ken 0.8 Ryu 0.8
B.Jump Distance
M.Bison 234 F.A.N.G 233.2 Rashid 205.2 Nash 198.6 Dhalsim 192 Ibuki 192 alex 187.2 Necalli 187.2 Vega 187.2 Cammy 184.5 Guile 182.4 Juri 182.4 Karin 182.4 Ken 182.4 Laura 182.4 R.Mika 182.4 Ryu 182.4 Akuma 182 Urien 171 Balrog 163.8 Zangief 156 Birdie 131.2
Throw Hurtbox Range
alex 0.45 Laura 0.41 Vega 0.4087 Rashid 0.402 Abigail 0.4 Zangief 0.4 Cammy 0.3511 Balrog 0.35 Kolin 0.35 Necalli 0.324 Birdie 0.3 Akuma 0.25 Dhalsim 0.25 Ed 0.25 F.A.N.G 0.25 Guile 0.25 Ibuki 0.25 Juri 0.25 Karin 0.25 Ken 0.25 M.Bison 0.25 Menat 0.25 Nash 0.25 R.Mika 0.25 Ryu 0.25 Urien 0.25
Neutral Jump
Dhalsim 72(4+64+4) F.A.N.G 51(4+43+4) Abigail 50(8+36+6) Birdie 50(5+41+4) Cammy 49 (4+41+4) Menat 49 (4+41+4) Zangief 49 (5+39+5) alex 47(4+39+4) Balrog 47(4+39+4) Kolin 47 (4+39+4) M.Bison 47 (4+39+4) Necalli 47 (4+39+4) Vega 47 (4+39+4) Akuma 46(4+38+4) Ed 46(4+38+4) Guile 46 (4+38+4) Ibuki 46 (4+38+4) Juri 46 (4+38+4) Karin 46 (4+38+4) Ken 46 (4+38+4) Laura 46 (4+38+4) Nash 46 (4+38+4) Rashid 46 (4+38+4) R.Mika 46 (4+38+4) Ryu 46 (4+38+4) Urien 46 (4+38+4)
Forward Walk
Vega 5.5 Cammy 5.4 Akuma 5.2 Guile 5.2 Ibuki 5.2 Juri 5 Karin 4.9 Balrog 4.8 Ed 4.7 Necalli 4.7 Ryu 4.7 Urien 4.35 Ken 4.2 R.Mika 4.2 Kolin 4.13 Menat 4 Rashid 4 alex 3.8 Laura 3.8 F.A.N.G 3.2 Birdie 3.1 Abigail 3.05 Zangief 3 Nash 2.7 Dhalsim 2.2 M.Bison 2
Back Dash
Birdie 26 Abigail 25 Dhalsim 25 Guile 25 Urien 25 Zangief 25 alex 24 Balrog 24 F.A.N.G 24 Juri 24 Ken 24 Menat 24 Nash 24 R.Mika 24 M.Bison 22 Necalli 22 Akuma 21 Cammy 21 Ed 21 Ibuki 21 Karin 21 Kolin 21 Laura 21 Ryu 21 Vega 21
Stun
alex 1050 Abigail 1050 Zangief 1050 Balrog 1000 Birdie 1000 Ed 1000 Ken 1000 Kolin 1000 Laura 1000 M.Bison 1000 Necalli 1000 Rashid 1000 R.Mika 1000 Ryu 1000 Urien 1000 Cammy 950 Dhalsim 950 F.A.N.G 950 Guile 950 Ibuki 950 Juri 950 Karin 950 Menat 950 Nash 950 Vega 950 Akuma 900
F.Jump Distance
F.A.N.G 250.4 M.Bison 234 Dhalsim 224 Ibuki 224 Necalli 214.5 Akuma 213 Guile 212.8 Karin 212.8 Ken 212.8 Laura 212.8 Rashid 212.8 R.Mika 212.8 Ryu 212.8 alex 210.17 Juri 209 Balrog 206.7 Nash 205.2 Cammy 205 Vega 195 Urien 190 Zangief 156 Birdie 143.5
Back Jump
Dhalsim 72(4+64+4) F.A.N.G 52(4+44+4) Abigail 50(8+36+6) Birdie 50(5+41+4) Cammy 49 (4+41+4) Menat 49 (4+41+4) Zangief 49 (5+39+5) alex 47(4+39+4) Balrog 47(4+39+4) Kolin 47 (4+39+4) M.Bison 47 (4+39+4) Necalli 47 (4+39+4) Vega 47 (4+39+4) Akuma 46(4+38+4) Ed 46(4+38+4) Guile 46 (4+38+4) Ibuki 46 (4+38+4) Juri 46 (4+38+4) Karin 46 (4+38+4) Ken 46 (4+38+4) Laura 46 (4+38+4) Nash 46 (4+38+4) Rashid 46 (4+38+4) R.Mika 46 (4+38+4) Ryu 46 (4+38+4) Urien 46 (4+38+4)
Health
Abigail 1100 Zangief 1050 alex 1025 Birdie 1025 Balrog 1000 Ed 1000 Ken 1000 Kolin 1000 Laura 1000 M.Bison 1000 Necalli 1000 Ryu 1000 Urien 1000 Vega 1000 Cammy 950 F.A.N.G 950 Guile 950 Ibuki 950 Juri 950 Menat 950 Nash 950 Rashid 950 R.Mika 950 Dhalsim 925 Akuma 900 Karin 900
Back Walk
Vega 4.5 Akuma 3.6 Cammy 3.6 Karin 3.6 Rashid 3.6 Ken 3.5 Ibuki 3.4 Ryu 3.4 alex 3.3 Guile 3.3 Juri 3.3 Kolin 3.26 Ed 3.2 Laura 3.2 Menat 3.2 Necalli 3.2 Balrog 3 R.Mika 3 Urien 3 F.A.N.G 2.8 Nash 2.7 Abigail 2.5 Birdie 2.4 M.Bison 2.3 Zangief 2.3 Dhalsim 2
if held Back Dash
Rashid 24
Forward Dash
Abigail 25 Zangief 25 Birdie 23 M.Bison 22 Dhalsim 21 F.A.N.G 20 Menat 20 alex 19 Guile 18 Nash 18 R.Mika 18 Balrog 17 Kolin 17 Laura 17 Necalli 17 Vega 17 Akuma 16 Ed 16 Ibuki 16 Juri 16 Karin 16 Ryu 16 Urien 16 Cammy 15 Ken 15 Rashid 15 (41