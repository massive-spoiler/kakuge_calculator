<?php
error_reporting(E_ALL ^ E_WARNING);

//密着+1状況、距離調節は一切できない。タゲコン後のその場受け身である。
//後ろ受け身をしても投げが届く状況なのでほぼ通常の起き上がりと期待値は変わらず、こちらの方が得である。
$main_table = [
'plus1        G       M      L3      Vj      Dp     DeG      BG      Fj      Bj      Bs      Th     F5T     F5M    F5L3     F8M    F12M      CG    F5CG', //CAブッパも必要
'CgT7    cg+168 mkc-190 lmc-194  jh-300  dp-150  cg+168 hhd-224 hhd-224       0 hhd-224  th-120  th-120  cg+168  cg+168  cg+168  cg+168  cg+168  cg+168',
'MT4     mkg+17 mkc+190 lmc-194 mkh+178  dp-150  mkg+17  mkg+17 mkh+178 mkh+178   mc+72 mkc+190  mkg+17  mkg+17  mkg+17  mkg+17  mkg+17 mkc+190  mkg+17', //投げられる心配がないので密着で良い。起き攻めなら最速バクステも空中ヒットにはならない。
'LT2       @lpg    @lpc    @lpc    @lph  dp-150    @lpg    @lpg    @lph    @lph    @lph    @lpc    @lpg    @lpg    @lpg    @lpg    @lpg    @lpc    @lpg', //2フレームならバクステは地上くらいになるので高いコンボができる
'Th_T4   th+120  th+120 lmc-194  aa+120  dp-150       0  th+120  aa+120       0   unk+0       0       0  th+120  th+120  th+120  th+120  th+120  th+120', //通常投げは理論上4Fジャンプコマ投げでかわせる・・・ガードと両立するのはクソ難しいはずだが・・・
'P_G          0  mkg-17  lpg-17  aa+120 hkc+290  th-120       0  aa+120       0       0  th-120  th-120  mkg-17  lpg-17  mkg-17  mkg-17  cg-168  cg-168', //コマ投げが届く間合い外からでは中攻撃ではダメージが取れない。
'Fs           0 mkh-178   lh-30  aa+120  dp-150  th-120       0       0   jl-40 mkh+178  th-120  th-120 mkh-178   lp-30 mkh-178 mkh-178  cg-168  cg-168',
'MT6     mkg+17 mkc-190 lmc-194  mka+60  dp-150  mkg+17  mkg+17  mka+60  mka+60  mks+72  th-120 mkc+190 mkc+190 mkc+190  mkg+17  mkg+17 mkc+190 mkc+190',//一応Mも用意
'L3T7    lpg+17 mkc-190 lmc-194  lpa+30 hkc+290  lpg+17   lp+30  lpa+30  lpa+30  lps+36  th-120 lmc+194 lmc+194   unk+0  lpg+17  lpg+17 lmc+194 lmc+194',//昇龍をガードし、コマ投げを咎める対ネカリ専用？ムーブ。4Fコパンだとビタになるので3f持ちしか出来ないだろう。
'MT9     mkg+17 mkc-190  lpg-17  aa+120 hkc+290 mkc+190  mks+72  aa+120   jl-40  mks+72  th-120   unk+0   unk+0 lmc-194 mkc+190  mkg+17  cg-168 mkc+190',//<-投げ間合い外から、昇龍暴れをガードしたタイミングで中攻撃でグラつぶしになる。バクステに対して中攻撃ではダメージを取れないだろう
'CT9     hrg+28 mkc-190 lmc-194  aa+120  dp-150 hmh+170 hmh+170  aa+120   jl-40 hms+176  th-120   unk+0   unk+0 lmc-194 hms+176  hrg+28  cg-168 hms+176',//密着から5HP、ヒット確認して中突進。ガード確認中決起レンガ（できるか？）
'MT15    mkg+17  mkg-17  lpg-17  aa+120 hkc+290 mkc+190       0  aa+120       0       0 mkh+178 mkh+178   unk+0   lpg-0 mkc-190 mkh+178  cg-168  cg-168',//頑張って後ろに下がっての投げ間合い外中K
'CT15    hrg+28  mkg-17  lpg-17  jh-300 hkc+290 hmh+170 hmh+170       0   jl-40 hmh+170  th-120  th-120 mkc-190 lmc-194 mkc-190 hms+176  cg-168  cg-168',//密着から5HP、ヒット確認して中突進。ガード確認中決起レンガ（できるか？）
'LT20     unk+0  mkg-17  lpg-17  aa+120 hkc+290  th-120 lxh+138  aa+120 mkh+178 lxh+138  th-120  th-120 mkh-178  lpg-17 mkh-178 mkh-178  cg-168  cg-168',//<-ガードしながら7-8Fのバックジャンプコパンをかわし、前に歩いて状況を見極め、バクステとバックジャンプを刈る。着地に確定取るのは難しいので要練習。バクステは最速のものでも見てから理論上発生6Fの技までは当てられるが、まあコパンがせいぜいである。バクステと前歩きの速度は最終的にはたいして変わらないので、しっかり歩けば距離はほとんどの場合問題にならないだろう。相手が動いたのを確認できない場合コパコパが良いはず。
'CT20    hrg+28  mkg-17  lpg-17  jh-300 hkc+290 hmh+170 hms+176       0       0 hmh+170  th-120  th-120  mkg-17  lpg-17 mkc-190 mkc-190  cg-168  cg-168'];//密着から5HP、ヒット確認して中突進。ガード確認中決起レンガ（できるか？）
//'-6FRevspi -RevG   -D1M  -Vjc  -D1L  -Dp Rev RevA AA   0 RevA  Rev  Rev  -D1M -D1LRev_7M-RevG Rev Rev']//<-もちろんネカリにはこんな技はない。'-10FSekisei' '-6FSekisei'なんかもそのうち調べたい

$lpg =[
"lpg              G      G>L3     G>Dp      Vj        Bs         M        L3        L4       Dp       Cmd",
"sMK        mkpg+20   mkpg+20  gdp-140 mkh+178    mks+72   mkc+190   lpc-154   mkc+190   dp-150   mkc+190",//G>Lはコパンの場合暴れるが中Kガードを見てから暴れをやめる、中Kの後はコパンで暴れても仕方ない。
"sMK>G       mkg+10    mkg+10 hkc2+300   mh+60    mks+72     mc+72   lpc-154     mc+72   dp-150     mc+72",
"P_Cmd       cg+168    cg+168   cg+168  jh-300    hh-238   mkc-190   lpc-154   lpc-154   dp-150    cg+168",
"sHP         hrg+28    hrg+28   hrg+28  hpa+80   hps+192   mkc-190   lpc-154   lpc-154   dp-150   hmh+170",//単発確認突進狙い
"sLP>EX/2MP  lmg+10   lpc-154   dp-150  lph+30    lph+30   lpc+154   lpc+154   lpc+154   dp-150   lpc+154", //
"sLP>EX/Lre  lrg+13   lrd+202   dp-150  lph+30    lph+30    lrc+81    lrc+81    lrc+81   dp-150    lrc+81", //5LKが当たっていない場合小決起にする
"sLP>EX      exg+18    exg+18   exg+18 exh+138   exh+138   exc+144   exc+144   exc+144   dp-150   exc+144",//5LKがあたってなくても入れ込む。連ガである。
"sLP>G          g+0       g+0  hkc+290  p_u+30    p_u+30   pc_u+36   pc_u+36   pc_u+36   dp-150   pc_u+36",
"P_G            g+0       g+0      g+0  aa+120       g+0    mkg-17       g+0       g+0  hkc+290    cg-168"];

$lph =[
"lph              G      G>L3     G>Dp      Vj        Bs         M        L3        L4       Dp       Cmd",
"sMK        mkpg+50   mkpg+50   gdp-90 mkh+208   mks+102   mkc+220   mkc+220   mkc+220   dp-120   mkc+220",//G>Lはコパンの場合暴れるが中Kガードを見てから暴れをやめる、中Kの後はコパンで暴れても仕方ない。
"sMK>G       mkg+40    mkg+40 hkc2+330   mh+90   mks+102    mc+102    mc+102    mc+102   dp-120    mc+102",//
"P_Cmd       cg+198    cg+198   cg+198  jh-270    hh-198    cg+198   lpc-124   lpc-124   dp-120    cg+198",
"sHP         hrg+58    hrg+58   hrg+58 hpa+110   hps+222   mkc-160   lpc-124   lpc-124   dp-120   hmh+200",//単発確認突進狙い
"sLP>EX/2MP  ll+153    ll+153   ll+153  ll+153    ll+153    ll+153    ll+153    ll+153   ll+153    ll+153", //
"sLP>EX/Lre  ll+153    ll+153   ll+153  ll+153    ll+153    ll+153    ll+153    ll+153   ll+153    ll+153", //5LKが当たっていない場合小決起にする
"sLP>EX      ll+153    ll+153   ll+153  ll+153    ll+153    ll+153    ll+153    ll+153   ll+153    ll+153",//5LKがあたってなくても入れ込む。連ガである。
"sLP>G       ll+153    ll+153   ll+153  ll+153    ll+153    ll+153    ll+153    ll+153   ll+153    ll+153",
"P_G           g+30      g+30     g+30  aa+150      g+30    mkg+13      g+30      g+30  hkc+320    cg-138"];

$lpc =[
"lpc              G      G>L3     G>Dp      Vj        Bs         M        L3        L4       Dp       Cmd",
"sMK        lmc+194   lmc+194  lmc+194 lmc+194   lmc+194   lmc+194   lmc+194   lmc+194  lmc+194   lmc+194",//G>Lはコパンの場合暴れるが中Kガードを見てから暴れをやめる、中Kの後はコパンで暴れても仕方ない。
"sMK>G      lmc+194   lmc+194  lmc+194 lmc+194   lmc+194   lmc+194   lmc+194   lmc+194  lmc+194   lmc+194",//
"P_Cmd       cg+204    cg+204   cg+204  jh-264    hh-192    cg+204   lpc-118   lpc-118   dp-114    cg+204",
"sHP         hrg+58    hrg+58   hrg+58 hpa+110   hps+222   mkc-160   lpc-124   lpc-124   dp-120   hmh+200",//単発確認突進狙い
"sLP>EX/2MP  ll+159    ll+159   ll+159  ll+159    ll+159    ll+159    ll+159    ll+159   ll+159    ll+159", //
"sLP>EX/Lre  ll+159    ll+159   ll+159  ll+159    ll+159    ll+159    ll+159    ll+159   ll+159    ll+159", //5LKが当たっていない場合小決起にする
"sLP>EX      ll+159    ll+159   ll+159  ll+159    ll+159    ll+159    ll+159    ll+159   ll+159    ll+159",//5LKがあたってなくても入れ込む。連ガである。
"sLP>G       ll+159    ll+159   ll+159  ll+159    ll+159    ll+159    ll+159    ll+159   ll+159    ll+159",
"P_G           g+36      g+36     g+36  aa+156      g+36    mkg+19      g+36      g+36  hkc+326    cg-132"];



$tables = [
  main_table => [
    table => $main_table,
    options => [
      no_dp => [deltags => [Dp]],
      no_cmd => [deltags => [CG,F5CG]],
      no_3f => [deltags => [L3,F5L3]],
      no_l2 => [deltags => LT2]
    ],
  ],
  lpg => [
    table => $lpg,
    options => [
      no_dp => [deltags => [Dp,"G>Dp"]],
      no_ex => [deltags => "sLP>EX"],
      no_cmd => [deltags => "Cmd"],
      no_3f => [deltags => [L3, "G>L3"]]
    ],
  ],
  lpc => [
    table => $lpc,
    options => [
      no_dp => [deltags => [Dp,"G>Dp"]],
      no_ex => [deltags => "sLP>EX"],
      no_cmd => [deltags => "Cmd"],
      no_3f => [deltags => [L3, "G>L3"]]
    ],
  ],
  lph => [
    table => $lph,
    options => [
      no_dp => [deltags => [Dp,"G>Dp"]],
      no_ex => [deltags => "sLP>EX"],
      no_cmd => [deltags => "Cmd"],
      no_3f => [deltags => [L3, "G>L3"]]
    ],
  ],
];

$execs = [
  do_main => [main_table, lpg => no_ex, lph => no_ex,lpc => no_ex],
  has_ex => [main_table, lpg, lpc,lph],

  no_dp => [main_table => no_dp,
    lpg => [no_dp, no_ex], lpc => [no_dp, no_ex],lph => [no_dp, no_ex]],
  ex_no_dp => [main_table => no_dp,
    lpg => [no_dp],lpc => [no_dp],lph => [no_dp]],
  has_dp => [main_table => [no_cmd],
    lpg => [no_cmd, no_ex], lpc => [no_cmd, no_ex],lph => [no_cmd, no_ex]],
  ex_has_dp => [main_table => [no_cmd],
    lpg => [no_cmd], lpc => [no_cmd],lph => [no_cmd]],

  no_dp_cmd => [main_table => [no_dp, no_cmd],
    lpg => [no_dp, no_cmd, no_ex], lpc => [no_dp, no_cmd, no_ex],lph => [no_dp, no_cmd, no_ex]],
  ex_no_dp_cmd => [main_table => [no_dp, no_cmd],
    lpg => [no_dp, no_cmd], lpc => [no_dp, no_cmd],lph => [no_dp, no_cmd]],


  no_3f => [main_table =>[no_3f],
    lpg => [no_3f, no_ex], lpc => [no_3f, no_ex],lph => [no_3f, no_ex]],
  ex_no_3f => [main_table =>[no_3f],
      lpg => [no_3f], lpc => [no_3f],lph => [no_3f]],
  no_dp_3f => [main_table => [no_dp, no_3f],
    lpg => [no_dp, no_3f, no_ex], lpc => [no_dp, no_3f, no_ex],lph => [no_dp, no_3f, no_ex]],
  ex_no_dp_3f => [main_table => [no_dp, no_3f],
    lpg => [no_dp, no_3f], lpc => [no_dp, no_3f],lph => [no_dp, no_3f]],

  has_dp_no_3f => [main_table => [no_cmd, no_3f],
    lpg => [no_cmd, no_3f, no_ex],lpc => [no_cmd,no_3f, no_ex],lph => [no_cmd, no_3f, no_ex]],
  ex_has_dp_no_3f => [main_table => [no_cmd, no_3f],
    lpg => [no_cmd, no_3f],lpc => [no_cmd,no_3f],lph => [no_cmd, no_3f]],

  no_dp_cmd_3f => [main_table => [no_dp, no_cmd, no_3f],
    lpg => [no_dp, no_cmd, no_3f, no_ex],lpc => [no_dp, no_cmd, no_ex, no_3f],lph => [no_dp, no_cmd, no_ex, no_3f]],
  ex_no_dp_cmd_3f => [main_table => [no_dp, no_cmd, no_3f],
      lpg => [no_dp, no_cmd, no_3f],lpc => [no_dp, no_cmd, no_3f],lph => [no_dp, no_cmd, no_3f]],
];
$master = [tables => $tables, execs => $execs];

echo json_encode($master);
/*
3フレがない場合と昇竜がない場合は十分な期待値が取れるので、画面中央のタゲコンは狙っていくべし、という結論になるだろうか。
ただ単発確認が難しすぎ、ミスると反確なので実戦で使うのは難しい。

do_main 期待値　4.77
攻撃側: LT2|P_G<23.1%> | P_G<15.9%> | L3T7<13.0%> | LT2|sLP>EX/2MP<12.2%> | LT2|sLP>EX/Lre<8.1%> | LT2|sLP>G<6.4%> | LT2|P_Cmd<5.0%> | LT2|sHP<4.7%> | MT15<4.6%> | LT2|sMK>G<3.3%> | MT9<1.7%> | CgT7<1.3%> | Th_T4<0.7%> |
防御側: L3|Cmd<23.5%> | Dp<14.2%> | F5CG|Dp<11.5%> | M|Cmd<9.2%> | F5L3|Dp<8.6%> | F5L3|L3<8.4%> | F8M|Dp<7.6%> | F5L3|Cmd<6.7%> | F5L3|G>L3<3.3%> | F5L3|G>Dp<3.2%> | DeG|Bs<2.4%> | F5CG|M<1.2%> | DeG|Dp<0.2%> |

has_ex 期待値　6.57
攻撃側: LT2|P_G<24.5%> | LT2|sLP>EX<23.1%> | P_G<11.5%> | MT15<10.7%> | LT2|P_Cmd<9.2%> | L3T7<8.4%> | MT9<4.9%> | Th_T4<2.6%> | LT2|sMK<2.3%> | LT2|sHP<1.5%> | LT2|sLP>EX/2MP<1.2%> |
防御側: M|Cmd<21.9%> | L3|Cmd<21.1%> | F5L3|Dp<17.9%> | Dp<17.4%> | F5CG|Dp<11.9%> | F5CG|L3<5.2%> | F8M|Dp<1.8%> | CG|Cmd<1.4%> | F5CG|M<0.9%> | F5CG|Bs<0.4%> |

has_dp 期待値　13.19
攻撃側: P_G<20.3%> | LT2|P_G<17.1%> | LT2|sMK>G<10.3%> | LT2|sLP>EX/2MP<9.9%> | MT6<9.9%> | LT2|sLP>EX/Lre<8.6%> | MT9<7.7%> | MT15<6.8%> | Th_T4<4.1%> | L3T7<2.2%> | CgT7<1.9%> | LT2|P_Cmd<0.9%> | LT2|sHP<0.2%> |
防御側: F5L3|G<26.9%> | F8M|G<20.8%> | L3|Bs<18.8%> | Dp<13.2%> | F5T|Dp<7.7%> | F5L3|L3<3.9%> | M|M<3.1%> | BG|G>Dp<1.2%> | M|Bs<1.1%> | DeG|Dp<1.0%> | DeG|G<1.0%> | BG|L3<0.8%> | DeG|G>L3<0.5%> |

ex_has_dp 期待値　14.30
攻撃側: P_G<20.5%> | LT2|P_G<17.8%> | LT2|sLP>EX<17.7%> | MT6<9.0%> | MT9<7.3%> | LT2|sMK<7.1%> | MT15<6.8%> | LT2|sMK>G<3.9%> | Th_T4<3.8%> | L3T7<2.8%> | CgT7<2.1%> | LT2|P_Cmd<1.0%> | LT2|sHP<0.2%> |
防御側: F5L3|G<28.1%> | L3|Bs<18.4%> | Dp<13.4%> | F8M|G<11.3%> | F8M|Dp<9.1%> | F5T|G<3.4%> | F5L3|G>L3<3.2%> | DeG|G<3.0%> | BG|G<2.9%> | M|L3<2.7%> | F5T|L3<2.3%> | F5T|G>Dp<1.2%> | M|M<0.8%> | M|Bs<0.3%> |


no_3f 期待値　40.64
攻撃側: MT4<33.3%> | Th_T4<23.3%> | MT9<18.9%> | MT15<15.1%> | L3T7<6.1%> | P_G<2.7%> | CT15<0.5%> |
防御側: M|Cmd<25.3%> | Dp<21.5%> | G|G<16.4%> | F8M|Bs<13.4%> | F5T|Dp<6.9%> | Bs|G<5.1%> | Bs|M<4.2%> | CG|Cmd<3.8%> | M|M<1.7%> | Bs|G>Dp<1.7%> |

ex_no_3f 期待値　40.64
攻撃側: MT4<33.3%> | Th_T4<23.3%> | MT9<18.9%> | MT15<15.1%> | L3T7<6.1%> | P_G<2.7%> | CT15<0.5%> |
防御側: M|M<27.0%> | Dp<21.5%> | F8M|G<13.4%> | G|M<12.4%> | F5T|Dp<6.9%> | Bs|G<6.8%> | Bs|G>Dp<4.0%> | CG|Cmd<3.8%> | G|Dp<2.3%> | G|G<1.7%> | Bs|Cmd<0.1%> |

has_dp_no_3f 期待値　40.97
攻撃側: MT4<33.1%> | Th_T4<23.5%> | MT9<20.9%> | MT15<16.9%> | L3T7<3.9%> | P_G<1.4%> | CT20<0.4%> |
防御側: M|M<29.2%> | Dp<20.2%> | F8M|Dp<13.9%> | Bs|G>Dp<13.1%> | G|G<8.2%> | F5T|Dp<7.3%> | F5M|G<5.4%> | F5M|M<2.4%> | F8M|Bs<0.3%> |

ex_has_dp_no_3f 期待値　40.97
攻撃側: MT4<33.1%> | Th_T4<23.5%> | MT9<20.9%> | MT15<16.9%> | L3T7<3.9%> | P_G<1.4%> | CT20<0.4%> |
防御側: M|M<29.2%> | Dp<20.2%> | F8M|Bs<11.6%> | Bs|M<8.9%> | G|G<8.2%> | F5T|Dp<7.3%> | F5M|G<5.0%> | Bs|G<4.2%> | F5M|G>Dp<2.8%> | F8M|Dp<2.1%> | F8M|G<0.5%> |



no_dp 期待値　39.62
攻撃側: LT2|sLP>EX/2MP<23.2%> | LT2|sLP>EX/Lre<20.2%> | Th_T4<15.2%> | LT2|sMK<14.3%> | MT6<7.5%> | MT9<5.7%> | LT2|P_Cmd<5.1%> | MT15<4.8%> | LT2|sHP<3.2%> | CgT7<0.6%> | L3T7<0.1%> |
防御側: G|G<39.3%> | F5T|Bs<15.2%> | F8M|G<13.4%> | L3|Vj<7.3%> | F5T|G<6.1%> | F5T|L3<6.0%> | DeG|G<5.6%> | L3|L3<5.5%> | F5T|G>L3<0.7%> | F5L3|G<0.6%> | BG|G<0.4%> |

ex_no_dp 期待値　51.49
攻撃側: LT2|sLP>EX<61.3%> | LT2|P_Cmd<18.9%> | LT2|sMK<9.4%> | Th_T4<4.6%> | MT6<2.3%> | MT9<1.7%> | MT15<1.5%> | CgT7<0.2%> |
防御側: G|G<37.6%> | F5T|Vj<16.1%> | F8M|G<12.8%> | DeG|G<11.2%> | F5T|G<8.9%> | L3|Vj<7.0%> | BG|G>L3<3.0%> | F5T|L3<2.7%> | F5L3|L3<0.8%> |


no_dp_cmd 期待値　39.62
攻撃側: LT2|sLP>EX/2MP<23.2%> | LT2|sLP>EX/Lre<20.2%> | Th_T4<15.2%> | LT2|sMK<14.3%> | MT6<7.5%> | MT9<5.7%> | LT2|P_Cmd<5.1%> | MT15<4.8%> | LT2|sHP<3.2%> | CgT7<0.6%> | L3T7<0.1%> |
防御側: G|G<39.3%> | F5T|Bs<15.2%> | F8M|G<13.4%> | L3|Vj<7.3%> | F5T|G<6.1%> | F5T|L3<6.0%> | DeG|G<5.6%> | L3|L3<5.5%> | F5T|G>L3<0.7%> | F5L3|G<0.6%> | BG|G<0.4%> |

ex_no_dp_cmd 期待値　51.49
攻撃側: LT2|sLP>EX<61.3%> | LT2|P_Cmd<18.9%> | LT2|sMK<9.4%> | Th_T4<4.6%> | MT6<2.3%> | MT9<1.7%> | MT15<1.5%> | CgT7<0.2%> |
防御側: G|G<37.6%> | F5T|Vj<16.1%> | F8M|G<12.8%> | DeG|G<11.2%> | F5T|G<8.9%> | L3|Vj<7.0%> | BG|G>L3<3.0%> | F5T|L3<2.7%> | F5L3|L3<0.8%> |


no_dp_3f 期待値　51.64
攻撃側: LT2|sMK<44.1%> | Th_T4<15.6%> | LT2|sHP<12.1%> | LT2|P_Cmd<10.5%> | MT6<6.8%> | MT9<5.9%> | MT15<4.9%> |
防御側: F5T|G<25.8%> | G|Bs<22.1%> | F8M|G<20.9%> | DeG|G<12.4%> | Th|L4<10.1%> | Bj|G<8.6%> |

ex_no_dp_3f 期待値　53.86
攻撃側: LT2|sLP>EX<35.6%> | LT2|sMK<18.1%> | LT2|P_Cmd<16.1%> | Th_T4<10.4%> | LT2|sHP<8.2%> | L3T7<4.4%> | MT9<3.9%> | MT15<3.3%> |
防御側: F8M|G<25.6%> | G|G<19.3%> | F5T|G<18.0%> | Th|Vj<13.8%> | F5T|Bs<11.1%> | DeG|G<9.7%> | F5T|M<1.9%> | F5T|Vj<0.5%> |


no_dp_cmd_3f 期待値　51.64
攻撃側: LT2|sMK<44.1%> | Th_T4<15.6%> | LT2|sHP<12.1%> | LT2|P_Cmd<10.5%> | MT6<6.8%> | MT9<5.9%> | MT15<4.9%> |
防御側: F5T|G<25.8%> | G|Bs<22.1%> | F8M|G<20.9%> | DeG|G<12.4%> | Th|L4<10.1%> | Bj|G<8.6%> |

ex_no_dp_cmd_3f 期待値　53.86
攻撃側: LT2|sLP>EX<35.6%> | LT2|sMK<18.1%> | LT2|P_Cmd<16.1%> | Th_T4<10.4%> | LT2|sHP<8.2%> | L3T7<4.4%> | MT9<3.9%> | MT15<3.3%> |
防御側: F8M|G<25.6%> | G|G<19.3%> | F5T|G<18.0%> | Th|Vj<13.8%> | F5T|Bs<11.1%> | DeG|G<9.7%> | F5T|M<1.9%> | F5T|Vj<0.5%> |
*/
?>