<?php
error_reporting(E_ALL ^ E_WARNING);
//これらは合成用のテーブルなので単体では意味をなさない

//MKガードの後に昇竜もち相手には2MPで白ダメージを取りにいくかという読みあいがあり、3割ぐらいガードが正解。相手は5%ぐらいで昇竜するべき
//ネカリの足だと2フレーム後ろ歩きをしても5MK2MPは両方ガードさせられる。+2なので後ろ歩きでは外せない。ガードせずに後ろ歩きする俺の知らないテクニックでもあれば別だが・・・
$lkg =[
"lkg              G      G>L3     G>Dp      Vj        Bs         M         L      Dp        Cmd",
"sMK        mkpg+20   mkpg+20  gdp-140 mkh+178    mks+72   mkc+190   mkc+190  dp-150   mkc+190",//G>Lはコパンの場合暴れるが中Kガードを見てから暴れをやめる、中Kの後はコパンで暴れても仕方ない。
"sMK>G       mkg+10    mkg+10 hkc2+300   mh+60    mks+72     mc+72     mc+72  dp-150     mc+72",
"P_Cmd       cg+168    cg+168   cg+168  jh-300    hh-238   mkc-190   lpc-154  dp-150    cg+168",
"sHP         hpg+13    hpg+13   hpg+13  hpa+80   hps+192   mkc-190   lpc-154  dp-150   hps+192",//単発確認突進狙い
"sLP>EX/2MP  lmg+10   lpc-154   dp-150  lph+30    lph+30   lpc+154   lpc+154  dp-150   lpc+154", //5LKが当たっていない場合2MPにする。5LPがカウンターしていれば確認してコンボに
"sLP>EX/Lre  lrg+13   lrd+202   dp-150  lph+30    lph+30    lrc+81    lrc+81  dp-150    lrc+81", //5LKが当たっていない場合小決起にする
"sLP>EX      exg+18    exg+18   exg+18 exh+138   exh+138   exc+144   exc+144  dp-150   exc+144",//5LKがあたってなくても入れ込む。連ガである。
"sLP>EX/G       g+0       g+0  hkc+290  p_u+30    p_u+30   pc_u+36   pc_u+36  dp-150   pc_u+36",
"P_G            g+0       g+0      g+0  aa+120       g+0    mkg-17       g+0 hkc+290   hhd+224"];//P側は後ろ下がりでコマ投げは避けられる。相手がネカリの場合だけだが,コマ投げが守備の選択肢になるのは実質ネカリだけである。

$lkh =[//5LKヒットなので全体に40加算
"lkh              G      G>L3     G>Dp      Vj        Bs         M         L      Dp       Cmd",
"sMK        mkpg+60   mkpg+60  gdp-100 mkh+218   mkh+218   mkc+230   mkc+230  dp-110   mkc+230",//中Kにはカウンターか持続でないとつながらない
"sMK>G       mkg+50    mkg+50 hkc2+340  mh+100    mh+100    mc+112    mc+112  dp-110    mc+112",//Vj,Bsにも地上ヒットするようになるが、ヒット確認できないのでそこでストップとなる
"P_Cmd       cg+208    cg+208   cg+208  jh-260    hh-198    cg+208   lpc-114  dp-110    cg+208",//中攻撃にヒット。4Fなら小技にもヒットするが、そこまで分けるのがしんどい・・・
"sHP         hpg+53    hpg+53   hpg+53 hpa+130   hps+232   hps+232   lpc-114  dp-110   hps+232",//+4から9フレームの技を出すことになるので中攻撃にヒットする
"sLP>EX/2MP lkh+163   lkh+163  lkh+163 lkh+163   lkh+163   lkh+163   lkh+163 lkh+163   lkh+163",//ヒット時はコンボになるので問答無用で当たる。
"sLP>EX/Lre lkh+163   lkh+163  lkh+163 lkh+163   lkh+163   lkh+163   lkh+163 lkh+163   lkh+163",
"sLP>EX     lkh+163   lkh+163  lkh+163 lkh+163   lkh+163   lkh+163   lkh+163 lkh+163   lkh+163",
"sLP>EX/G   lkh+163   lkh+163  lkh+163 lkh+163   lkh+163   lkh+163   lkh+163 lkh+163   lkh+163",
"P_G           g+40      g+40     g+40  aa+160      g+40    mkg-23      g+40 hkc+330   hhd+224"];//コパンの後中パンをガードしなきゃいけないはずだがそこを計算してない・・・

$lkj =[//持続ヒットでは中Kがコンボに
"lkj              G      G>L3     G>Dp       Vj       Bs        M        L       Dp      Cmd",
"sMK       lkmh+198  lkmh+198 lkmh+198 lkmh+198 lkmh+198 lkmh+198 lkmh+198 lkmh+198 lkmh+198",
"sMK>G     lkmh+198  lkmh+198 lkmh+198 lkmh+198 lkmh+198 lkmh+198 lkmh+198 lkmh+198 lkmh+198",//持続確認など存在しない・・・はず。だが4F中3F持続でコンボになるのだから持続確信していいと思う。ちゃんと持続あてになるように練習もすれば出来るだろうし。
"P_Cmd       cg+208    cg+208   cg+208   jh-260   hh-198  mkc-140  lpc-114   dp-110   cg+208",//持続4F目からだと最速コマ投げが当たらなくなる。起き上がりでのコマ投げは少し遅めに出す必要があるか。だから中攻撃にも負けることにする。
"sHP         hpg+53    hpg+53   hpg+53  hpa+130  hps+232  hps+232  lpc-114   dp-110  hps+232",//持続3-4F目なら3Fコパンにも勝てるが、評価には加えていない。
"sLP>EX/2MP lkh+163   lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163",
"sLP>EX/Lre lkh+163   lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163",
"sLP>EX     lkh+163   lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163",
"sLP>EX/G   lkh+163   lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163  lkh+163",
"P_G           g+40      g+40     g+40   aa+160     g+40   mkg-23     g+40  hkc+330  hhd+224"];

$lkc =[//カウンターヒットするとダメージ8プラス、+2フレーム余分にもらえる。
"lkc              G      G>L3     G>Dp       Vj       Bs        M        L      Dp       Cmd",
"sMK       lkmc+206  lkmc+206 lkmc+206 lkmc+206 lkmc+206 lkmc+206 lkmc+206 lkmc+206 lkmc+206",//そもそも小K密着で当てる状況がほぼ端の起き上がりしかないんだが・・・
"sMK>G     lkmc+206  lkmc+206 lkmc+206 lkmc+206 lkmc+206 lkmc+206 lkmc+206 lkmc+206 lkmc+206",//画面中央の読み合いになってるから実用性に疑問がある。
"P_Cmd       cg+216    cg+216   cg+216   jh-252   hh-190  mkc-132  lpc-106   dp-102   cg+216",//ちょっとでも持続だと最速コマ投げが当たらなくなる。やはり送らせなければなるまい
"sHP         hpg+61    hpg+61   hpg+61  hpa+138  hps+240  hpc+240  hpc+240   dp-102  hps+240",//3Fコパンにも勝てる
"sLP>EX/2MP lkh+171   lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171",
"sLP>EX/Lre lkh+171   lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171",
"sLP>EX     lkh+171   lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171",
"sLP>EX/G   lkh+171   lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171  lkh+171",
"P_G           g+48      g+48     g+48   aa+168     g+48   mkg-15     g+48  hkc+338  hhd+224"];

//これらは合成用のテーブルなので単体では意味をなさない




?>