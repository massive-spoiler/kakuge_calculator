<?php
error_reporting(E_ALL ^ E_WARNING);

//投げ後は再度投げに行こうとすると3F,4F小技で咎められる。少しでも近づくとEXコマ投げが当たる距離になるが、
//択にしようとしても入力が間に合わない（やれば出来るのかも知れないが・・・)
$lpg =[
"cor_th           G        Dp       DeG      Lk       M       Fj     F4L",
"P_G              0   hkc+290   tgl+216   lk-10  mkg-17   aa+120       0",
"th          th+120    dp-150         0 lkc-162  th+120        0  th+120",
"HK          hkg+15    dp-150    hkg+15 hhc+282 hhc+282    hk+90  hkg+15",
"FuzTh       th+120   hkc+290         0  aa+120 mkc-190   aa+120 lpc-154",//近づいてから昇竜やコパン暴れのタイミングでガードし、攻撃が来なかったらさらに歩いて投げる。
"MK          mkg+17    dp-150    mkg+17 tgl+216 tgl+216  tgl+216  mkg-10",
"GT6         hrg+28    dp-150    hrg+28 lkc-162 mkc-190   hpa+80 hhs+160"];


$tables = [
  main_table => [
    table => $lpg,
    options => [
      no_dp => [deltags => [Dp]],
      oki => [symbol_nums => [hkc,tgl,hhc,aa,hhs,"+=60"],[th, "+=45"]],
      oki_no_dp => [symbol_nums => [hkc,tgl,hhc,aa,hhs,"+=107"],[th,"+=57"]]
    ],
  ]
];

$execs = [
  main_table => [main_table],
  oki => [main_table => oki],
  no_dp => [main_table => no_dp],
  oki_no_dp => [main_table => [no_dp,oki_no_dp]],
];
$master = [tables => $tables, execs => $execs];

echo json_encode($master);

/*
main_table 期待値　37.15
攻撃側: P_G<42.5%> | HK<30.3%> | th<27.2%> |
防御側: F4L<38.5%> | G<31.5%> | Lk<16.6%> | Dp<13.4%> |

oki 期待値　45.65
攻撃側: P_G<26.8%> | HK<24.8%> | th<18.6%> | GT6<17.6%> | FuzTh<12.3%> |
防御側: F4L<38.2%> | G<32.0%> | Dp<13.6%> | Lk<13.4%> | M<2.8%> |

端で戦う以上起き攻めの期待値を考慮しない意味はあるまい

守備側は大まかに言って、4Fコパン1/3 ガード1/3 昇竜1/6 小K1/6で良いだろう。
攻撃側はガード1/4 大K1/4 投げ、6フレーム攻撃、ファジー投げ1/6といったところ。

no_dp 期待値　51.72
攻撃側: HK<40.9%> | th<38.0%> | P_G<21.1%> |
防御側: F4L<37.8%> | DeG<24.6%> | G<23.9%> | Lk<13.8%> |

oki_no_dp 期待値　61.56
攻撃側: th<46.8%> | HK<35.8%> | P_G<17.4%> |
防御側: G<50.4%> | DeG<19.4%> | F4L<17.7%> | Lk<12.4%> |

昇竜がない相手には、投げ1/2 大K1/3 ガード1/6で。

*/
?>