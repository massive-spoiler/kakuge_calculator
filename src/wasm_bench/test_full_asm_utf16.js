function main() {
const imports = {
    env: {
        timer_start: function() { console.time("t"); },
        timer_stop: function() { console.timeEnd("t"); },
    }
};

    fetch('compact64.wasm').then(response => response.arrayBuffer())
        .then(bytes => WebAssembly.instantiate(bytes, imports))
        .then(results => {

            const ex = results.instance.exports;
            ex.test_full_asm_utf16();






            console.log("done");
        });
}