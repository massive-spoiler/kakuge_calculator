function main(){
fetch('compact64.wasm').then(response => response.arrayBuffer())
    .then(bytes => WebAssembly.instantiate(bytes, {}))
    .then(results => {
        var ss = ["", "1",
            "5", "314.8",
            "direct_str","にほんご"];

        const ex = results.instance.exports;
        var b = ex.create_compact64_encode_input_builder();

        for(let s of ss){
            ex.next_string(b);
            console.log(s);
            for(let c of s){
                ex.enc_builder_append_char(b, c.charCodeAt(0));
                console.log(c.charCodeAt(0));
            }
        }

    var encoded = ex.encode_builder_into_compact64(b);
    var enc_len = ex.get_encoded_length(encoded);
    var enc_addr = ex.get_encoded_address(encoded);

    var mem = ex.memory.buffer;
    var decoder = new TextDecoder('utf-8');
    var bytes = new Uint8Array(mem, enc_addr, enc_len);
    var encode_result = decoder.decode(bytes);

    ex.destroy_compact64_encode_input_builder(b);

    var b = ex.create_compact64_decode_input_builder();
    for(let c of encode_result){
        ex.dec_builder_append_char(b, c.charCodeAt(0));
    }

    let decoded = ex.decode_compact64_from_builder(b);

    var i = 0;
    for(;;) {
        console.log("loop " + i);
        ex.next(decoded);

        var len = ex.get_item_length(decoded);
        if(len < 0) { break; }
        var addr = ex.get_item_address(decoded);
        var bytes = new Uint8Array(mem, addr, len);
        var s = decoder.decode(bytes);

        //println!("{}", str);
        if(s != ss[i]){
            console.log(s + "  "+ ss[i]);
        }
        i += 1;
    }
    ex.destroy_compact64_decode_input_builder(b);
    ex.destroy_encoded_object(encoded);
    ex.destroy_decoded_object(decoded);
    console.log("done");
    });
}