function main() {
    fetch('compact64.wasm').then(response => response.arrayBuffer())
        .then(bytes => WebAssembly.instantiate(bytes, {}))
        .then(results => {
            var ss = ["ABCDE", "あああああ",
                "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ",
                "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん",
                "😃😁😂",
                "😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂"];
            var loop = 10000;
            var top_loop = 3;
            var super_top_loop = 10;
            const ex = results.instance.exports;
            var result_holder = 0;

            for (var super_top_counter = 0; super_top_counter < super_top_loop; super_top_counter++) {
                for (let s of ss) {
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("sb " + s);
                        for (var i = 0; i < loop; i++) {
                            var b = ex.create_string_builder();
                            for (let c of s) {
                                ex.string_builder_append_unchecked(b, c.codePointAt(0));
                            }
                            let r = ex.test_with_string_builder(b);
                            result_holder += r;
                            ex.destroy_string_builder(b);
                        }
                        console.timeEnd("sb " + s);
                    }
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("sb wc" + s);
                        for (var i = 0; i < loop; i++) {
                            var b = ex.create_string_builder_with_capacity(s.length * 3);
                            for (let c of s) {
                                ex.string_builder_append_unchecked(b, c.codePointAt(0));
                            }
                            let r = ex.test_with_string_builder(b);
                            result_holder += r;
                            ex.destroy_string_builder(b);
                        }
                        console.timeEnd("sb wc" + s);
                    }
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("sb nc" + s);
                        for (var i = 0; i < loop; i++) {
                            var b = ex.create_string_builder_with_capacity(s.length * 3);
                            for (let c of s) {
                                ex.string_builder_append(b, c.codePointAt(0));
                            }
                            let r = ex.test_with_string_builder(b);
                            result_holder += r;
                            ex.destroy_string_builder(b);
                        }
                        console.timeEnd("sb nc" + s);
                    }
                }
            }

            console.log("done " + result_holder);
        });
}