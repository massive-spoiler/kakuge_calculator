function main() {
    fetch('compact64.wasm').then(response => response.arrayBuffer())
        .then(bytes => WebAssembly.instantiate(bytes, {}))
        .then(results => {
            var ss = ["ABCDE", "あああああ",
                "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ",
                "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん",
                "😃😁😂",
                "😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂"];
            var loop = 10000;
            var top_loop = 3;
            var super_top_loop = 3;
            const ex = results.instance.exports;

            var result_holder = 0;
            for (var super_top_counter = 0; super_top_counter < super_top_loop; super_top_counter++) {

                for (let s of ss) {
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("utf8 uc" + s);

                        for (var loop_counter = 0; loop_counter < loop; loop_counter++) {

                            let utf8 = new TextEncoder().encode(s);
                            let len = utf8.byteLength;
                            var allocated = ex.alloc(len);
                            var mem = ex.memory.buffer;
                            var array = new Uint8Array(mem, allocated, len);

                            for (var i = 0; i < len; ++i) {
                                array[i] = utf8[i];
                            }
                            let r = ex.test_with_utf8_unchecked(allocated, len);
                            //console.log("result " + r);

                            result_holder += r;
                            ex.free(allocated);
                        }
                        console.timeEnd("utf8 uc" + s);
                    }
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("utf8 " + s);

                        for (var loop_counter = 0; loop_counter < loop; loop_counter++) {
                            let utf8 = new TextEncoder().encode(s);
                            let len = utf8.byteLength;
                            var allocated = ex.alloc(len);
                            var mem = ex.memory.buffer;
                            var array = new Uint8Array(mem, allocated, len);
                            array.set(utf8, 0);

                            let r = ex.test_with_utf8(allocated, len)
                            //console.log("result " + r);

                            result_holder += r;
                            ex.free(allocated);
                        }
                        console.timeEnd("utf8 " + s);
                    }
                }

            }


            console.log("done " + result_holder);
        });
}