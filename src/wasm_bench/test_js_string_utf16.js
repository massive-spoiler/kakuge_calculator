function main() {
    fetch('compact64.wasm').then(response => response.arrayBuffer())
        .then(bytes => WebAssembly.instantiate(bytes, {}))
        .then(results => {
            var ss = ["ABCDE", "あああああ",
                "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ",
                "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん",
                "😃😁😂",
                "😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂"];
            var loop = 10000;
            var top_loop = 3;
            var super_top_loop = 10;
            const ex = results.instance.exports;

            var result_holder = 0;
            for (var super_top_counter = 0; super_top_counter < super_top_loop; super_top_counter++) {
                for (let s of ss) {
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("utf16 " + s);

                        for (var loop_counter = 0; loop_counter < loop; loop_counter++) {
                            let len = s.length;
                            var allocated = ex.alloc(len * 2);
                            var mem = ex.memory.buffer;
                            let array = new Uint16Array(mem, allocated, len);

                            for (var i = 0; i < len; i++) {
                                array[i] = s.charCodeAt(i);
                            }
                            let r = ex.test_with_utf16_lossy(allocated, len)

                            result_holder += r;
                            ex.free(allocated);
                        }
                        console.timeEnd("utf16 " + s);
                    }
                }

            }


            console.log("done " + result_holder);
        });
}