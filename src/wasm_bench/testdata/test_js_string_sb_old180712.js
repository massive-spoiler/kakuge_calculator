function main() {
    fetch('compact64.wasm').then(response => response.arrayBuffer())
        .then(bytes => WebAssembly.instantiate(bytes, {}))
        .then(results => {
            var ss = ["ABCDE", "あああああ",
                "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ",
                "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん",
                "😃😁😂",
                "😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂"];
            var loop = 1000;
            var top_loop = 10;
            const ex = results.instance.exports;
            var result_holder = 0;

            for (var super_top_counter = 0; super_top_counter < 2; super_top_counter++) {

                for (let s of ss) {
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("sb " + s);
                        for (var i = 0; i < loop; i++) {
                            var b = ex.create_string_builder();
                            for (let c of s) {
                                ex.string_builder_append(b, c.codePointAt(0));
                            }
                            let r = ex.test_with_string_builder(b);
                            result_holder += r;
                            ex.destroy_string_builder(b);
                        }
                        console.timeEnd("sb " + s);
                    }
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("sb unchecked" + s);
                        for (var i = 0; i < loop; i++) {
                            var b = ex.create_string_builder();
                            for (let c of s) {
                                ex.string_builder_append_unchecked(b, c.codePointAt(0));
                            }
                            let r = ex.test_with_string_builder(b);
                            result_holder += r;
                            ex.destroy_string_builder(b);
                        }
                        console.timeEnd("sb unchecked" + s);
                    }
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("sb array" + s);
                        for (var i = 0; i < loop; i++) {
                            var b = ex.create_string_builder();
                            let array = Array.from(s);
                            for (let c of array) {
                                ex.string_builder_append(b, c);
                            }
                            let r = ex.test_with_string_builder(b);
                            result_holder += r;
                            ex.destroy_string_builder(b);
                        }
                        console.timeEnd("sb array" + s);
                    }
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("sb unchecked array" + s);
                        for (var i = 0; i < loop; i++) {
                            var b = ex.create_string_builder();
                            let array = Array.from(s);
                            for (let c of array) {
                                ex.string_builder_append_unchecked(b, c);
                            }
                            let r = ex.test_with_string_builder(b);
                            result_holder += r;
                            ex.destroy_string_builder(b);
                        }
                        console.timeEnd("sb unchecked array" + s);
                    }
                }
            }
            // console.time("malloc utf8" + s);
            // for (var i = 0; i < loop; i++) {
            //     var b = ex.create_string_builder();
            //     let array = Array.from(s);
            //     for (let c of array) {
            //         ex.string_builder_append_unchecked(b, c);
            //     }
            //     let r = ex.test_with_string_builder(b);
            //     result_holder += r;
            //     ex.destroy_string_builder(b);
            // }
            // console.timeEnd("sb unchecked array" + s);


            console.log("done " + result_holder);
        });
}