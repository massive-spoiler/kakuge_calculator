function main() {
    fetch('compact64.wasm').then(response => response.arrayBuffer())
        .then(bytes => WebAssembly.instantiate(bytes, {}))
        .then(results => {
            var ss = ["ABCDE", "あああああ",
                "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ",
                "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん",
                "😃😁😂",
                "😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂"];
            var loop = 1000;
            var top_loop = 3;
            const ex = results.instance.exports;
            var result_holder = 0;
            for (let s of ss) {
                for (var super_top_counter = 0; super_top_counter < 3; super_top_counter++) {
                    for (var top_counter = 0; top_counter < top_loop; top_counter++) {
                        console.time("utf8 " + s);

                        for (var loop_counter = 0; loop_counter < loop; loop_counter++) {
                            let utf8 = new TextEncoder().encode(s);
                            let len = utf8.byteLength;
                            var allocated = ex.alloc(utf8.byteLength);

                            for (var i = 0; i < len; ++i) {
                                allocated[i] = utf8[i];
                            }
                            let r = ex.test_with_utf8_unchecked(allocated, len)

                            result_holder += r;
                            ex.free(allocated);
                        }
                        console.timeEnd("utf8 " + s);
                    }
                }

            }
            // console.time("malloc utf8" + s);
            // for (var i = 0; i < loop; i++) {
            //     var b = ex.create_string_builder();
            //     let array = Array.from(s);
            //     for (let c of array) {
            //         ex.string_builder_append_unchecked(b, c);
            //     }
            //     let r = ex.test_with_string_builder(b);
            //     result_holder += r;
            //     ex.destroy_string_builder(b);
            // }
            // console.timeEnd("sb unchecked array" + s);


            console.log("done " + result_holder);
        });
}