use write_file::{_write_file, _read_file};
use calc_ground::search_for_good_moves::SFGM;
use ordered_float::OrderedFloat;

pub fn _search_for_good_moves_test_and_write_file(){
    let result = calc_ground::search_for_good_moves_test::search_for_good_moves_test();
    let text = serde_json::to_string(&result).unwrap();
    _write_file("good_moves.json", &text);
}

pub fn _analyze_good_moves(){
    let text = _read_file("good_moves.json");
    let mut sfgm : SFGM = serde_json::from_str(&text).unwrap();
    sfgm.offence.sort_by_key(|of| OrderedFloat::<f64>(-of.score()));
    for item in sfgm.offence{
        println!("{} {} score {}", item.name, item.count(), item.score());
    }
}

