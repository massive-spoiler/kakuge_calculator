function showMenu(x, y) {
	var myMenu = document.getElementById('js-menu');
	myMenu.style.left = x + 'px';
	myMenu.style.top = y + 'px';
	myMenu.classList.add('show');
}

function hideMenu() {
	var myMenu = document.getElementById('js-menu');
	if (myMenu.classList.contains('show')) {
		myMenu.classList.remove('show');
	}
}

function getTable() {
	return document.getElementById("main-table");
}

function getTableElement(col, row) {
	return getTable().rows[row].cells[col].children[0];
}

function adjustDefaultNames() {
	var table = getTable();
	var rows = table.rows.length;
	var cols = table.rows[0].cells.length;

	var colRegex = /col\d+/;
	for (i = 1; i < cols; i++) {
		var btn = getTableElement(i, 0);
		if (colRegex.test(btn.innerHTML)) {
			btn.innerHTML = "col" + i;
		}
	}

	var rowRegex = /row\d+/;
	for (i = 1; i < rows; i++) {
		var btn = getTableElement(0, i);
		if (rowRegex.test(btn.innerHTML)) {
			btn.innerHTML = "row" + i;
		}
	}
}

function colPlusBtnClicked() {
	let name = prompt();
	if (name) {
		var table = getTable();
		var cols = table.rows[0].cells.length;
		insertCol(cols - 1, name);
		initTableColors();
	}
}

function insertCol(ins_index, name) {
	var table = getTable();
	var rows = table.rows.length;
	var cols = table.rows[0].cells.length;
	var ins = ins_index;

	var titleCell = table.rows[0].insertCell(ins_index);
	titleCell.outerHTML = '<th><button type="button" onclick="headerClicked(this,event)">' + name + '</button></th>';
	for (var i = 1; i < rows - 1; i++) {
		var insertCell = table.rows[i].insertCell(ins);
		insertCell.innerHTML = '<input class="inp" type="text">';
	}
	var lastCell = table.rows[rows - 1].insertCell(ins);
	lastCell.innerHTML = "-";
	adjustDefaultNames();
}

function rowPlusBtnClicked() {
	let name = prompt();
	if (name) {
		var table = document.getElementById("main-table");
		var rows = table.rows.length;
		insertRow(rows - 1, name);
		initTableColors();
	}
}

function insertRow(ins_index, name) {
	var table = getTable();
	var rows = table.rows.length;
	var cols = table.rows[0].cells.length;
	var ins = ins_index;

	var insertRow = table.insertRow(ins);
	var titleCell = insertRow.insertCell(0);
	titleCell.outerHTML = '<th><button type="button" onclick="headerClicked(this,event)">' + name + '</button></th>';
	for (var i = 1; i < cols - 1; i++) {
		var insertCell = insertRow.insertCell(i);
		insertCell.innerHTML = '<input class="inp" type="text">';
	}
	var lastCell = insertRow.insertCell(cols - 1);
	lastCell.innerHTML = "-";
	adjustDefaultNames();
}

function getTablePos(btn) {
	var td = btn.parentElement;
	var tr = td.parentElement;
	var row = $(tr)[0].rowIndex;
	var col = $(td)[0].cellIndex;

	return { row, col };
}

function titleClicked(btn) {
	var name = window.prompt("title");
	if (name) {
		btn.innerHTML = name;
	}
}

function headerClicked(btn, event) {
	var pos = getTablePos(btn);
	g_clickedTablePos = pos; //グローバルに突っ込む(もっときれいな書き方があるとは思うが・・・)

	e = event;
	showMenu(e.clientX, e.clientY)
}

function renameClicked() {
	var name = window.prompt("rename");
	if (name) {
		var btn = getTableElement(g_clickedTablePos.col, g_clickedTablePos.row);
		btn.innerHTML = name;
	}
}

function insertClicked() {
	let name = prompt();
	if (name) {
		var col = g_clickedTablePos.col;
		var row = g_clickedTablePos.row;
		if (col == 0) {
			insertRow(row, name); initTableColors(); return;
		}
		if (row == 0) {
			insertCol(col, name); initTableColors(); return;
		}
	}

}

function deleteClicked() {
	var col = g_clickedTablePos.col;
	var row = g_clickedTablePos.row;
	deleteLine(col, row);
	initTableColors();
}
function deleteLine(col, row) {
	var table = getTable();
	var rowLen = table.rows.length;
	var colLen = table.rows[0].cells.length;

	if (col == 0) {
		if (4 < rowLen) {
			deleteRowForced(row);
		}
		return;
	}
	if (row == 0) {
		if (4 < colLen) {
			deleteColForced(col);
		}
		return;
	}
}

function deleteRowForced(row) {
	var table = getTable();
	table.deleteRow(row);
}

function deleteColForced(col) {
	var table = getTable();
	var rows = table.rows;
	for (var r of rows) {
		r.deleteCell(col);
	}
}

function getTableObject() {
	var table = getTable();
	var rowLen = table.rows.length;
	var colLen = table.rows[0].cells.length;
	var title = getTableElement(0, 0).lastChild.nodeValue;
	var colHeaders = [];
	for (var i = 1; i < colLen - 1; i++) {
		colHeaders.push(getTableElement(i, 0).lastChild.nodeValue);
	}
	var rowHeaders = [];
	for (var i = 1; i < rowLen - 1; i++) {
		rowHeaders.push(getTableElement(0, i).lastChild.nodeValue);
	}
	var values = [];
	for (var y = 1; y < rowLen - 1; y++) {
		inner = [];
		for (var x = 1; x < colLen - 1; x++) {
			var elem = getTableElement(x, y);
			inner.push(elem.value);
		}
		values.push(inner);
	}
	return { title, colHeaders, rowHeaders, values };
}

async function getC64() {
	var obj = getTableObject();
	var promise = objToCompact64(obj);
	return await promise;
}

function getBaseUrl() {
	var url = location.href;
	var hatena = url.indexOf("?");
	if (hatena != -1) {
		return url.substr(0, hatena);
	}
	else { return url; }
}

function writeC64andMessage(url, message, copyEnabled) {
	let urlArea = document.getElementById('url_area');
	let messageArea = document.getElementById('message_area');
	let copyBtn = document.getElementById('copy_btn');


	messageArea.innerHTML = message;
	urlArea.innerHTML = url;
	copyBtn.classList.remove("mienai");
	urlArea.classList.remove("mienai");
	if (copyEnabled === false) {
		copyBtn.classList.add("mienai");
		urlArea.classList.add("mienai");
	}
	else {
		urlArea.select();
	}
}

function getUrl(c64) {
	return getBaseUrl() + "?q=" + c64;
}

async function saveUrl(lang) {
	let c64 = await getC64();
	let url = getUrl(c64);

	if (lang == "ja") {
		writeC64andMessage(url, "このURLをテキストエディタ等に保存してください。", true);
	} else {
		writeC64andMessage(url, "Please note down this url.", true);
	}
}

async function shareUrl(lang) {
	var c64 = await getC64();
	let url = getUrl(c64);
	if (lang == "ja") {
		writeC64andMessage(url, "このURLをシェアしてください。", true);
	} else {
		writeC64andMessage(url, "Please share this url.", true);
	}
}

async function calcClicked() {
	let tableData = getTableObject();
	try {
		var answer = await calcEquilibrium(tableData);
	}
	catch{
		alert("Calculation failed");
		return;
	}
	writeC64andMessage("", answer, false);
	//var area = document.getElementById('url_area');
	//area.innerHTML = answer;
	let lines = answer.split("<BR>");
	let attacks = splitResult(lines[1]);
	let defs = splitResult(lines[2]);
	setProbabilityColorCol(defs);
	setProbabilityColorRow(attacks);
}

function setProbabilityColorCol(map) {
	var colLen = getTable().rows[0].cells.length;
	for (let x = 1; x < colLen - 1; ++x) {
		let elem = getTableElement(x, 0);
		if (elem.lastChild.nodeValue in map) {
			let col = Number(map[elem.lastChild.nodeValue]) / 100.0 * 255;
			col = parseInt(col / 2);
			let col16 = (255 - col).toString(16);
			let colStr = "#ff" + col16 + col16;
			elem.style.backgroundColor = colStr;;
		}
		else {
			elem.style.backgroundColor = "#aaa";
		}
	}
}

function setProbabilityColorRow(map) {
	var rowLen = getTable().rows.length;
	for (let y = 1; y < rowLen - 1; ++y) {
		let elem = getTableElement(0, y);
		if (elem.lastChild.nodeValue in map) {
			let col = Number(map[elem.lastChild.nodeValue]) / 100.0 * 255;
			col = parseInt(col / 2);
			let col16 = (255 - col).toString(16);
			let colStr = "#ff" + col16 + col16;
			elem.style.backgroundColor = colStr;;
		}
		else {
			elem.style.backgroundColor = "#aaa";
		}
	}
}


function splitResult(line) {
	let map = {};

	let data = line.split(": ")[1];

	let splitted = data.split(" | ");
	for (s of splitted) {
		let pair = s.split("<");
		if (pair.length == 2) {
			let name = pair[0];
			let val = pair[1].split("%")[0];
			map[name] = val;
		}
	}
	return map;
}

async function jumpClicked() {
	var url = document.getElementById('url_box').value;
	let result = await analyzeUrlAndInitTable(url);
	if (result == AnalyzeUrlResult.BrokenData) {
		alert("Unable to analyze url data.");
	}
	else if (result == AnalyzeUrlResult.BaseUrlIsNotCorrect) {
		alert("The url needs to start with " + getBaseUrl());
	}
	else if (result == AnalyzeUrlResult.NotAnalyzed) {
		alert("Unable to recognize data to analyze");
	}
}

var AnalyzeUrlResult = { Success: 0, BaseUrlIsNotCorrect: 1, BrokenData: 2, NotAnalyzed: 3 };

async function onLoad() {
	let result = await analyzeUrlAndInitTable(window.location.href);
	if (result == AnalyzeUrlResult.BrokenData) {
		alert("Unable to analyze url data.");
	}
}

async function analyzeUrlAndInitTable(url) {
	var baseUrl = getBaseUrl() + "?q=";
	if (url.startsWith(baseUrl)) {
		var query = url.substr(baseUrl.length);
		try {
			var obj = await compact64ToObj(query);
		} catch{
			return AnalyzeUrlResult.BrokenData;
		}

		initTableWithObj(obj);
		return AnalyzeUrlResult.Success;
	}
	else if (url.startsWith(getBaseUrl()) == false) {
		return AnalyzeUrlResult.BaseUrlIsNotCorrect;
	}
	else { return AnalyzeUrlResult.NotAnalyzed; }
}

function removeTableLines() {
	var table = getTable();
	var rowLen = table.rows.length;
	var colLen = table.rows[0].cells.length;

	var rowRepeat = rowLen - 2;
	for (var i = 0; i < rowRepeat; ++i) {
		deleteRowForced(1);
	}
	var colRepeat = colLen - 2;
	for (var i = 0; i < colRepeat; ++i) {
		deleteColForced(1);
	}
}

function initTableWithObj(obj) {
	removeTableLines();
	var table = getTable();
	var rowLen = table.rows.length;
	var colLen = table.rows[0].cells.length;

	var rowLen = obj.rowHeaders.length;
	var colLen = obj.colHeaders.length;

	var table = getTable();
	for (var i = 0; i < rowLen; ++i) {
		insertRow(1);
	}
	for (var i = 0; i < colLen; ++i) {
		insertCol(1);
	}
	getTableElement(0, 0).innerHTML = obj.title;
	for (var i = 0; i < colLen; ++i) {
		getTableElement(i + 1, 0).innerHTML = obj.colHeaders[i];
	}
	for (var i = 0; i < rowLen; ++i) {
		getTableElement(0, i + 1).innerHTML = obj.rowHeaders[i];
	}
	for (var y = 0; y < rowLen; ++y) {
		for (var x = 0; x < colLen; ++x) {
			getTableElement(x + 1, y + 1).value = obj.values[y][x];
		}
	}
	initTableColors();
}

function initTableColors() {
	var table = getTable();
	var rowLen = table.rows.length;
	var colLen = table.rows[0].cells.length;
	for (let y = 1; y < rowLen - 1; ++y) {
		for (let x = 1; x < colLen - 1; ++x) {
			let cl = getTableElement(x, y).classList;
			cl.remove("gray");

			if (x % 2 == 0 || y % 2 == 0) {
				cl.add("gray");
			}
		}
	}
}

function copyClicked() {
	let urlArea = document.getElementById('url_area');
	urlArea.select();
	document.execCommand('copy');
}