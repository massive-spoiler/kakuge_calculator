#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct TableData {
    pub title: String,
    pub colHeaders: Vec<String>,
    pub rowHeaders:Vec<String>,
    pub values: Vec<Vec<String>>,
}