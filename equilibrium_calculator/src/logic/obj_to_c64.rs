use ::serde_json as js;

use ::for_wasm::string_builder::*;
use ::for_wasm::result::*;

use super::table_data::TableData;

fn err(s : &str) -> *mut ResultForWasm{ create_err(s) }

#[no_mangle]
pub extern "C" fn obj_to_c64(obj : *const StringBuilder) -> *mut ResultForWasm{
    let json : Result<TableData,_> = js::from_str(unsafe{ &(*obj).s });
    match json {
        Err(_) => return err("obj_to_c64 needs {title, colHeaders, rowHeaders, values}"),
        Ok(mut o) => {
            let mut v: Vec<String> = vec![];
            v.push(o.title);
            v.push(o.colHeaders.len().to_string());
            v.append(&mut o.colHeaders);
            v.push(o.rowHeaders.len().to_string());
            v.append(&mut o.rowHeaders);

            for array in o.values {
                for item in array {
                    v.push(item);
                }
            }

            match ::compact64::compact64_encode::encode_utf8s_into_compact64(&v){
                Err(s) => return err(&s),
                Ok(c64) => return create_ok(&c64),
            }
        }
    }
}

#[no_mangle]
pub extern "C" fn c64_to_obj(c64 : *const StringBuilder) -> *mut ResultForWasm {
    match ::compact64::compact64_decode::decode_compact64(unsafe { (*c64).s.as_bytes() }) {
        Err(s) => return err(&s),
        Ok(vec) => {
            fn analyze(vec: Vec<String>) -> Result<TableData, String> {
                let mut iter = vec.iter();
                let mut read = || {
                    match iter.next() {
                        Some(s) => Ok(s.to_string()),
                        None => Err(format!("c64 to obj format error")),
                    }
                };
                let title = read()?;
                let col_count : usize = read()?.parse().or(Err("col count".to_string()))?;
                let mut cols : Vec<String> = vec![];
                for _ in 0..col_count{
                    cols.push(read()?);
                }
                let row_count : usize = read()?.parse().or(Err("row count".to_string()))?;
                let mut rows : Vec<String> = vec![];
                for _ in 0..row_count{
                    rows.push(read()?);
                }
                let mut values : Vec<Vec<String>> = vec![];
                for _ in 0..row_count{
                    let mut array : Vec<String> = vec![];
                    for _ in 0..col_count{
                        array.push(read()?);
                    }
                    values.push(array);
                }
                return Ok(TableData{ title, colHeaders : cols, rowHeaders : rows, values });
            }
            return match analyze(vec){
                Err(s) => err(&s),
                Ok(table_data) => match ::serde_json::to_string(&table_data){
                    Err(_) => err("converting error"),
                    Ok(s) => create_ok(&s),
                },
            }
        }
    }
}

