use super::table_data::TableData;
use ::serde_json as js;
use ::for_wasm::string_builder::StringBuilder;
use ::for_wasm::result::*;

#[no_mangle]
pub extern "C" fn calc_from_obj(sb : *const StringBuilder) -> *mut ResultForWasm{
    let json : Result<TableData,_> = js::from_str(unsafe{ &(*sb).s });
    let t = match json{
        Err(_) => return create_err("calc_from_c64: json is not table data"),
        Ok(t) => t,
    };

    let mut input : Vec<Vec<String>> = vec![];
    input.push({
        let mut vec : Vec<String> = vec![];
        vec.push(t.title);
        for col in t.colHeaders{
            vec.push(col);
        }
        vec
    });
    let row_count = t.rowHeaders.len();
    for i in 0..row_count{
        let mut vec : Vec<String> = vec![];
        vec.push(t.rowHeaders[i].to_string());
        for v in &t.values[i]{
            vec.push(v.to_string());
        }
        input.push(vec);
    }
    match kakuge::calc(&input){
        Ok(result) => return create_ok(&result.print("").join("<BR>")),
        Err(_) => return create_err("Sorry, I don't know why but this calculation was failed."),
    }
}
