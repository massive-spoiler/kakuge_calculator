///exportをとって結果を返すcallbackを入れる。
function fetchWasm(wasmFile, callback){
	return fetch(wasmFile).then(response => response.arrayBuffer())
    .then(bytes => WebAssembly.instantiate(bytes, {}))
    .then(results => {
      return callback(results.instance.exports);
    });
}

///exportと文字列からwasmの入力に使うStringBuilderを作る。それを使うコールバックを実行し、抜ける時StringBuilderをdestroyする。
function createWasmInput(exp, text, callback){
  let sb = exp.create_string_builder();
  try{
    for (let c of text) {
      exp.string_builder_append(sb, c.codePointAt(0));
    }
    return callback(sb);
  } finally{
  	exp.destroy_string_builder(sb);
  }
}

///exportとResultForWasmから文字列を取り出す。resultのdestroyもする。
function readWasmResultAndDestroy(exp, result){
  try{
    var len = exp.result_get_length(result);
    var addr = exp.result_get_address(result);
    var decoder = new TextDecoder('utf-8');
    var mem = exp.memory.buffer;
    var bytes = new Uint8Array(mem, addr, len);
    return decoder.decode(bytes);
  } finally {
  	exp.destroy_result(result);
  }
}