extern crate compact64;
extern crate kakuge;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

pub mod for_wasm;
pub mod for_c64;
pub mod logic;

use for_wasm::string_builder::*;

pub fn test(){
    let json = r#"{
        "title":"hoge",
        "colHeaders":["col1","col2"],
        "rowHeaders":["row1","row2"],
        "values":[
            ["1","4"],
            ["3","2"]
        ]
    }"#;
    let res = logic::obj_to_c64::obj_to_c64(&StringBuilder{s : json.to_string()});
    unsafe{ println!("{}", (*res).result); }
}

//pub fn test() {
// http://localhost:63342/kakuge_calculator/equilibrium_calculator/src/index.html?q=UtitleDTcol1Tcol2DTrow1Trow2CIEIDD
// http://localhost:63342/kakuge_calculator/equilibrium_calculator/src/index.html?q=UtitleIGp44Ks44O844OJs5oqV44GS5oqc44GRq6YGF44KJ44GbRws44OQ44Kv44K544OGmNUblsI9Qs54Sh5pW15piH56ucIGs5omT5pKD6YeN44Gts5oqV44GS6YeN44GtqLTEwRuWeguebtArQ0Pph43jga1GMTAv5b6M44KN5LiL44GM44KKqQ0Pph43jga1GNgBJDIBJDIBJiWJB4BBBJB4JiWJh4Jh4JEsJh4Jh4JhQBJh4JEYJC0JigJiWBJDIJDIBBJEYBJh4BJC0JEYJiW
// 英語版 http://eqcalc.jounin.jp/eq_calc/1_0/index.html?q=UtitleIJUGuardTTechWDe-TechVB-DashV5F_JabWDragonPV1F_JabVBack_JTFW_JIIUMeatyUThrowa-10F_N_JumpVCC_F10VB_WalkUCC_F6Y5F_LariatYCommand_GBJDIBJDIBJiWJDIJDIJDIJB4BBBJB4JiWJB4BJB4Jh4Jh4JEsJh4Jh4JhQJh4BBBJh4JEYJC0JigJiWJigBJB4BJDIJDIBBJEYBBJB4BJh4BJC0JEYJiWJigBJBQBJCWJCWJCWJigJEYBBBJDIJDIJDIJjIJDIJiWJDIBJjI
// ultra ver. http://eqcalc.jounin.jp/eq_calc/1_0/index.html?q=UtitleINUBlockTTechWDe-TechVB-DashV5F_JabWDragonPV1F_JabVBack_JTFW_JWFuzzy_JVFJxxCGVFJxxSBWBD_TechIIUMeatyUThrowa-10F_N_JumpVCC_F10VB_WalkUCC_F6Y5F_LariatYCommand_GBJDIBJDIBJiWJDIJDIJDIBBBBJB4BBBJB4JiWJB4BJB4BJjIJjIBJh4Jh4JEsJh4Jh4JhQJh4BBBJEsJEsBBJh4JEYJC0JigJiWJigBJB4BJEYJEYJC0BJDIJDIBBJEYBBJB4BJD6BBBJh4BJC0JEYJiWJigJBQJBQJDIJEYJEYJDIBJCWJCWJCWJigJEYBBBBJCWJjIJCWJDIJDIJDIJjIJDIJiWJDIBJjIBJjIJjIJjI
//    let c64 = "UtitleDTcol1Tcol2DTrow1Trow2CIEIDD";
//    let result = for_c64::c64_decode(&for_wasm::string_builder::StringBuilder{ s : c64.to_string() });
//    let (is_ok, result_text) = unsafe{ ( (*result).is_ok, (*result).result.to_string() ) };
//    println!("{} {}", is_ok, result_text);
//}