///UTF8の文字列で結果を返す。必要ならばJsonで複雑な型の値を返してもよろしかろうと思う。is_ok==falseの場合エラーメッセージが入る
pub struct ResultForWasm{
    pub result: String,
    pub is_ok : bool
}

use super::string_builder::StringBuilder;

///C側から呼び出す必要は基本的にない。
#[no_mangle]
pub extern "C" fn create_result(is_ok : bool, sb : *const StringBuilder) -> *mut ResultForWasm {
    let s = unsafe{ &(*sb).s };
    let b = Box::new(ResultForWasm { is_ok, result : s.to_string() });
    return Box::into_raw(b);
}

//pub fn create_result_rust(is_ok : bool, s : &str) -> *mut ResultForWasm {
//    return create_result(is_ok, &StringBuilder{ s : s.to_string()});
//}

pub fn create_err(s : &str) -> *mut ResultForWasm {
    return create_result(false, &StringBuilder{ s : s.to_string()});
}

pub fn create_ok(s : &str) -> *mut ResultForWasm {
    return create_result(true, &StringBuilder{ s : s.to_string()});
}

#[no_mangle]
pub extern "C" fn destroy_result(p: *mut ResultForWasm) {
    unsafe { Box::from_raw(p) };
}

#[no_mangle]
pub extern "C" fn result_get_length(p: *const ResultForWasm) -> i32 {
    unsafe {
        return (*p).result.len() as i32;
    }
}

///wasmからはこれとlengthを使ってバイト列を取り出す。
#[no_mangle]
pub extern "C" fn result_get_address(p: *const ResultForWasm) -> *const u8 {
    unsafe {
        return (*p).result.as_ptr();
    }
}

///okならば1が返り、その場合resultには結果が、okでなければ0が返りその場合resultにはエラーメッセージが入っている
#[no_mangle]
pub extern "C" fn result_is_ok(p: *const ResultForWasm) -> i32 {
    unsafe {
        return if (*p).is_ok{ 1 } else { 0 };
    }
}