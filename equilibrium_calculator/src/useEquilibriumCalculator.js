

/**
* 全データをURLに利用可能な文字列にエンコードする。データの形式は
* {title, colHeaders:Array<String>, rowHeaders:Array<String>, values:Array<Array<String>>}
* Promiseを返す。
*/
function objToCompact64(obj) {
	let json = JSON.stringify(obj);
  return fetchWasm('equilibrium_calculator.wasm',
  	ex =>{
			return createWasmInput(ex, json, inp =>{
      	let r = ex.obj_to_c64(inp);
      	return readWasmResultAndDestroy(ex, r);
      });
    });
}

function compact64ToObj(c64) {
  return fetchWasm('equilibrium_calculator.wasm',
    ex => {
    	return createWasmInput(ex, c64, inp =>{
      	let r = ex.c64_to_obj(inp);
      	let result = readWasmResultAndDestroy(ex, r);
      	return JSON.parse(result);
      });
    });
}

function calcEquilibrium(obj){
	let json = JSON.stringify(obj);
	return fetchWasm('equilibrium_calculator.wasm',
      ex => {
      	return createWasmInput(ex, json, inp =>{
        	let r = ex.calc_from_obj(inp);
        	let result = readWasmResultAndDestroy(ex, r);
        	return result;
        });
      });
}


