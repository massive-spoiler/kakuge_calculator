function getLanguage(){
    var navigator_obj = window.navigator;
    if(navigator_obj.language !== undefined)    return navigator_obj.language;
    if(navigator_obj.browserLanguage !== undefined) return navigator_obj.browserLanguage;
    if(navigator_obj.userLanguage !== undefined)    return navigator_obj.userLanguage;
    return "en";
}

function write(ja,en){
	if(g_lang == null){ //windowを外すと動かない・・・なぜだろうなあ
		g_lang = getLanguage();
	}
	if(g_lang == "ja"){
  		document.write(ja);
  } else {
  		document.write(en);
  }
}