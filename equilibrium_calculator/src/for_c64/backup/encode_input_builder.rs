//use super::compact64_encode::{Input, Encoded};
//
//pub struct EncodeInputBuilder{
//    vec : Vec<String>
//}
//
//#[no_mangle]
//pub extern "C" fn create_compact64_encode_input_builder() -> *mut EncodeInputBuilder {
//    let b = Box::new(EncodeInputBuilder{ vec : vec![]});
//    Box::into_raw(b)
//}
//
//#[no_mangle]
//pub extern "C" fn destroy_compact64_encode_input_builder(b : *mut EncodeInputBuilder){
//    unsafe{ Box::from_raw(b); }
//}
//
/////wasmからまともに呼び出すにはここまでやらないといけないが、これならどの環境からでも明確に使えるなあ
///// charはunicodeのcodepointで32bitのunsigned整数。互換性のためにi32にしている。
///// cがcodepointでないと失敗して-1、最初にnext_stringしていない場合も失敗して-2, そうでないと成功して1が返る
//#[no_mangle]
//pub extern "C" fn enc_builder_append_char(b : *mut EncodeInputBuilder, c : i32) -> i32{
//    unsafe {
//        match (*b).vec.last_mut() {
//            Some(s) => match ::std::char::from_u32(c as u32) {
//                Some(c) => {
//                    s.push(c);
//                    return 0;
//                }
//                _ => return -1,
//            }
//            _ => return -2,
//        }
//    }
//}
//
/////次のstringを追加。文字列をいくつもInputしたいので、next_stringしてから複数回append_char、
///// またnext_stringしてappend_char・・・というのを何回も繰り返す。
//#[no_mangle]
//pub extern "C" fn next_string(b : *mut EncodeInputBuilder){
//    unsafe {
//        (*b).vec.push(String::new());
//    }
//}
//
//#[no_mangle]
//pub extern "C" fn encode_builder_into_compact64(b : *mut EncodeInputBuilder) -> *mut Encoded{
//    use super::compact64_encode::encode_byte_arrays_into_compact64;
//    unsafe {
//        let vec = &(*b).vec;
//        let mut ins : Vec<Input> = vec![];
//        for s in vec{
//            ins.push(Input{ bytes : s.as_ptr(), len : s.len() as i64 });
//        }
//        encode_byte_arrays_into_compact64(ins.as_ptr(), ins.len() as i32)
//    }
//}
//
/////メモリをやたらと使ってみる。
//#[no_mangle]
//pub extern "C" fn wasm_memory_test() {
//    let mut sum: u64 = 0;
//    let limit = 1024 * 1000; //1024KBを確保してみる。
//    loop {
//        let _vec = get_vec(1024);
//        sum += 4 * 1024;
//
//        if limit < sum { break; }
//    }
//}
//
//fn get_vec(size : usize) -> *const Vec<u32>{
//    let mut b: Box<Vec<u32>> = Box::new(vec![]);
//    for i in 0..size {
//        (*b).push(i as u32);
//    }
//    Box::into_raw(b)
//}