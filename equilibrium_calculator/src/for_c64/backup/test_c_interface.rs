//#[allow(dead_code)]
//pub fn test() {
//    use super::compact64_decode as c64;
//    use super::compact64_encode::*;
//
//    fn inp(s : &str) -> Input{
//        Input { bytes: s.as_ptr(), len: s.len() as i64 }
//    }
//    //let ss = ["123456789012345678901234567890"];
//    let dec_max = i64::max_value() - 1;
//    let dec_max_p = dec_max as i128 + 1;
//    let enc_max = 2i64.pow(42) -1;
//    let enc_max_p = enc_max as i128 + 1;
//    let enc_min = -enc_max;
//    let enc_min_p = enc_min - 1;
//
//    let ss = [
//        "", "1",
//        "5", "314.8",
//        "1234567.89123","direct_str",
//        "にほんご","ながああああああああああああああああああああああああああいUTF8888888888888あああ", //7
//        &dec_max.to_string(), &dec_max_p.to_string(),
//        &enc_max.to_string(), &enc_max_p.to_string(),
//        &enc_min.to_string(), &enc_min_p.to_string(), //13
//        "0","0.0",
//        "00.0",".0","0.", "0.100", "0.0000000000100","0.0000000000001",
//        "0.000","12345678901234567890_30_moji_da",
//        "1234567890123 15"
//    ];
//
//    //let c1 = c64::Input { bytes: s1.as_ptr(), len: s1.len() as i32 };
//    let in_vec : Vec<Input> = ss.iter().map(|s| inp(s)).collect();
//
//    let encoded = encode_byte_arrays_into_compact64(in_vec.as_ptr(), in_vec.len() as i32);
//    let enc_len = get_encoded_length(encoded);
//    let mut copy_to: Vec<u8> = vec![0; enc_len as usize];
//    copy_encoded_bytes(encoded, copy_to.as_mut_ptr());
//
//
////    let decoded = c64::decode_compact64(str.as_ptr(), str.len() as i32);
//    let decoded = c64::decode_compact64(copy_to.as_ptr(), copy_to.len() as i32);
//    println!("{}" , String::from_utf8(copy_to).unwrap());
//
//    let mut i = 0;
//    loop{
//        println!("loop {}", i);
//        c64::next(decoded);
//
//        let len = c64::get_item_length(decoded);
//        if len < 0 { break; }
//        let mut copy_to : Vec<u8> = vec![0; len as usize];
//        c64::copy_item_bytes(decoded, copy_to.as_mut_ptr());
//        let str = String::from_utf8(copy_to).unwrap();
//        //println!("{}", str);
//        assert_eq!(str, ss[i]);
//        i += 1;
//    }
//    println!("done");
//}