//extern crate kakuge_calculator_lib;
//extern crate compact64;
//
//use kakuge_calculator_lib::kakuge::calc::calc;
//
//pub struct StringBuilder{
//    s : String
//}
//
//#[no_mangle]
//pub extern "C" fn create_string_builder() -> *mut StringBuilder{
//    let b = Box::new(StringBuilder{ s : "".to_string() });
//    Box::into_raw(b)
//}
//
//#[no_mangle]
//pub extern "C" fn destroy_string_builder(b : *mut StringBuilder){
//    unsafe{ Box::from_raw(b); }
//}
//
//#[no_mangle]
//pub extern "C" fn string_builder_append(sb : *mut StringBuilder, code_point : i32) -> i32{
//    match ::std::char::from_u32(code_point as u32) {
//        Some(c) => {
//            unsafe { (*sb).s.push(c) };
//            return 1;
//        }
//        _ => return 0,
//    }
//}
//
////#[cfg(all(target_arch = "wasm32", target_os = "unknown"))]
//struct Decoded;
//
//#[link(name = "compact64")]
//extern {
//    fn decode_compact64(addr : *const u8, len : i32) -> *mut Decoded;
//    fn get_item_length(p: *const Decoded) -> i32;
//    fn copy_item_bytes(p: *const Decoded, copy_to: *mut u8) -> i32;
//    fn next(p: *mut Decoded);
//    fn is_valid(p: *const Decoded) -> i32;
//}
//
//pub fn decode(s : &str) -> Option<Vec<String>>{
//    unsafe {
//        let decoded = decode_compact64(s.as_ptr(), s.len() as i32);
//        if is_valid(decoded) == 0{
//            return None;
//        }
//        let mut vec : Vec<String> = vec![];
//        loop{
//            next(decoded);
//            let length = get_item_length(decoded);
//            if length < 0{ break; }
//            let mut array : Vec<u8> = vec![0; length as usize];
//            copy_item_bytes(decoded, array.as_mut_ptr());
//            match String::from_utf8(array){
//                Ok(text) => vec.push(text),
//                Err(_) => return None,
//            }
//        }
//        return Some(vec);
//    }
//}
//
//pub fn calc_from_c64_rust(sb : &str) -> Result<String,String>{
//    let ss = match decode(sb) {
//        Some(ss) => ss,
//        None => return Err("Decode failed".to_string()),
//    };
//    let mut it = ss.into_iter();
//    let mut read = ||{ it.next().unwrap() };
//    //fn read(it : &mut impl Iterator<Item=String>) -> String{ it.next().unwrap() }
//    let title =  read();
//    let colCount : usize = match read().parse(){
//        Ok(v) => v,
//        _ => return Err("Decode failed(colCount)".to_string()),
//    };
//    let mut colHeaders : Vec<String> = vec![];
//    for _ in 0..colCount {
//        colHeaders.push(read());
//    }
//    let rowCount : usize = match read().parse(){
//        Ok(v) => v,
//        _ => return Err("Decode failed(rowCount)".to_string()),
//    };
//    let mut rowHeaders : Vec<String> = vec![];
//    for _ in 0..rowCount{
//        rowHeaders.push(read());
//    }
//    let mut values : Vec<Vec<f64>> = vec![];
//    for y in 0..rowCount{
//        let mut array : Vec<f64> = vec![];
//        for x in 0..colCount{
//            let s = read();
//            match s.parse::<f64>(){
//                Ok(v) => array.push(v),
//                _ => return Err(format!["{} {} '{}' couldn't be parsed",rowHeaders[y], colHeaders[x], s]),
//            }
//        }
//        values.push(array);
//    }
//
//    let mut input : Vec<Vec<String>> = vec![];
//    input.push({
//        let mut vec : Vec<String> = vec![];
//        vec.push(title);
//        for col in colHeaders{
//            vec.push(col);
//        }
//        vec
//    });
//    for i in 0..rowCount{
//        let mut vec : Vec<String> = vec![];
//        vec.push(rowHeaders[i].to_string());
//        for v in &values[i]{
//            vec.push(v.to_string());
//        }
//        input.push(vec);
//    }
//    match kakuge_calculator_lib::kakuge::calc(&input){
//        Ok(result) => return Ok(result.print("").join("\n")),
//        Err(e) => return Err("Sorry, I don't know why but this calculation was failed.".to_string()),
//    }
//}
//
//#[no_mangle]
//pub extern "C" fn calc_from_c64(sb : *const StringBuilder) -> *const ResultForWasm {
//    unsafe {
//        match calc_from_c64_rust(&(*sb).s) {
//            Ok(result) => return create_result(true, result),
//            Err(e) => return create_result(false, e),
//        }
//    }
//}
//
/////UTF8の文字列で結果を返す。必要ならばJsonで複雑な型の値を返してもよろしかろうと思う。is_ok==falseの場合エラーメッセージが入る
//pub struct ResultForWasm{
//    result: String,
//    is_ok : bool
//}
//
//fn create_result(is_ok : bool, result : String) -> *mut ResultForWasm {
//    let b = Box::new(ResultForWasm { is_ok, result });
//    return Box::into_raw(b);
//}
//
//#[no_mangle]
//pub extern "C" fn destroy_result(p: *mut ResultForWasm) {
//    unsafe { Box::from_raw(p) };
//}
//
//#[no_mangle]
//pub extern "C" fn get_result_length(p: *const ResultForWasm) -> i32 {
//    unsafe {
//        return (*p).result.len() as i32;
//    }
//}
//
/////wasmからはこれとlengthを使ってバイト列を取り出す。
//#[no_mangle]
//pub extern "C" fn get_result_address(p: *const ResultForWasm) -> *const u8 {
//    unsafe {
//        return (*p).result.as_ptr();
//    }
//}
//
/////okならば1が返り、その場合resultには結果が、okでなければ0が返りその場合resultにはエラーメッセージが入る
//#[no_mangle]
//pub extern "C" fn get_is_ok(p: *const ResultForWasm) -> i32 {
//    unsafe {
//        return if (*p).is_ok{ 1 } else { 0 };
//    }
//}
//
//pub fn test(){
//    let c64 = "UtitleDTcol1Tcol2DTrow1Trow2CIEIDD";
//    match calc_from_c64_rust(c64){
//        Ok(s) => println!("{}",s),
//        Err(s) => println!("{}",s),
//    }
//}