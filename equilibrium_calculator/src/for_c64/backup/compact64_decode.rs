////use super::my_base64_item::MyBase64Item;
//use super::get_base64_from_b6s::get_b6s_from_base64;
//use super::get_items::get_items;
//
//#[derive(Debug)]
//pub struct Decoded {
//    pub vec: Option<Vec<String>>,
//    pub index : i32,
//    pub error_message : Option<String>,
//}
//
/////decodeしたオブジェクトを返す。使い終わったらちゃんとdestroyする必要がある。
//#[no_mangle]
//pub extern "C" fn decode_compact64(base64 : *const u8, len : i32) -> *mut Decoded  {
//    fn error(message : String) -> *mut Decoded{
//        let b = Box::new(Decoded { vec: None, index:0, error_message : Some(message) });
//        return Box::into_raw(b);
//    }
//    let slice :&[u8] = unsafe { ::std::slice::from_raw_parts(base64, len as usize) };
//    let b6s = get_b6s_from_base64(slice);
//    match b6s{
//        Some(vec) =>{
//            let items = get_items(&vec);
//            match items {
//                Ok(items) => {
//                    let b = Box::new(Decoded { vec: Some(items), index : -1, error_message : None });
//                    return Box::into_raw(b);
//                }
//                Err(s) =>{ return error(format!("decode failed :{}", s)); }
//            }
//        }
//        _=>{return error("unknown error".to_string());}
//    }
//}
//
//#[no_mangle]
//pub extern "C" fn destroy_decoded_object(p: *mut Decoded) {
//    unsafe { Box::from_raw(p) };
//}
//
/////validなら 1 解析失敗して値が取れなかった場合0を返す
//#[no_mangle]
//pub extern "C" fn is_valid(p: *const Decoded) -> i32 {
//    unsafe{
//        match (*p).vec{
//            Some(_) => 1,
//            None => 0
//        }
//    }
//}
//
/////次のアイテムにポインタを移動する
//#[no_mangle]
//pub extern "C" fn next(p: *mut Decoded){
//    unsafe {
//        (*p).index += 1;
//    }
//}
//
//pub fn get_current_item(s : &Decoded) -> Option<&String>{
//    if let Some(ref vec) = s.vec{
//        if s.index < vec.len() as i32{
//            return Some(&vec[s.index as usize]);
//        }
//    }
//    return None;
//}
//
/////バイト列を受け取るのに必要なバイト数を取得。受け取れない場合-1
//#[no_mangle]
//pub extern "C" fn get_item_length(p: *const Decoded) -> i32{
//    unsafe {
//        match get_current_item(&*p) {
//            Some(ref v) => v.len() as i32,
//            _ => -1,
//        }
//    }
//}
//
/////アドレスを受け取る。手っ取り早いアクセスに使えるし、wasmではこれがないと無理
//#[no_mangle]
//pub extern "C" fn get_item_address(p: *const Decoded) -> *const u8{
//    unsafe {
//        match get_current_item(&*p) {
//            Some(ref v) => v.as_ptr(),
//            _ => u64::max_value() as *const u8, //wasmでは0はnullポインタではない・・・
//        }
//    }
//}
//
///// バイト列をコピーする。必要な量全てを一度に書ききるので、事前にget_item_length大きさを調べて、
///// 入りきる大きさのメモリ領域を準備する必要がある。成功すると1 失敗すると0
//#[no_mangle]
//pub extern "C" fn copy_item_bytes(p: *const Decoded, copy_to: *mut u8) -> i32{
//    unsafe {
//        match get_current_item(&*p) {
//            Some(ref v) =>{
//                v.as_ptr().copy_to(copy_to, v.len());
//                return 1;
//            }
//            _ => return 0,
//        }
//    }
//}
//
/////エラーメッセージがあるならその長さを取得。ない場合-1
//#[no_mangle]
//pub extern "C" fn get_decode_error_message_length(p: *const Decoded) -> i32 {
//    unsafe {
//        return match (*p).error_message {
//            Some(ref s) => s.len() as i32,
//            None => -1,
//        }
//    }
//}
//
/////長さを計ってからメモリ領域を用意しそれを渡してエラーメッセージを取得する・・・面倒臭さがクレイジー。成功すると1を返す
//#[no_mangle]
//pub extern "C" fn copy_decode_error_messag(p: *const Decoded, copy_to: *mut u8) -> i32 {
//    unsafe {
//        match (*p).error_message{
//            Some(ref v) => {
//                v.as_ptr().copy_to(copy_to, v.len());
//                return 1;
//            }
//            _ => return 0,
//        }
//    }
//}
