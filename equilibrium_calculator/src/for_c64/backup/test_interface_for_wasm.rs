//#[allow(dead_code)]
//pub fn test() {
//    use super::compact64_decode as c64;
//    use super::compact64_encode as enc;
//    use super::encode_input_builder as builder;
//    use super::decode_input_builder as dec_b;
//
//    fn inp(s: &str) -> enc::Input {
//        enc::Input { bytes: s.as_ptr(), len: s.len() as i64 }
//    }
//    //let ss = ["123456789012345678901234567890"];
//    let dec_max = i64::max_value() - 1;
//    let dec_max_p = dec_max as i128 + 1;
//    let enc_max = 2i64.pow(42) - 1;
//    let enc_max_p = enc_max as i128 + 1;
//    let enc_min = -enc_max;
//    let enc_min_p = enc_min - 1;
//
//    let ss = [
//        "", "1",
//        "5", "314.8",
//        "1234567.89123", "direct_str",
//        "にほんご", "ながああああああああああああああああああああああああああいUTF8888888888888あああ", //7
//        &dec_max.to_string(), &dec_max_p.to_string(),
//        &enc_max.to_string(), &enc_max_p.to_string(),
//        &enc_min.to_string(), &enc_min_p.to_string(), //13
//        "0", "0.0",
//        "00.0", ".0", "0.", "0.100", "0.0000000000100", "0.0000000000001",
//        "0.000", "12345678901234567890_30_moji_da",
//        "1234567890123 15"
//    ];
//
//    let b = builder::create_compact64_encode_input_builder();
//
//    for &s in ss.iter(){
//        builder::next_string(b);
//        for c in s.chars(){
//            builder::enc_builder_append_char(b, c as i32);
//        }
//    }
//
//    let encoded = builder::encode_builder_into_compact64(b);
//    let enc_len = enc::get_encoded_length(encoded);
//    let enc_addr = enc::get_encoded_address(encoded);
//    let s = unsafe{ String::from_utf8_unchecked(::std::slice::from_raw_parts(enc_addr, enc_len as usize).to_vec() ) };
//    println!("encoded string {}",s);
//    builder::destroy_compact64_encode_input_builder(b);
//
//    let b = dec_b::create_compact64_decode_input_builder();
//    for c in s.chars(){
//        dec_b::dec_builder_append_char(b, c as i32);
//    }
//
//    let decoded = dec_b::decode_compact64_from_builder(b);
//
//    let mut i = 0;
//    loop {
//        println!("loop {}", i);
//        c64::next(decoded);
//
//        let len = c64::get_item_length(decoded);
//        if len < 0 { break; }
//        let addr = c64::get_item_address(decoded);
//        let s = unsafe{ String::from_utf8_unchecked(::std::slice::from_raw_parts(addr, len as usize).to_vec() ) };
//        //println!("{}", str);
//        assert_eq!(s, ss[i]);
//        i += 1;
//    }
//    dec_b::destroy_compact64_decode_input_builder(b);
//    enc::destroy_encoded_object(encoded);
//    c64::destroy_decoded_object(decoded);
//    println!("done");
//}