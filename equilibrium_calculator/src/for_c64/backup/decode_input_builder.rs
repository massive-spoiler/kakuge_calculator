//use super::compact64_decode::Decoded;
//
//pub struct DecodeInputBuilder{
//    s : String
//}
//
//#[no_mangle]
//pub extern "C" fn create_compact64_decode_input_builder() -> *mut DecodeInputBuilder {
//    let b = Box::new(DecodeInputBuilder{ s : "".to_string()});
//    Box::into_raw(b)
//}
//
//#[no_mangle]
//pub extern "C" fn destroy_compact64_decode_input_builder(b : *mut DecodeInputBuilder){
//    unsafe{ Box::from_raw(b); }
//}
//
///// charはunicodeのcodepointで32bitのunsigned整数。互換性のためにi32にしている。
///// cがcodepointでないと失敗して-1、そうでないと成功して1が返る
//#[no_mangle]
//pub extern "C" fn dec_builder_append_char(b : *mut DecodeInputBuilder, c : i32) -> i32{
//    unsafe {
//        match ::std::char::from_u32(c as u32) {
//            Some(c) => {
//                (*b).s.push(c);
//                return 0;
//            }
//            _ => return -1,
//        }
//    }
//}
//
//
//#[no_mangle]
//pub extern "C" fn decode_compact64_from_builder(b : *mut DecodeInputBuilder) -> *mut Decoded{
//    use super::compact64_decode::decode_compact64;
//    unsafe {
//        let s = &(*b).s;
//
//        decode_compact64(s.as_ptr(), s.len() as i32)
//    }
//}
