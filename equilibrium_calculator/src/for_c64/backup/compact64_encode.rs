//
//#[repr(C)]
//#[derive(Debug)]
//pub struct Input {
//    pub bytes: *const u8,
//    pub len: i64,
//}
//
//pub struct Encoded {
//    vec : Option<Vec<u8>>,
//    error_message : Option<String>,
//}
//
/////複数のbyte列をまとめてURLで表現可能な6bit文字列に変換する。byte列をUTF8と解釈したときに10進数の文字列だった場合と、
///// a-zA-Z0-9-_だけで構成されていた場合、特別扱いして可変長数値または6bit文字列として保持することで容量を節約する
//#[no_mangle]
//pub extern "C" fn encode_byte_arrays_into_compact64(array : *const Input, len : i32) -> *mut Encoded {
//    use super::get_single_item::encode_single_item;
//    use super::get_base64_from_b6s::get_base64_from_b6s;
//
//    fn error(error_message: String) -> *mut Encoded {
//        let b = Box::new(Encoded { vec: None, error_message: Some(error_message) });
//        return Box::into_raw(b);
//    }
//
//    let slice: &[Input] = unsafe { ::std::slice::from_raw_parts(array, len as usize) };
//    let mut r: Vec<u8> = vec![];
//    for inp in slice {
//        let inp_bytes = unsafe { ::std::slice::from_raw_parts(inp.bytes, inp.len as usize) };
//
//        match encode_single_item(inp_bytes) {
//            Ok(mut v) =>{
//                r.append(&mut v)
//
//            },
//            Err(_) => {},
//        }
//    }
//    match get_base64_from_b6s(&r) {
//        Some(b64) => {
//            let b = Box::new(Encoded { vec: Some(b64), error_message: None });
//            return Box::into_raw(b);
//        }
//        None => { return error(format!("unknown error")) }
//    }
//}
//
//#[no_mangle]
//pub extern "C" fn destroy_encoded_object(p: *mut Encoded) {
//    unsafe { Box::from_raw(p) };
//}
//
/////エンコードしたBase64バイト列の長さを取得。失敗したら-1
//#[no_mangle]
//pub extern "C" fn get_encoded_length(p: *const Encoded) -> i32{
//    unsafe {
//        match (*p).vec{
//            Some(ref v) => v.len() as i32,
//            _ => -1,
//        }
//    }
//}
//
/////エンコードしたBase64バイト列のアドレスを取得。wasmからはこれとlengthを使ってバイト列を取り出す。
//#[no_mangle]
//pub extern "C" fn get_encoded_address(p: *const Encoded) -> *const u8{
//    unsafe {
//        match (*p).vec{
//            Some(ref v) => v.as_ptr(),
//            _ => u64::max_value() as *const u8,
//        }
//    }
//}
//
///// バイト列をコピーする。必要な量全てを一度に書ききるので、事前にget_item_length大きさを調べて、
///// 入りきる大きさのメモリ領域を準備する必要がある。成功すると1 失敗すると0
//#[no_mangle]
//pub extern "C" fn copy_encoded_bytes(p: *const Encoded, copy_to: *mut u8) -> i32{
//    unsafe {
//        match (*p).vec {
//            Some(ref v) =>{
//                v.as_ptr().copy_to(copy_to, v.len());
//                return 1;
//            }
//            _ => return 0,
//        }
//    }
//}
//
/////エラーメッセージがあるならその長さを取得
//#[no_mangle]
//pub extern "C" fn get_encode_error_message_length(p: *const Encoded) -> Option<i32> {
//    unsafe {
//        return match (*p).error_message {
//            Some(ref s) => Some(s.len() as i32),
//            None => None,
//        }
//    }
//}
//
/////長さを計ってからメモリ領域を用意しそれを渡してエラーメッセージを取得する・・・面倒臭さがクレイジー。成功すると1を返す
//#[no_mangle]
//pub extern "C" fn copy_encode_error_messag(p: *const Encoded, copy_to: *mut u8) -> i32 {
//    unsafe {
//        match (*p).error_message{
//            Some(ref v) => {
//                v.as_ptr().copy_to(copy_to, v.len());
//                return 1;
//            }
//            _ => return 0,
//        }
//    }
//}
