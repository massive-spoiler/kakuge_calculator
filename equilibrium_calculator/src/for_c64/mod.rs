//use super::for_wasm::string_builder::*;
//use super::for_wasm::result::*;
//
/////array<string>のjsonを返す
//#[no_mangle]
//pub extern "C" fn c64_decode(c64: *const StringBuilder) -> *mut ResultForWasm{
//    let s = unsafe{ &(*c64).s };
//    match ::compact64::compact64_decode::decode_compact64(s.as_bytes()){
//        Err(err) =>{
//            create_err(&err)
//        }
//        Ok(vec) =>{
//            let json_array = super::serde_json::Value::from(vec);
//            let json = ::serde_json::to_string(&json_array).unwrap();
//            create_ok(&json)
//        },
//    }
//}
//
/////array<string>のjsonを受け取る
//#[no_mangle]
//pub extern "C" fn c64_encode(array: *const StringBuilder) -> *mut ResultForWasm{
//    let s = unsafe{ &(*array).s };
//    use ::serde_json::Value;
//    let json_array : Result<Value, _> = ::serde_json::from_str(s);
//    match json_array{
//        Err(_) => return create_err("c64_encode needs a json array(string)"),
//        Ok(json) => match json.as_array() {
//            None => return create_err("c64_encode needs a json array(string)"),
//            Some(array) => {
//                let mut vec: Vec<String> = vec![];
//                for item in array {
//                    match item{
//                        Value::Number(num) =>{
//                            vec.push(num.to_string());
//                        }
//                        Value::String(s) =>{
//                            vec.push(s.to_string());
//                        }
//                        _ => return create_err(&format!("c64_encode needs a json array that only contains Number or String: {}", &item.to_string()))
//                    }
//                    vec.push(item.to_string());
//                }
//                match ::compact64::compact64_encode::encode_utf8s_into_compact64(&vec) {
//                    Err(e) => return create_err( &e),
//                    Ok(c64) => return create_ok( &c64),
//                }
//            }
//        },
//    }
//}