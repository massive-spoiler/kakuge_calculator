use crate::rate_table::RateTable;

///相性表 | 式神　| 妖怪 | 自然D | コンエル |
///式神   | -    | 50   | 60   |  60     |
///妖怪   |  -   | -    | 50   |  40     |
///自然D  |  -   |  -   !  -    !  60    !
///コンエル!  -   !  -   !  -    !  -     |
///
///こんな表を
///names =  "式神", "妖怪", "自然D", "コンエル"
///rates(0) = 0.5,   0.5,   0.6,   0.6
///rates(1) = 0.5,   0.5,   0.5,   0.4
///rates(2) = 0.4,   0.5,   0.5,   0.6
///rates(3) = 0.4,   0.6,   0.4,   0.5
///こんな構造体にすることを想定している
///
/// あとから確率にしないユースケースが現れたので、確率にしないバージョンも作った・・・
pub fn text_to_table(txt : &str, is_probability : bool) -> Result<RateTable, String>{
    let ans: Vec<&str> = txt.split("\n").collect();
    let ans : Vec<Vec<&str>> = ans.iter().map(|line| line.split("|").collect()).collect();
    let ans : Vec<Vec<String>> = ans.iter().map(|line| {
        line.iter().map(|item| item.trim().to_string()).collect()
    }).collect();

    //中身のない行は飛ばす。
    let mut ans : Vec<Vec<String>> = ans.into_iter().filter(|line| 1 < line.len()).collect();
    if ans.len() <= 1{ return Err(format!("table error")); }

    //最後は|があってもなくてもいい。
    // 最初の行で判別し、必要なら最後の空のアイテムを捨てる
    let is_empty = ans[0].last().unwrap().is_empty();
    if is_empty{
        for (i,line) in ans.iter_mut().enumerate(){
            if line.last().unwrap().is_empty() == false{
                return Err(format!("\"{}\" There's something at the end of line {}", line.last().unwrap(), i))
            }
            line.remove(line.len() - 1);
        }
    }

    let len = ans.len();
    for (i, line) in ans.iter().enumerate(){
        if line.len() != len{
            return Err(format!("line {}: the table is not square", i));
        }
    }

    //タグも厳密に一致してる必要あり
    for i in 1..ans.len(){
        if ans[0][i] != ans[i][0]{
            return Err(format!("{} != {}", &ans[0][i], &ans[i][0]));
        }
    }

    fn equal(f : f64, ans : f64) -> bool{
        return ans - 0.1 < f && f < ans + 0.1;
    }

    let mut a2 = vec![ vec![0.0; len]; len];
    //対角線に数字がある場合、50である必要あり
    for i in 1..len{
        match ans[i][i].parse::<f64>(){
            Ok(f) =>{
                if equal(f, 50.0) == false{
                    return Err(format!("row {} col {}: it should be 50:50", i, i))
                }
                a2[i][i] = 50.0;

            },
            _=>{ a2[i][i] = 50.0; }
        }
    }

    //対応するものが足して100にならないとエラー
    //普通はもう片方には何も書かないか、---とかを書く
    for i in 1..len{
        for k in i+1..len {
            match (ans[i][k].parse::<f64>(), ans[k][i].parse::<f64>()) {
                (Ok(l), Ok(r)) => {
                    if equal(l + r, 100.0) == false {
                        return Err(format!("row {} col {} & row {} col {} : they should be added up to 100", i, k, k, i));
                    }
                    a2[i][k] = l;
                    a2[k][i] = r;
                },
                (Ok(l), _) => {
                    if l < 0.0 || 100.0 < l {
                        return Err(format!("row {} col {} : please write a number between 0 to 100", i, k));
                    }
                    //let ans = l / 100.0;
                    a2[i][k] = l;
                    a2[k][i] = 100.0 - l;
                },
                (_, Ok(r)) => {
                    if r < 0.0 || 100.0 < r {
                        return Err(format!("row {} col {} : please write a number between 0 to 100", k, i));
                    }
                    //let ans = r / 100.0;
                    a2[k][i] = r;
                    a2[i][k] = 100.0 - r;
                }
                _ => {
                    return Err(format!("row {} col {} or col {} row {} : please write a number between 0 to 100", i, k, k, i));
                }
            }
        }
    }

//    for l in &a2{
//        println!("{:?}", l);
//    }

    if is_probability {
        for i in 1..len {
            for k in 1..len {
                a2[i][k] /= 100.0;
            }
        }
    }

    a2.remove(0);
    for line in &mut a2{
        line.remove(0);
    }


    let names : Vec<String> = ans[0][1..].iter().map(|s| s.to_string()).collect();
    return Ok(RateTable{ rates : a2, names });
}