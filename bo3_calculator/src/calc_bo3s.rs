//use crate::rate_table::RateTable;
use crate::calc_bo3::calc_bo3;
use crate::calc_bo3s_conditions::CalcBo3sConditions;
use crate::calc_bo3s_result::{CalcBo3sResult, CalcBo3sResultItem};

pub fn calc_bo3s(table : &Vec<Vec<f64>>, conds : &CalcBo3sConditions, limit : usize) -> Result<CalcBo3sResult, String>{

    let len = table.len();
    for vec in table{
        if vec.len() != len{
            return Err(format!("table is not square"))
        }
    }
    let mut result = CalcBo3sResult::new(limit);

    for i_p in 0..len{
        for k_p in i_p+1..len{
            let mut item = CalcBo3sResultItem::new(i_p, k_p, limit);

            for i_o in 0..len{
                for k_o in i_o+1..len{
                    if conds.is_valid(i_p, k_p, i_o, k_o){
                        let r = calc_bo3(table[i_p][i_o], table[i_p][k_o], table[k_p][i_o], table[k_p][k_o]);

                        item.add(i_o, k_o, r);
                    }
                }
            }
            if item.count != 0 {
                result.add(item);
            }
        }
    }

    return Ok(result);
}