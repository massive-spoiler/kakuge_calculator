use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct JsonInput{
    ///p1に利用可能な名前を全部列挙する
    pub p1 : Vec<String>,
    pub p2 : Vec<String>,
    pub o1 : Vec<String>,
    pub o2 : Vec<String>,
    ///形式に沿ったtable
    pub table : String,
    pub limit : usize,
    pub has_class : bool,
}