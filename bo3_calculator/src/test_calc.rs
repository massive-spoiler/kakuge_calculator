//use crate::calc_bo3::calc_bo3;
use crate::json_input::JsonInput;
use crate::calc_json::json_to_json;

pub fn get_sample_json() -> JsonInput{
    let table = r#"相性表 | 式神W | 妖怪Nc | コントロE | 自然D | アマツE | 潜伏R |
式神W | 50 | 50 | 60 | 60 | 40 | 40 |
妖怪Nc | 50 | 50 | 40 | 50 | 40 | 60 |
コントロE | 40 | 60 | 50 | 40 | 60 | 80 |
自然D | 40 | 50 | 60 | 50 | 40 | 40 |
アマツE | 60 | 60 | 40 | 60 | 50 | 45 |
潜伏R | 60 | 40 | 20 | 60 | 55 | 50 |
"#;

    let v : Vec<String> = ["式神W","妖怪Nc","自然D","コントロE","潜伏R","アマツE"].iter().map(|s| s.to_string()).collect();
    let json_input = JsonInput{
        limit : 10,
        table : table.to_string(),
        p1 : v.to_vec(),
        p2 : v.to_vec(),
        o1 : v.to_vec(),
        o2 : v.to_vec(),
        has_class : true,
    };
    return json_input;
}

pub fn calc_test() -> Result<(), String>{
//    let table = r#"
//相性表 | 式神　| 妖怪 | 自然D | コンエル |
//式神   | -    | 50   | 60   |  60     |
//妖怪   |  -   | -    | 50   |  40     |
//自然D  |  -   |  -   |  -    |  60    |
//コンエル|  -   |  -   |  -    |  -     |"#;

//    let table = r#"
//相性表 | 式神　| 妖怪 | 自然D | コンE | 潜伏 | アマツE | アグロヴ |
//式神   | -    | 50   | 60   |  60     | 35    |  35    |   30    |
//妖怪   |  -   | -    | 50   |  40     |  70   |  60    |   60    |
//自然D  |  -   |  -   |  -    |  60    |  40   |  40    |   45    |
//コンE |  -   |  -   |  -    |  -     |  75  |   40    |  45    |
//潜伏   |  -    |  -   |  -    |  -     |  -   |  50     |  35    |
//アマツE  |  -   |  -   |  -    |  -     |  -   |  -      |  55    |
//アグロヴ|  -   |  -   |  -    |  -    |   -   |   -     |   -    |"#;


    let json_input = get_sample_json();

    let json = serde_json::to_string(&json_input).unwrap();
    //let huga : JsonInput = serde_json::from_str(&json).unwrap();
    //println!("huga {:?}", huga);
    let json = json_to_json(&json)?;
    let json = serde_json::to_string_pretty(&json).unwrap();
    println!("{}", json);
    println!("ok");
    return Ok(());
}