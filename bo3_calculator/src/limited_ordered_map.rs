use std::collections::BTreeMap;
use ordered_float::OrderedFloat;

///上位だけを保存して後は捨てていく。メモリを節約。
#[derive(Debug, Clone)]
pub struct LimitedOrderedMap<T>{
    pub limit : usize,
    pub drop_lower : bool,
    map : BTreeMap<OrderedFloat<f64>, T>
}

impl<T> LimitedOrderedMap<T>{

    ///drop_lowerでない場合drop_higher、低い方をlimit個残して高いkeyのものを捨てていく
    pub fn new(limit : usize, drop_lower : bool) -> LimitedOrderedMap<T>{
        LimitedOrderedMap{ limit, drop_lower, map : BTreeMap::new() }
    }

    pub fn insert(&mut self, key : f64, item : T){
        let map = &mut self.map;
        map.insert(key.into(), item);
        self.adjust_size();
    }

    ///別に俺はオブジェクト指向ライブラリを作りたいわけではないので、中身も露出
    pub fn inner_map(&self) -> &BTreeMap<OrderedFloat<f64>, T>{
        &self.map
    }

    pub fn adjust_size(&mut self){
        let limit = self.limit;
        loop{
            let map = &mut self.map;
            if map.len() <= limit{ break; }

            if self.drop_lower {
                let key = *map.keys().nth(0).unwrap();
                map.remove(&key); //この二段構えは絶対遅いな・・・ちゃんと調べて早い実装を使うべき。first_entryはunstableとかいわれたけど・・・
            } else{
                let key = *map.keys().last().unwrap();
                map.remove(&key);
            }
        }
    }
}