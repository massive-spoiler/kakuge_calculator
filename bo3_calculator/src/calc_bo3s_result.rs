use crate::limited_ordered_map::LimitedOrderedMap;
use crate::bo3_result::Bo3Result;

pub struct CalcBo3sResult{
    ///勝率最悪の組み合わせの勝率が高いもの
    pub average_worst : LimitedOrderedMap<CalcBo3sResultItem>,
    ///当て負け時勝率最悪の組み合わせの勝率が高いもの
    pub atemake_worst : LimitedOrderedMap<CalcBo3sResultItem>,
    ///平均勝率が高いもの
    pub average_average : LimitedOrderedMap<CalcBo3sResultItem>,
    ///当て負け時勝率の平均が高いもの
    pub atemake_average : LimitedOrderedMap<CalcBo3sResultItem>,
}

#[derive(Debug, Clone)]
pub struct CalcBo3sResultItem{
    ///勝率最悪の組み合わせの勝率
    pub average_worst : f64,
    ///当て負けじ勝率最悪の組み合わせの勝率
    pub atemake_worst : f64,
    ///勝率の総和
    pub average_sum : f64,
    ///当て負け時の勝率の総和
    pub atemake_sum : f64,
    ///加えたBo3Result数。これで割れば平均が出る
    pub count : usize,
    pub average_worsts : LimitedOrderedMap<CalcBo3sResultItem2>,
    pub atemake_worsts : LimitedOrderedMap<CalcBo3sResultItem2>,
    pub u : usize,
    pub d : usize,
}

impl CalcBo3sResultItem{
    pub fn new(u : usize, d : usize, limit : usize) -> CalcBo3sResultItem{
        CalcBo3sResultItem{
            u, d,
            average_worst : 2.0,
            atemake_worst : 2.0,
            average_sum : 0.0,
            atemake_sum : 0.0,
            count : 0,
            average_worsts : LimitedOrderedMap::new(limit, false),
            atemake_worsts : LimitedOrderedMap::new(limit, false),
        }
    }

    pub fn add(&mut self, l : usize, r : usize, result : Bo3Result){
        let r = CalcBo3sResultItem2{
            l, r, result : result.clone(),
        };
        let average = result.average();
        let low = result.low();
        if  average < self.average_worst{
            self.average_worst = average;
        }
        if low < self.atemake_worst{
            self.atemake_worst = low;
        }
        self.average_sum += average;
        self.atemake_sum += low;
        self.count += 1;
        self.average_worsts.insert(average, r.clone());
        self.atemake_worsts.insert(low, r);
    }

    pub fn average_average(&self) -> f64{
        self.average_sum / self.count as f64
    }

    pub fn atemake_average(&self) -> f64{
        self.atemake_sum / self.count as f64
    }
}

#[derive(Debug, Clone)]
pub struct CalcBo3sResultItem2{
    pub result : Bo3Result,
    pub l : usize,
    pub r : usize,
}

impl CalcBo3sResult{
    pub fn new(limit : usize) -> CalcBo3sResult{
        CalcBo3sResult{
            average_worst : LimitedOrderedMap::<CalcBo3sResultItem>::new(limit, true),
            atemake_worst : LimitedOrderedMap::<CalcBo3sResultItem>::new(limit, true),
            average_average : LimitedOrderedMap::<CalcBo3sResultItem>::new(limit, true),
            atemake_average : LimitedOrderedMap::<CalcBo3sResultItem>::new(limit, true),
        }
    }

    pub fn add(&mut self, input : CalcBo3sResultItem){
        self.average_worst.insert(input.average_worst, input.clone());
        self.atemake_worst.insert(input.atemake_worst, input.clone());
        self.average_average.insert(input.average_average(), input.clone());
        self.atemake_average.insert(input.atemake_average(), input);
    }
}