#[derive(Debug, Clone)]
pub struct Bo3Result{
    ul_p : f64,
    ur_p : f64,
}

impl Bo3Result{
    pub fn ul(&self) -> f64{ self.ul_p }
    pub fn ur(&self) -> f64{ self.ur_p }
    pub fn dl(&self) -> f64{ self.ur() }
    pub fn dr(&self) -> f64{ self.ul() }
    pub fn high(&self) -> f64{ f64::max(self.ul(), self.ur()) }
    pub fn low(&self) -> f64{ f64::min(self.ul(), self.ur()) }
    pub fn average(&self) -> f64{ (self.ul() + self.ur()) / 2.0 }
    pub fn new(ul : f64, ur : f64) -> Bo3Result { Bo3Result{ ul_p : ul, ur_p : ur } }
}