use crate::bo3_result::Bo3Result;

/// 勝率表    | 式神 | 妖怪 |
/// ドラゴン  | 0.4 | 0.5 |
/// コンエル  | 0.3 | 0.7 |
/// みたいな図を考えた時に up left(0.4) up right(0.5) down left(0.3) down right(0.7)を入れると、
/// 最初にulが当たったとき、最初にurが当たったとき・・・といった形でBO3勝率を返す。
/// BO3では最初に当たった組合せ以外はその後の勝率に影響を与える要素はない
pub fn calc_bo3(ul : f64, ur : f64, dl : f64, dr : f64) -> Bo3Result{
    //let up = 1.0 - (1.0 - ul) * (1.0 - ur);
    let down = 1.0 - (1.0 - dl) * (1.0 - dr); //両方に負けるのでなければ勝ち
    let left = ul * dl;
    let right = ur * dr;

    //ul で勝つと、 d のどちらかで勝てば良い(down)
    //ul で負けた場合、 uとd でrに勝たなければならない(right)
    let ans_ul = ul * down + (1.0-ul) * right;
    let ans_ur = ur * down + (1.0-ur) * left;
    //数式的に、ans_dlとans_ur, ans_drとans_ulは同じなので計算する必要なし
    //let ans_dl = dl * up + (1.0-dl) * right;
    //ans_dr = dr * up + (1.0-dr)*left;
    return Bo3Result::new(ans_ul, ans_ur);
}

//ans_urとans_dlが同じかどうか確かめる
//ans_ur = ur * (1.0 - (1.0 - dl) * (1.0 - dr)) + (1.0-ur) * (ul * dl);
//ans_dl = dl * (1.0 - (1.0 - ul) * (1.0 - ur)) + (1.0-dl) * (ur * dr);
//dl - dl(1.0-ul)*(1.0-ur) + ur*dr -dl(ur*dr)
//dl - dl(1.0-ul-ur+ul*ur) + ur*dr - dl*ur*dr
//dl - dl + dl*ul + dl*ur - dl*ul*ur + ur*dr - dl*ur*dr
//dl*ul + dl*ur - dl*ul*ur + ur*dr - dl*ur*dr

//ans_urとans_dlの変換dlがurに、urがdlに、 ulがdrに、  drがulに
//dl*ul + dl*ur - dl*ul*ur + ur*dr - dl*ur*dr
//ur*ul + dl*ur - dl*ul*ur + dl*dr - dl*ur*dr //urとdlを逆転
//ur*dr + dl*ur - dl*dr*ur + dl*ul - dl*ur*ul //ulとdrを逆転

//ans_ur-ans_dlを計算
//dl*ul + dl*ur - dl*ul*ur + ur*dr - dl*ur*dr - (ur*dr + dl*ur - dl*dr*ur + dl*ul - dl*ur*ul)
//dl*ul + dl*ur - dl*ul*ur + ur*dr - dl*ur*dr - ur*dr - dl*ur + dl*dr*ur - dl*ul + dl*ur*ul
//=0 やっぱり同じだった。