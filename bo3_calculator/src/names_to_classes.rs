use lazy_static::lazy_static;
use regex::Regex;
use std::ops::Index;
use std::collections::HashMap;

/// 式神W | 自然D | コンE | 自然W | アマツE | ほげ | 密林E | ふが |
/// といったクラス定義が末尾についているやつは、同一クラスはBO3でチームを組ませられないため、
/// 組ませられないものの index のセットを作って判別できるようにしておく
///
/// vec(0) = (0, 3)
/// vec(1) = (1)
/// vec(2) = (2,4,6)
/// vec(3) = (0,3)
/// vec(4) = (2,4,6)
/// vec(5) = ()
/// vec(6) = (2,4,6)
pub fn names_to_classes(names : &Vec<String>) -> Vec<Vec<usize>>{
    let mut classes : Vec<String> = vec![];
    let mut map : HashMap<String, Vec<usize>> = HashMap::new();

    for (i, name) in names.iter().enumerate(){
        if let Some(c) = name_to_class(name){
            classes.push(c.to_string());
            map.entry(c)
                .or_insert_with(Vec::new)
                .push(i);
        }
        else{ classes.push("".to_string()); }
    }
    let mut result : Vec<Vec<usize>> = vec![];

    for class in &classes{
        if class.is_empty() == false{
            let v = map.get(class).unwrap();
            result.push(v.to_vec());
        }
        else{
            result.push(vec![]);
        }
    }
    return result;
}

fn name_to_class(s : &str) -> Option<String>{
    lazy_static! {
        static ref RE: Regex = Regex::new("^[^a-zA-Z]+([a-zA-Z]+)$").unwrap();
    }
    let rex : &Regex = &RE;
    if let Some(c) = rex.captures(s){
        return Some(c.index(1).to_string());
    }
    else{ return None; }
}