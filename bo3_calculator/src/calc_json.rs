use crate::text_to_table::text_to_table;
use crate::calc_bo3s::calc_bo3s;
use crate::json_input::JsonInput;
use crate::calc_bo3s_conditions::CalcBo3sConditions;
use std::collections::{HashMap};
use crate::calc_bo3s_result::CalcBo3sResult;
use crate::analyze_calc_bo3s_result::analyze_calc_bo3s_result;
use crate::rate_table::RateTable;
use crate::json_string_output::JsonStringOutput;
use crate::names_to_classes::names_to_classes;

///jsonを受け取りCalcBo3sResultを取得。
pub fn calc_json(json : &str) -> Result<(CalcBo3sResult, RateTable), String>{
    let json : JsonInput = match serde_json::from_str(json){
        Ok(a) => a,
        Err(_) =>{ return Err(format!("json couldn't be converted {}", json)); }
    };
    let table = text_to_table(&json.table, true)?;
    //println!("{:?}", table);
    let conds = get_conds(&table.names, &json)?;
    let r = calc_bo3s(&table.rates, &conds, json.limit)?;
    return Ok((r,table));
}

///jsonを受け取り解析結果文字列をjsonで取得する
pub fn json_to_json(json : &str) -> Result<JsonStringOutput, String>{
    let (r, table) = calc_json(json)?;
    let r = analyze_calc_bo3s_result(&r, &table)?;
    return Ok(r);
}

pub fn get_conds(names : &Vec<String>, json : &JsonInput) -> Result<CalcBo3sConditions, String>{
    let map : HashMap<String, usize> = names.iter().enumerate().map(|(i, item)| (item.to_string(), i)).collect();
    fn to(map : &HashMap<String, usize>, json : &Vec<String>) -> Result<Vec<usize>, String>{
        let mut r = Vec::<usize>::new();
        for name in json{
            let ans = map.get(name).ok_or(format!("{} there's no such name in the table", name))?;
            r.push(*ans);
        }
        return Ok(r);
    }
    let classes = names_to_classes(names);

    return Ok(
        CalcBo3sConditions::new(
        &to(&map, &json.p1)?,
        &to(&map, &json.p2)?,
        &to(&map, &json.o1)?,
        &to(&map, &json.o2)?,
        if json.has_class{ Some(&classes) } else{ None },
        )
    );
}