
pub mod calc_bo3;
pub mod bo3_result;
pub mod rate_table;
pub mod calc_bo3s;
pub mod calc_bo3s_conditions;
pub mod calc_bo3s_result;
pub mod text_to_table;
//pub mod calc_table;
pub mod calc_table_result;
pub mod calc_json;
pub mod json_input;
//pub mod json_output;
pub mod json_string_output;
pub mod analyze_calc_bo3s_result;
pub mod names_to_classes;
mod test_names_to_classes;
pub mod test_calc;

mod limited_ordered_map;
#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {

        super::hoge();
    }


}

pub fn hoge(){
    test_names_to_classes::test_names_to_classes();
    match test_calc::calc_test(){
        Ok(_) =>{},
        Err(s) =>{ println!("{}", &s) }
    }
}
