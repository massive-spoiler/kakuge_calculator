use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct JsonStringOutput {
    pub average_worst : Vec<JsonStringOutputItem>,
    pub atemake_worst : Vec<JsonStringOutputItem>,
    pub average_average : Vec<JsonStringOutputItem>,
    pub atemake_average : Vec<JsonStringOutputItem>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JsonStringOutputItem {
    pub desc : String,
    pub average_enemies : Vec<String>,
    pub atemake_enemies : Vec<String>,
}