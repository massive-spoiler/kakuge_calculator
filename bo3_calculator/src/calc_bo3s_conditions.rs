use std::collections::HashSet;

pub struct CalcBo3sConditions{
    pub player1_include: HashSet<usize>,
    pub player2_include: HashSet<usize>,
    pub oppo1_include: HashSet<usize>,
    pub oppo2_include: HashSet<usize>,
    pub classes : Option<Vec<HashSet<usize>>>,
}

impl CalcBo3sConditions{
    pub fn new(
        player1_must_include : &[usize],
        player2_must_include : &[usize],
        oppo1_must_include : &[usize],
        oppo2_must_include : &[usize],
        classes : Option<&[Vec<usize>]>,
    ) -> CalcBo3sConditions{
        fn to(slice : &[usize]) -> HashSet<usize>{
            slice.iter().map(|v| *v).collect()
        }

        CalcBo3sConditions{
            player1_include: to(player1_must_include),
            player2_include: to(player2_must_include),
            oppo1_include: to(oppo1_must_include),
            oppo2_include: to(oppo2_must_include),
            classes : classes.map(|vec|
                vec.iter().map(|v|
                    v.iter().map(|i| *i).collect()).collect())
        }
    }

    pub fn is_valid(&self, p1 : usize, p2 : usize, o1 : usize, o2 : usize) -> bool{
        if is_valid2(&self.player1_include, &self.player2_include, p1, p2) == false{
            if is_valid2(&self.player1_include, &self.player2_include, p2, p1) == false{
                return false;
            }
        }
        if is_valid2(&self.oppo1_include, &self.oppo2_include, o1, o2) == false{
            if is_valid2(&self.oppo1_include, &self.oppo2_include, o2, o1) == false{
                return false;
            }
        }

        if let Some(c) = &self.classes{
            if c[p1].contains(&p2) || c[o1].contains(&o2){
                return false;
            }
        }
        return true;
    }
}

fn is_valid2(l : &HashSet<usize>, r : &HashSet<usize>, l_ind : usize, r_ind : usize) -> bool{
    if l.len() != 0{
        if l.contains(&l_ind) == false{
            return false;
        }
    }
    if r.len() != 0{
        if r.contains(&r_ind) == false{
            return false;
        }
    }
    return true;
}