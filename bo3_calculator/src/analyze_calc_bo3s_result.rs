use crate::calc_bo3s_result::{CalcBo3sResult, CalcBo3sResultItem, CalcBo3sResultItem2};
use crate::rate_table::RateTable;
use crate::json_string_output::{JsonStringOutput, JsonStringOutputItem};
use crate::limited_ordered_map::LimitedOrderedMap;

pub fn analyze_calc_bo3s_result(r : &CalcBo3sResult, table : &RateTable) -> Result<JsonStringOutput, String>{
    return Ok(JsonStringOutput {
        average_worst : hoge(&r.average_worst, table)?,
        atemake_worst : hoge(&r.atemake_worst, table)?,
        average_average : hoge(&r.average_average, table)?,
        atemake_average : hoge(&r.atemake_average, table)?,
    });
}

fn hoge(results : &LimitedOrderedMap<CalcBo3sResultItem>, table : &RateTable) -> Result<Vec<JsonStringOutputItem>, String>{
    let mut vec : Vec<JsonStringOutputItem> = vec![];
    for (_key, value) in results.inner_map().iter().rev(){
        let a = hoge2(value, table)?;
        vec.push(a)
    }
    return Ok(vec);
}

fn hoge2(inp : &CalcBo3sResultItem, table : &RateTable) -> Result<JsonStringOutputItem, String>{
    Ok(JsonStringOutputItem {
        desc : get_desc(inp, table)?,
        average_enemies : get_enemies(&inp.average_worsts, table, inp)?,
        atemake_enemies : get_enemies(&inp.atemake_worsts, table, inp)?,
    })
}

fn get_desc(a : &CalcBo3sResultItem, table : &RateTable) -> Result<String, String> {
    return Ok(format!("{}, {}: 平均勝率 {:.2}% 平均当て負け時勝率 {:.2}%, 最悪組み合わせ勝率 {:.2}% 最悪時当て負け勝率 {:.2}%",
                      table.get_name(a.u), table.get_name(a.d),
                      a.average_average() * 100.0, a.atemake_average() * 100.0,
                      a.average_worst * 100.0, a.atemake_worst * 100.0));
}

fn get_enemies(a : &LimitedOrderedMap<CalcBo3sResultItem2>, table : &RateTable, p : &CalcBo3sResultItem) -> Result<Vec<String>, String>{
    let mut vec : Vec<String> = vec![];
    for (_k, v) in a.inner_map(){
        vec.push(get_desc2(v, table, p)?)
    }
    return Ok(vec);
}

fn get_desc2(a : &CalcBo3sResultItem2, table : &RateTable, p : &CalcBo3sResultItem) -> Result<String, String>{
    let (first, second) = if a.result.ul() < a.result.ur(){
        (a.r, a.l)  //urが高いのでuはrと当たりたい
    } else{
        (a.l, a.r)
    };

    let atekati_kumiawase = format!("当て勝ち組み合わせ {} -> {} , {} -> {}", table.get_name(p.u), table.get_name(first), table.get_name(p.d), table.get_name(second) );

    Ok(format!("{}, {}: 勝率 {:.2}% 当て勝ち時勝率 {:.2}% 当て負け時勝率 {:.2}% {}",
               table.get_name(a.l), table.get_name(a.r), a.result.average() * 100.0, a.result.high() * 100.0, a.result.low() * 100.0, atekati_kumiawase))
}