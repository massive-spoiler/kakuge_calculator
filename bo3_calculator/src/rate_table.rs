///相性表 | 式神　| 妖怪 | 自然D | コンエル |
///式神   | -    | 50   | 60   |  60     |
///妖怪   |  -   | -    | 50   |  40     |
///自然D  |  -   |  -   !  -    !  60    !
///コンエル!  -   !  -   !  -    !  -     |
///
///こんな表を
///names =  "式神", "妖怪", "自然D", "コンエル"
///rates(0) = 0.5,   0.5,   0.6,   0.6
///rates(1) = 0.5,   0.5,   0.5,   0.4
///rates(2) = 0.4,   0.5,   0.5,   0.6
///rates(3) = 0.4,   0.6,   0.4,   0.5
///こんな構造体にすることを想定している
#[derive(Debug)]
pub struct RateTable{
    pub names : Vec<String>,
    pub rates : Vec<Vec<f64>>,
}

impl RateTable{
    pub fn get_name(&self, index : usize) -> String{
        self.names[index].to_string()
    }
}