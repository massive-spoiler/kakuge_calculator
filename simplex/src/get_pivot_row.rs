use super::EPS;

///dest_colから増加限界のrowを見つける。
pub fn get_pivot_row(a : &Vec<Vec<f64>>, n_var : usize, n_cond : usize,
                     dest_col : usize)
    -> Option<usize>
{
    let mut found_result= false;
    let mut opt_cond = 0;
    let mut opt_limit = 0.0;
    let mut i = 0;
    while i < n_cond {
        if a[i + 1][dest_col] < -EPS {
            opt_limit = -a[i + 1][0] / a[i + 1][dest_col];
            opt_cond = i;
            found_result = true;
            break;
        }
        i += 1;
    }
    if found_result == false{ return None; }
    i += 1;
    while i < n_cond {
        if a[i + 1][dest_col] < -EPS {
            let q = -a[i + 1][0] / a[i + 1][dest_col];
            if q < opt_limit {
                opt_cond = i;
                opt_limit = q;
            } else if q == opt_limit {
                let mut q0 = 0.0;
                let mut qp = 0.0;
                //退化
                for k in 0..n_var {
                    qp = -a[opt_cond + 1][k + 1] / a[opt_cond + 1][dest_col];
                    q0 = -a[i + 1][k + 1] / a[i + 1][dest_col];
                    if q0 != qp { break; }
                }
                //他の係数の比も比べてより小さい方を優先するようだ。
                if q0 < qp { opt_cond = i; }
            }
        }
        i += 1;
    }
    return Some(opt_cond+1);
}