use super::get_pivot_row::get_pivot_row;
use super::hakidashi::hakidashi;
use super::get_max_col::get_max_col;
use super::{LpResult, LpError};

pub fn phase2(a : &mut Vec<Vec<f64>>, n_cond : usize, n_vars : usize,
          exchangables : &Vec<usize>, //exs_size : usize,
            izrov : &mut Vec<usize>, iposv : &mut Vec<usize>) -> Result<LpResult,LpError>{
    loop {
        let max = get_max_col(a, 0,
                              &exchangables, /*exs_size,*/ false);
        if max.value <= 0.0 {
            return Ok(LpResult::new(a, iposv));
        }
        let col = max.col;
        let row = get_pivot_row(a, n_vars, n_cond, col)
            .ok_or(LpError::DestFunctionUnbounded)?;

        hakidashi(a, n_cond, n_vars, row, col);
        let temp = izrov[col-1];
        izrov[col-1] = iposv[row-1];
        iposv[row-1] = temp;
    }
}