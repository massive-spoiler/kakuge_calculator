pub fn simplex(dest_func : &Vec<f64>,
               less_cond : &Vec<Vec<f64>>, greater_cond : &Vec<Vec<f64>>, eq_cond : &Vec<Vec<f64>>)
               -> Result<LpResult,LpError> {
    super::simplex_main::simplex_main2(dest_func, less_cond, greater_cond, eq_cond)
}

#[derive(Debug)]
pub enum LpError{
    AuxiliaryFunctionCantReachZero,
    AuxiliaryFunctionUnbounded,
    DestFunctionUnbounded,
    BadInput(String),
}

pub struct LpResult{
    pub table : Vec<Vec<f64>>,
    pub max : f64,
    pub optimal: Vec<f64>
}

impl LpResult{
    #[allow(dead_code)]
    pub fn print(&self) {
        println!("[ table ]");
        let a = &self.table;
        a.iter().for_each(|vec| {
            vec.iter().for_each(|val| {
                print!("{} ", val);
            });
            println!();
        });

        println!("解 : {}", self.max);
        self.optimal.iter().enumerate().for_each(|(index,value)|{
            println!(" x{} = {}", index + 1, value);
        });
    }

    pub fn new(table : &Vec<Vec<f64>>, iposv : &Vec<usize>) -> LpResult{
        let optimal : Vec<f64> = (0..table[0].len() - 1).map(|var|{
            iposv.iter().position(|pos|{ *pos == var})
                .map_or(0.0, |cond|{ table[cond+1][0] })
        }).collect();
        LpResult{ table : table.clone(), max : table[0][0], optimal }
    }
}
