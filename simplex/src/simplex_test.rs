////use super::simplex_main::simplex_main2;
//use std::ops::Index;
//
//
//pub fn simplex_test() {
//
//
//    //	最大化条件式 : z = x1 + x2 + 3 * x3 - x4 / 2
//    //	制約条件 :
//    //		x1 + 2 * x3 <= 740
//    //		2 * x2 - 7 * x4 <= 0
//    //		x2 - x3 + 2 * x4 >= 0.5
//    //		x1 + x2 + x3 + x4 = 9
//
//    let a: Vec<Vec<f64>> = vec![
//        vec![0.0, 1.0, 1.0, 3.0, -0.5],
//        vec![740.0, -1.0, 0.0, -2.0, 0.0],
//        vec![0.0, 0.0, -2.0, 0.0, 7.0],
//        vec![0.5, 0.0, -1.0, 1.0, -2.0],
//        vec![9.0, -1.0, -1.0, -1.0, -1.0],
//        vec![0.0; 5]
//    ];
//
//    match super::simplex(&a[0],
//                        &a.index(1..3).iter().map(|vec| vec.clone()).collect(),
//                        &vec![a[3].clone()], &vec![a[4].clone()]) {
//        Ok(result) => result.print(),
//        Err(_) => {}
//    }
//}
//
//
