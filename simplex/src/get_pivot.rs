use super::LpError;
use super::EPS;
use super::get_max_col::get_max_col;
use super::get_pivot_row::get_pivot_row;

pub enum PivotRet{
    Pivot{ row : usize, col : usize },
    Completed,
}

pub fn get_pivot(a : &mut Vec<Vec<f64>>, n_var : usize, n_cond : usize, n_less : usize, n_greater : usize,
                 exchangables : &Vec<usize>, //exs_size : usize,
                 iposv : &Vec<usize>, greater_flags : &Vec<bool>) -> Result<PivotRet, LpError>{
    // 補助目的関数の最大の係数を探す
    let max = get_max_col(a, n_cond + 1,
                          exchangables, /*exs_size,*/ false);
    let col = max.col; //bmax = max.value;

    if max.value <= EPS && a[n_cond + 1][0] < -EPS {
        //補助目的関数は負のままであり、これ以上改良できない。つまり、可能解は存在しない。
        return Err(LpError::AuxiliaryFunctionCantReachZero);
    } else if max.value <= EPS && a[n_cond + 1][0] <= EPS {
        let eq_id = n_less + n_greater;

        //補助目的関数の最大値 0 が得られた。つまり、可能な初期解が見つかった。
        if eq_id <= n_cond {
            //eqの式を調べる
            for cond in eq_id..n_cond {
                if iposv[cond] == cond + n_var {
                    //eqの人口変数が基底変数に残っている
                    let max = get_max_col(a, cond+1,
                                          exchangables, /*exs_size,*/ true);

                    if max.value > 0.0 {
                        //eqの人口変数を吐き出す
                        //そもそもeqやgreaterがあっても(0,0)が初期解として可能でないとは限らないはず
                        //その対応なのだと思うが、
                        //正の係数がない場合は吐き出さなくて良い理由は不明
                        return Ok(PivotRet::Pivot{ row: cond+1, col: max.col });
                    }
                }
            }
        }


        let greater_last = eq_id - 1;
        //greaterは人口変数を消してスラック変数だけにする時に正負が逆転する
        //逆転がこのタイミングでいい理由がよくわからない。
        //if n_less < greater_last {
            for i in n_less..greater_last {
                if greater_flags[i - n_less] {
                    for k in 0..n_var + 1 {
                        a[i + 1][k] = -a[i + 1][k];
                    }
                }
            }
        //}
        return Ok(PivotRet::Completed);
    }

    let row = get_pivot_row(a, n_var, n_cond, col)
            .ok_or(LpError::AuxiliaryFunctionUnbounded)?;
    return Ok(PivotRet::Pivot{ row, col });
}