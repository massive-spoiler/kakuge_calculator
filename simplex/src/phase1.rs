//use super::EPS;
use super::get_pivot::{get_pivot,PivotRet};
//use super::get_pivot_row::get_pivot_row;
use super::hakidashi::hakidashi;
//use super::get_max_value::get_max_value;
use super::LpError;

pub fn phase1(a : &mut Vec<Vec<f64>>, n_cond : usize, n_less : usize, n_greater : usize, n_vars : usize,
exchangables : &mut Vec<usize>, //exs_size : &mut usize,
izrov : &mut Vec<usize>, iposv : &mut Vec<usize>) -> Result<(), LpError> {
    let mut greater_flags = vec![true; n_greater];

    loop {
        let ret = get_pivot(a, n_vars, n_cond, n_less, n_greater,
                            exchangables, /*exs_size,*/ iposv, &greater_flags)?;
        match ret {
            PivotRet::Completed => { return Ok(()) }
            PivotRet::Pivot { row, col } => {
                hakidashi(a, n_cond + 1, n_vars, row, col);

                if iposv[row - 1] >= n_vars + n_less + n_greater {
                    //n_eqの場合　永久に触らないので候補から外してしまう
                    //確かにn_eqの場合、人口変数排除後に基底変数が一個消えるんだけど、
                    //この段階で排除していいものなのかよくわからない。
                    let item_index = exchangables.iter()
                        .position(|val|{ *val == col - 1}).unwrap();
                    exchangables.remove(item_index);

                    a[n_cond + 1][col] += 1.0; //これがなぜ必要なのかわからん・・・
                    for i in 0..n_cond + 2 { a[i][col] = -a[i][col]; }
                } else if iposv[row - 1] >= n_vars + n_less {
                    //greaterの場合
                    let greater_id = iposv[row - 1] - n_less - n_vars;
                    if greater_flags[greater_id] {
                        greater_flags[greater_id] = false;
                        a[n_cond + 1][col] += 1.0; // これはわかる。greaterは目的関数に1が発生する
                        for i in 0..n_cond + 2 { a[i][col] = -a[i][col]; }
                    }
                }
                let temp = izrov[col - 1];
                izrov[col - 1] = iposv[row - 1];
                iposv[row - 1] = temp;
            }
        }
    }
}