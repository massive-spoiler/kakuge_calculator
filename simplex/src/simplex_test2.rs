//use super::simplex_main::simplex_main2;
//
//pub fn simplex_test2() {
////    let n_cond = 3;
////    let n_var = 2;
////    let n_less = 1; //<=条件数
////    let n_greater = 1; //>=条件数
//
//    //目的関数，
//    //z = -x1 - x2
//    //制約条件，
//    //3x1 + 5x2 ≦ 15　　　(1)
//    //-2x1 - x2 ≦ -5　　　(2)
//    //x1 - x2 ＝ 1　　　(3)
//    //https://www.sist.ac.jp/~suganuma/kougi/other_lecture/SE/opt/linear/linear.htm#1.4
//
//    let dest_func = vec![0.0, -1.0, -1.0];
//    let less_cond = vec![
//        vec![15.0, -3.0, -5.0],
//    ];
//    let greater_cond = vec![
//        vec![5.0, -2.0, -1.0],
//    ];
//    let eq_cond = vec![
//        vec![1.0, -1.0, 1.0],
//    ];
//
//    match simplex_main2(&dest_func, &less_cond, &greater_cond, &eq_cond){
//        Ok(result) => result.print(),
//        Err(_) => {}
//    };
//}
//
//
