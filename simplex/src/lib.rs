mod simplex_main;
mod get_max_col;
mod get_pivot_row;
mod get_pivot;
mod hakidashi;
mod phase1;
mod phase2;
mod publics;
//pub mod simplex_test;
//pub mod simplex_test2;

const EPS :f64 = 1.0e-6;

pub use self::publics::*;