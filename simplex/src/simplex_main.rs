use crate::{LpResult, LpError};



pub fn simplex_main(mut a : Vec<Vec<f64>>, n_cond : usize, n_vars : usize,
                    n_less : usize, n_greater : usize)
                    -> Result<LpResult,LpError> {
    let mut exchangables = vec![0; n_vars];

    //非基底変数。配列のcol方向。初めはx0~xNの変数を意味しているが、基底変数とスワップされていく。
    let mut izrov = vec![0; n_vars];
    //基底変数。配列のrow方向。初めは人工変数やスラック変数を指しているが、非基底変数とスワップされていく。
    let mut iposv = vec![0; n_cond];

    for k in 0..n_vars {
        exchangables[k] = k;
        izrov[k] = k;
    }

    for i in 0..n_cond {
        if a[i + 1][0] < 0.0 {
            //条件式の定数は非負になるように変形されていなければならない
            //a[0]は目的関数なので定数は0である
            return Err(LpError::BadInput("Constants must be non negative".into()))
        }
        iposv[i] = n_vars + i;
    }

    //less条件しかない場合は原点を初期値とすることが出来るが、
    // そうでない場合は初期値を探すために二段階シンプレックス法(BigM)を行う。
    if n_cond != n_less {
        /* 補助目的関数を計算 */
        for k in 0..n_vars + 1 {
            let mut q1 = 0.0;
            for i in n_less..n_cond {
                q1 += a[i+1][k];
            }
            a[n_cond + 1][k] = -q1;//最下段が補助目的関数
        }
        super::phase1::phase1(&mut a, n_cond, n_less, n_greater, n_vars,
                              &mut exchangables, &mut izrov, &mut iposv)?;
    }

    super::phase2::phase2(&mut a, n_cond, n_vars, &exchangables, //exs_size, //&iranai, iranai_size,
                          &mut izrov, &mut iposv)
}

pub fn simplex_main2(dest_func : &Vec<f64>,
                     less_cond : &Vec<Vec<f64>>, greater_cond : &Vec<Vec<f64>>, eq_cond : &Vec<Vec<f64>>)
                     -> Result<LpResult,LpError> {
    let mut a: Vec<Vec<f64>> = vec![dest_func.clone()];
    a.extend(less_cond.clone().into_iter());
    a.extend(greater_cond.clone().into_iter());
    a.extend(eq_cond.clone().into_iter());
    a.push(vec![0.0;dest_func.len()]); //最下段は補助目的関数が入るので領域を作っておく。

    if a.iter().any(|vec| vec.len() != dest_func.len()) {
        return Err(LpError::BadInput("Input vectors must have the same length".into()));
    }

    let n_cond = a.len() - 2;
    let n_vars = dest_func.len() - 1;

    simplex_main(a, n_cond, n_vars,
                 less_cond.len(), greater_cond.len())
}









