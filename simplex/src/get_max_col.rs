pub struct MaxCol {
    pub col : usize,
    pub value : f64,
}
///dest_rowの関数の最大の係数を探す。
pub fn get_max_col(a : &Vec<Vec<f64>>, dest_row : usize,
                   exchangables : &Vec<usize>,//, exs_size : usize,
                   is_abs : bool) -> MaxCol
{
    let mut max_id = exchangables[0];
    let mut max_value = a[dest_row][max_id + 1];
    for k in 1..exchangables.len() {
        let current = a[dest_row][exchangables[k] + 1];
        let is_max = if is_abs {
            current.abs() > max_value.abs()
        }else{
            current > max_value
        };
        if is_max {
            max_value = current;
            max_id = exchangables[k];
        }
    }
    MaxCol { col : max_id+1, value : max_value }
}