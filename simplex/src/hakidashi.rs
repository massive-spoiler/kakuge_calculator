///row <= row_maxのrowまで吐き出す。
pub fn hakidashi(a : &mut Vec<Vec<f64>>, row_max : usize, n_vars : usize, row : usize, col : usize) {

    let piv = 1.0 / a[row][col];
    for i in 0..row_max + 1 {
        if i != row {
            a[i][col] *= piv;
            for k in 0..n_vars + 1 {
                if k != col {
                    a[i][k] -= a[row][k] * a[i][col];
                }
            }
        }
    }
    for k in 0..n_vars + 1 {
        if k != col { a[row][k] *= -piv; }
    }
    a[row][col] = piv;

}