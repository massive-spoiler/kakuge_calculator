pub struct Decimal{
    pub int : i64,
    pub scale : u32,
}

impl Decimal{
    pub fn to_string(&self) -> String{
        let mut s = self.int.to_string();
        if self.scale != 0{
            let len = s.len();
            let point = len as i32 - self.scale as i32;
            if point <= 0{
                let mut r = "0.".to_string();
                r += unsafe{ &String::from_utf8_unchecked(vec![b'0'; point.abs() as usize]) };
                r += &s;
                return r;
            }
            else{
                s.insert(point as usize, '.');
                return s;
            }
        }
        return s;
    }

    pub fn from_str(s : &str) -> Option<Decimal> {
        if s.len() == 0 { return None; }
        let mut s = s.as_bytes();
        let mut is_minus = false;
        match s[0] {
            b'+' | b'-' => {
                if s[0] == b'-' {
                    is_minus = true;
                }
                s = &s[1..];
            }
            _ => {}
        }
        let left;
        let mut right = "".as_bytes();
        let dot = s.iter().position(|b| *b == b'.');

        match dot {
            Some(index) => {
                left = &s[0..index];
                right = &s[index + 1..];
            }
            None => {
                left = s;
            }
        }

        if left.len() == 0 { return None } //"."ドットの左に何かないとここではdecimalと認めない。
        if dot.is_some() && right.len() == 0 { return None; } //ドットがある場合、その右に何かないと数値とは認めない
        //このライブラリは文字列を復元することを第一に考える。数値化は圧縮のために行う。圧縮の前後で文字列は変化してはならない。
        if left[0] == b'0' && left.len() != 1{ return None } //左に余計な0がついているものも数値と認めない。0.0とかなら認める。左に0が2個以上あると復元できない。


        fn utf8(s : &[u8]) -> &str{ unsafe{ ::std::str::from_utf8_unchecked(s) } }

        let all : String = (if is_minus{ "-" } else { "" }).to_string() + utf8(left) + utf8(right);
        let int : i64 = match all.parse(){
            Ok(i) => i,
            _ => return None,
        };
        //if int >= 2u64.pow(42){ return None }; //整数部は0-2^(6*7)-1までだが、それはここで判定することではないはずだ・・・
        let scale = right.len() as u32; //右のゼロは全て保持する

        return Some(Decimal{ int, scale });
    }
}