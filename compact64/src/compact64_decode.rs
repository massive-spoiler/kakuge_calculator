//use super::my_base64_item::MyBase64Item;
use super::get_base64_from_b6s::get_b6s_from_c64;
use super::get_items::get_items;

///decodeしたオブジェクトを返す。

pub fn decode_compact64(c64 : &[u8]) -> Result<Vec<String>, String>  {
    let b6s = get_b6s_from_c64(c64);
    match b6s{
        Some(vec) => return get_items(&vec),
        _=>{return Err("unknown error".to_string());}
    }
}
