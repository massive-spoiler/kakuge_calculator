///6ビットの列を8ビットの列に詰め直す。base64をバイト列に戻すために明らかに必要なメソッドである。
pub fn get_bytes_from_b6s(b6s : &[u8]) -> Vec<u8> {
    let bit_length = 6 * b6s.len();
    if bit_length == 0{ return vec![]; }

    ///vecのlastバイトのb8_indexのビットから、b6のb6_indexのbitからのデータを書き込む。
    fn set(b8s : &mut Vec<u8>, b8_index : &mut usize, mut b6 : u8, b6_index : &mut usize){
        let &last_byte = b8s.last().unwrap();
        b6 = b6 << (*b6_index + 2);
        b6 = b6 >> *b8_index;
        *b8s.last_mut().unwrap() = last_byte | b6;
        let next = *b8_index + (6 - *b6_index);
        if 8 < next{
            //全部書き込めなかった
            *b8_index = 8;
            let exceed = next - 8;
            *b6_index = 6 - exceed;
        }
        else{
            //全部書き込めた
            *b8_index = next;
            *b6_index = 6;
        }
    }

    //8bitをいくつ入れられるか。
    //最後の8bitを埋め終わった段階でも少し6bitの方は残る事が多いわけだが、残った部分は無視する必要がある。
    let capacity = bit_length / 8;
    let mut b8s: Vec<u8> = Vec::with_capacity(capacity);
    b8s.push(0);
    let mut b8_index = 0;
    let mut b6_index = 0;

    //左から埋めていくのは意外と難しいな・・・
    for &b6 in b6s {
        loop {
            if b8_index == 8 {
                if b8s.len() == capacity{
                    //全部埋め終わった
                    return b8s;
                }
                else {
                    b8s.push(0);
                    b8_index = 0;
                }
            }
            set(&mut b8s, &mut b8_index, b6, &mut b6_index);
            if b6_index == 6{
                b6_index = 0;
                break;
            }
        }
    }
    return b8s;
}

pub fn encode_b6s_from_bytes(bytes : &[u8]) -> Vec<u8>{

    fn set(b6s : &mut Vec<u8>, b6_index : &mut usize, mut b8 : u8, b8_index : &mut usize){
        let &last_byte = b6s.last().unwrap();
        let need = 8 - *b8_index;
        b8 = b8 << *b8_index;
        b8 = b8 >> *b6_index + 2;
        *b6s.last_mut().unwrap() = last_byte | b8;
        if *b6_index + need <= 6 {
            //全部書けている
            *b6_index = *b6_index + need;
            *b8_index = 8;
        }
        else {
            //全部はかけなかった
            let exceed = *b6_index + need - 6;
            *b8_index = 8 - exceed;
            *b6_index = 6;
        }
    }

    let mut b6s : Vec<u8> = vec![];
    b6s.push(0);
    let mut b6_index = 0;
    let mut b8_index = 0;

    for &b8 in bytes{
        loop{
            if b6_index == 6{
                b6s.push(0);
                b6_index = 0;
            }
            set(&mut b6s, &mut b6_index, b8, &mut b8_index);

            if b8_index == 8{
                b8_index = 0;
                break;
            }
        }
    }
    return b6s;
}


//pub fn test() {
//    let v: u8 = 0b11101100;
//
//    let vec = vec![v,v,v,v,v];
//    let result = encode_b6s_from_bytes(&vec);
//
//    for b6 in &result {
//        print!("{:>08b} ", b6);
//    }
//
//    println!();
//    println!();
//
//    let v8 = get_bytes_from_b6s(&result);
//    for b8 in &v8 {
//        print!("{:>08b} ", b8);
//    }
//}