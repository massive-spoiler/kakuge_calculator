use super::char_to_b6::{b6_to_char, char_to_b6};

pub fn get_c64_from_b6s(bytes : &[u8]) -> Option<Vec<u8>>{
    let mut s : Vec<u8> = vec![];
    for b in bytes{
        match b6_to_char(*b){
            Some(c) => s.push(c as u8),
            None => return None,
        }
    }
    return Some(s);
}

pub fn get_b6s_from_c64(bytes : &[u8]) -> Option<Vec<u8>>{
    if let Ok(s) = String::from_utf8(bytes.to_vec()) {
        let mut vec: Vec<u8> = vec![];
        for c in s.chars() {
            match char_to_b6(c) {
                Some(b) => vec.push(b),
                None => return None,
            }
        }
        return Some(vec);
    }
    return None;
}
