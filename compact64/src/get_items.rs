//use super::my_base64_item::MyBase64Item;
use super::get_single_item::*;

pub fn get_items(b6s : &[u8]) -> Result<Vec<String>, String>{
    let mut index = 0;
    let mut vec : Vec<String> = vec![];
    loop {
        match get_single_item(&b6s[index..]) {
            Some((item, size)) =>{ vec.push(String::from_utf8(item).unwrap()); index += size; }
            None =>{ return Err(format!("error index {}", index)); }
        }
        if b6s.len() <= index{ break; }
    }
    return Ok(vec);
}

//pub fn encode_items(items : &[*const u8]) -> Result<Vec<u8>, String>{
//    let mut r : Vec<u8> = vec![];
//    for item in items{
//        match item{
//            MyBase64Item::Str(s) =>{
//
//                let mut vec = encode_str(&s)?;
//                r.append(&mut vec);
//            },
//            MyBase64Item::Num(n) =>{
//                if let Some(mut vec) = encode_num(&n) {
//                    r.append(&mut vec);
//                }
//            }
//        }
//    }
//    return Ok(r);
//}