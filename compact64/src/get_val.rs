use super::get_u64_from_b6s::*;
use super::decimal::*;

///可変長の値をb6sから取得。lenは1-8で、最大48bitのデータから数値を得る。
///値は文字列で保持することになっているので、値の文字列表現と処理したb6数も返す。
///最大長は整数部6*7=42bit+10進の小数点以下桁数が5bit(使用ライブラリの都合上最大桁数は28)&符号1bit
pub fn get_val(b6s : &[u8]) -> Vec<u8>{
    return get_dec(b6s).to_string().into_bytes();
}

fn get_dec(b6s : &[u8]) -> Decimal{
    ///一番左がサインビットで、残りは数字。ネイティブ数値とは違うが、このほうが簡単だと思う・・・？
    fn get_int(bits : u64, sign_bit : usize) -> i64 {
        let sign_val = 1 << (sign_bit - 1);
        let is_minus = bits & sign_val != 0;
        let bits = bits & (!sign_val);

        let bits = bits as i64 * if is_minus { -1 } else { 1 };
        return bits;
    }

    ///一番右の5bitがスケールで、6bit目は符号。そこから整数
    fn get_general_dec(bits : u64) -> Decimal{
        let scale = bits & 0b11111;
        //println!("scale {}", scale);
        //println!("bits {:b}", bits);
        let is_minus = bits & 0b100000 != 0;

        let int = bits >> 6;
        return Decimal{ int : int as i64 * if is_minus{ -1 } else {1}, scale : scale as u32 };
    }

    let len = b6s.len();
    let bits = get_u64_from_b6s(b6s);
    return match len{
        1=> Decimal{ int: get_int(bits, 6), scale: 0}, //+-31
        2 => Decimal{int: get_int(bits, 12), scale: 0}, //+-2047
        3..=8 => get_general_dec(bits),
        //4 => Decimal::new(get_int(bits, 24), 2), //+-83886.07
        //5..=8 => get_general_dec(bits),
        _ => panic!("undefined get_fixed len {}", len),
    }
}

enum SizeAndValue{
    Fixed(usize, i64),
    Decimal(usize),
    None
}

fn get_fixed_size_and_value(int : i64, place : usize) -> SizeAndValue{
//    fn adjust_place(abs : i64, place : usize, dest : usize) -> i64{
//        if place < dest {
//            return abs * 10i64.pow((dest - place) as u32);
//        } else {return abs; }
//    }

//    fn append_sign(abs : i64, int : i64) -> i64{
//        if int < 0 { abs * -1 } else{ abs }
//    }
    let abs = int.abs();

    use self::SizeAndValue::*;
    if place == 0{
        //バイト長1,2に関しては整数を優先。3-8バイトは小数を認める
        if abs <= 31{ return Fixed(1, int); }
        if abs <= 2047{ return Fixed(2, int); }
    }
    for i in 3..=8{
        let limit : i64 = 2i64.pow(6*(i-1));
        if abs < limit{ return Decimal(i as usize); }
    }
    return None;
}

fn get_fixed_b6s(int : i64, size : usize) -> Vec<u8>{
    let mut abs = int.abs() as u64;
    let mut vec : Vec<u8> = vec![];
    for _ in 0..size{
        let bits = abs & 0b111111;
        vec.push(bits as u8);
        abs = abs >> 6;
    }
    if int< 0{
        *vec.last_mut().unwrap() = vec.last().unwrap() | 0b100000;
    }
    vec.reverse();
    return vec;
}

fn get_dec_b6s(int : i64, scale : u32, size : usize) -> Vec<u8>{
    let is_minus = int < 0;
    let mut abs = int.abs() as u64;
    let mut vec : Vec<u8> = vec![];
    for _ in 0..size-1{
        let bits = abs & 0b111111;
        vec.push(bits as u8);
        abs = abs >> 6;
    }
    vec.reverse();
    vec.push(if is_minus{ 0b100000 } else { 0 } | (scale as u8));
    return vec;
}

pub fn encode_dec(d : Decimal) -> Option<Vec<u8>> {
    use super::get_val::SizeAndValue::*;
    match get_fixed_size_and_value(d.int, d.scale as usize) {
        Fixed(size, int) => return Some(get_fixed_b6s(int, size)),
        Decimal(size) => return Some(get_dec_b6s(d.int, d.scale, size)),
        None => return Option::None,
    }
}

