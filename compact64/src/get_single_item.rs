use super::get_bytes_from_b6s::get_bytes_from_b6s;
use super::get_base64_from_b6s::*;
//use super::my_base64_item::MyBase64Item;
use super::get_val::get_val;
use super::get_u64_from_b6s::*;
use super::get_val::encode_dec;
use super::get_bytes_from_b6s::encode_b6s_from_bytes;

pub fn get_single_item(b6s : &[u8]) -> Option<(Vec<u8>, usize)>{
    fn bit_is_up(b6 : u8, n : usize) -> bool{
        let b6 = b6 as usize;
        return (b6 & (1 << (n-1))) != 0;
    }

    fn b8_size_to_b6_size(b8size : usize) -> usize{
        f64::ceil ((b8size*8) as f64 / 6.0) as usize
    }

    let f = b6s[0];
    if bit_is_up(f, 6){
        //0-31*6bitの情報量があるutf8文字列を表す
        let size = (f & 0b11111) as usize;
        let size = b8_size_to_b6_size(size);
        let bytes = get_bytes_from_b6s(&b6s[1..=size]);
        return Some((bytes, 1+size));
    }
    if bit_is_up(f,5){
        let size  = ((f & 0b1111) + 1) as usize; //1-16文字のBase64文字列

        return match get_c64_from_b6s(&b6s[1..=size]){
            Some(s) => Some((s, 1+size as usize)),
            None => None,
        }
    }
    if bit_is_up(f,4){
        let size = ((f & 0b111) + 1) as usize; //1-8*6bitの可変長数値
        let s = get_val(&b6s[1..=size]);
        return Some((s, 1 + size));
    }
    if bit_is_up(f,3) {
        let len_size = ((f & 0b11) + 1) as usize; //1-4*6bitで表される数値を長さNとして、6*Nbitで表されるUTF8文字列。最大1677万*6bitの長さとなる
        let len = get_u64_from_b6s(&b6s[1..=len_size]) as usize;
        let start = len_size + 1;
        let txt_b6s = &b6s[start..start + len];
        let txt_b8s = get_bytes_from_b6s(txt_b6s);

        return Some((txt_b8s, start + len));
    }
    let value = (f & 0b11) as i32 - 1; //-1から2までの数値を表す
    return Some((value.to_string().into_bytes(), 1))
}

///数値文字列として解釈可能なら数値として、それ以外は任意のバイト列としてエンコード
pub fn encode_single_item(b8s : &[u8]) -> Result<Vec<u8>, String>{
    let vec = b8s.to_vec();
    match String::from_utf8(vec){
        Ok(s) =>{
            match encode_num(&s){
                Ok(v) => return Ok(v),
                _ =>{ }
            }
        },
        Err(_) =>{}
    }
    return encode_str(b8s);
}

pub fn encode_str(s : &[u8]) -> Result<Vec<u8>,String>{
    if s.len() == 0{
        return Ok(vec![0b100000]);
    }
    if s.len() <= 16{
        if let Some(mut vec) = get_b6s_from_c64(s){
            vec.insert(0, 0b10000 | (s.len() - 1) as u8);
            return Ok(vec);
        }
    }
    if s.len() <= 31{
        let mut vec = encode_b6s_from_bytes(s);
        vec.insert(0,0b100000 | s.len() as u8);
        return Ok(vec);
    }

    let mut vec = encode_b6s_from_bytes(s);
    let mut len_b6s = encode_u64_to_b6s(vec.len() as u64);
    let len = len_b6s.len();
    if 5 <= len{
        return Err(format!("length is too long : len {}", s.len()));
    }

    len_b6s.insert(0, 0b100 | (len - 1) as u8);
    len_b6s.append(&mut vec);

    return Ok(len_b6s);
}

pub fn encode_num(val : &str) -> Result<Vec<u8>, String>{
    use super::decimal::*;
    //use rust_decimal::Decimal;
    //use std::str::FromStr;
    //use num_traits::ToPrimitive;

    if let Some(d) = Decimal::from_str(&val) {

        if d.scale == 0 {
            if [-1, 0, 1, 2].iter().any(|f| *f == d.int) {
                return Ok(vec![(d.int + 1) as u8]);
            }
        }

        if let Some(mut vec) = encode_dec(d) {
            let len = vec.len();
            vec.insert(0, 0b1000 | ((len - 1) as u8));
            //println!("is num {}", val);
            return Ok(vec);
        }
    }
    return Err(format!("{} was not able to be interpreted as a number", val));
}