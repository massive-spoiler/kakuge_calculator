//
//#[no_mangle]
//pub extern "C" fn alloc(len : usize) -> *mut u8{
//    let mut vec = Vec::<u8>::with_capacity(len);
//    unsafe { vec.set_len(len); }
//    Box::into_raw(vec.into_boxed_slice()) as *mut u8
//}
//
//#[no_mangle]
//pub extern "C" fn free(raw: *mut u8, len : usize) {
//    unsafe {
//        let s = ::std::slice::from_raw_parts_mut(raw, len);
//        Box::from_raw(s);
//    }
//}
//
//pub struct StringBuilder{
//    s : String
//}
//
//#[no_mangle]
//pub extern "C" fn create_string_builder() -> *mut StringBuilder{
//    let b = Box::new(StringBuilder{ s : "".to_string() });
//    Box::into_raw(b)
//}
//
//#[no_mangle]
//pub extern "C" fn create_string_builder_with_capacity(size : i32) -> *mut StringBuilder{
//    let b = Box::new(StringBuilder{ s : String::with_capacity(size as usize) });
//    Box::into_raw(b)
//}
//
//#[no_mangle]
//pub extern "C" fn destroy_string_builder(b : *mut StringBuilder){
//    unsafe{ Box::from_raw(b); }
//}
//
//#[no_mangle]
//pub extern "C" fn string_builder_append(sb : *mut StringBuilder, code_point : i32) -> i32{
//    match ::std::char::from_u32(code_point as u32) {
//        Some(c) => {
//            unsafe { (*sb).s.push(c) };
//            return 1;
//        }
//        _ => return 0,
//    }
//}
//
//#[no_mangle]
//pub extern "C" fn string_builder_append_unchecked(sb : *mut StringBuilder, code_point : i32){
//    unsafe { (*sb).s.push(::std::char::from_u32_unchecked(code_point as u32)); }
//}
//
//#[no_mangle]
//pub extern "C" fn test_with_string_builder(sb : *mut StringBuilder) -> i32 {
//    unsafe{ test_method(&(*sb).s) }
//}
//
//#[no_mangle]
//pub extern "C" fn test_with_utf16(utf16 : *mut u16, len : i32) -> i32 {
//    unsafe {
//        let slice =  ::std::slice::from_raw_parts(utf16, len as usize);
//        match String::from_utf16(slice) {
//            Ok(s) => test_method(&s),
//            _ => -1,
//        }
//    }
//}
//
//#[no_mangle]
//pub extern "C" fn test_with_utf16_lossy(utf16 : *const u16, len : i32) -> i32 {
//    unsafe{
//        let slice = ::std::slice::from_raw_parts(utf16, len as usize);
//        test_method(&String::from_utf16_lossy(slice))
//    }
//}
//
//#[no_mangle]
//pub extern "C" fn test_with_utf8_unchecked(utf8 : *const u8, len : i32) -> i32 {
//    unsafe{
//        let slice = ::std::slice::from_raw_parts(utf8, len as usize);
//        test_method(&String::from_utf8_unchecked(slice.to_vec()))
//    }
//}
//
//#[no_mangle]
//pub extern "C" fn test_with_utf8(utf8 : *const u8, len : i32) -> i32 {
//    unsafe{
//        let slice = ::std::slice::from_raw_parts(utf8, len as usize);
//        match String::from_utf8(slice.to_vec()){
//            Ok(s) => test_method(&s),
//            _ => -1
//        }
//    }
//}
//
//fn test_method(s : &str) -> i32{
//    let mut result = 0;
//    for c in s.chars(){
//        result += c as i32;
//    }
//    return result;
//}
//
//pub struct FullAsmInput{
//    array : Vec<Vec<u16>>
//}
//
//#[no_mangle]
//pub extern "C" fn create_full_asm_input() -> *const FullAsmInput {
//    let ss = ["ABCDE", "あああああ",
//        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ",
//        "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん",
//        "😃😁😂",
//        "😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂😃😁😂"];
//
//    let vec : Vec<Vec<u16>> = ss.iter().map(|s| s.encode_utf16().collect()).collect();
//    let b = Box::new(FullAsmInput{ array : vec });
//    return Box::into_raw(b);
//}
//
//#[no_mangle]
//pub extern "C" fn full_webasm(inp : *const FullAsmInput, index : i32, loop_count : i32) -> i32 {
//    unsafe{
//        let mut r = 0;
//        for _ in 0..loop_count {
//            let utf16 : &Vec<u16> = &(*inp).array[index as usize];
//            let s = String::from_utf16_lossy(utf16);
//            r += test_method(&s);
//        }
//        return r;
//    }
//}
//
////extern "C" {
////    fn timer_start();
////    fn timer_stop();
////}
//
////#[no_mangle]
////pub extern "C" fn test_full_asm_utf16() -> i32{
////    let top_loop = 3;
////    let super_top_loop = 10;
////    let inner_loop = 10000;
////
////    let mut result_holder = 0;
////    let inp = create_full_asm_input();
////    for _ in 0..super_top_loop{
////        unsafe {
////            for index in 0..(*inp).array.len() {
////                for _ in 0..top_loop {
////                    timer_start();
////                    let r = full_webasm(inp, index as i32, inner_loop);
////                    timer_stop();
////                    result_holder += r;
////                }
////            }
////        }
////    }
////    return result_holder;
////}
//
//
//pub fn test_native_utf16(){
//    let top_loop = 3;
//    let super_top_loop = 3;
//    let inner_loop = 10000;
//
//    let mut result_holder = 0;
//    let inp = create_full_asm_input();
//    for _ in 0..super_top_loop{
//        unsafe {
//            for index in 0..(*inp).array.len() {
//                for _ in 0..top_loop {
//                    let start = ::std::time::Instant::now();
//                    let r = full_webasm(inp, index as i32, inner_loop);
//                    let end = start.elapsed();
//                    let s = String::from_utf16_lossy(&(*inp).array[index]);
//                    println!("{}ms {} {}", end.subsec_millis(), s, r);
//                    result_holder += r;
//                }
//            }
//        }
//    }
//    println!("Done {}", result_holder);
//}
//
//pub fn test_native_sb(){
//    let top_loop = 3;
//    let super_top_loop = 3;
//    let inner_loop = 10000;
//
//    let mut result_holder = 0;
//    let inp = create_full_asm_input();
//
//    for _ in 0..super_top_loop{
//        unsafe {
//            for index in 0..(*inp).array.len() {
//                for _ in 0..top_loop {
//                    let start = ::std::time::Instant::now();
//                    for _ in 0..inner_loop{
//                        let s = String::from_utf16_lossy(&(*inp).array[index]);
//                        let sb = create_string_builder_with_capacity((s.len() * 3) as i32);
//                        for c in s.chars() {
//                            string_builder_append_unchecked(sb, c as i32);
//                        }
//                        let r = test_method(&(*sb).s);
//                        result_holder += r;
//                        destroy_string_builder(sb);
//                    }
//                    let end = start.elapsed();
//                    let s = String::from_utf16_lossy(&(*inp).array[index]);
//                    println!("{}ms {} {}", end.subsec_millis(), s, result_holder);
//                }
//            }
//        }
//    }
//    println!("Done {}", result_holder);
//}