
pub fn char_to_b6(c : char) -> Option<u8>{
    fn sub(l : char, r : char) -> u8{
        return l as u8 - r as u8;
    }
    return match c {
        'A'..='Z' => Some(sub(c,'A')),
        'a'..='z' => Some(sub(c, 'a') + 26),
        '0'..='9' => Some(sub( c, '0') + 52),
        '-' => Some(62),
        '_' => Some(63),
        _ => None
    }
}

pub fn b6_to_char(b : u8) -> Option<char>{
    fn add(l : char, diff : usize) -> char{
        return (l as usize + diff) as u8 as char;
        //((l as usize) + diff) as char
    }
    let b = b as usize;
    return match b{
        0..=25 => Some(add('A', b)),
        26..=51 => Some(add('a', b - 26)),
        52..=61 => Some(add('0', b - 52)),
        62 => Some('-'),
        63 => Some('_'),
        _ => None,
    }
}