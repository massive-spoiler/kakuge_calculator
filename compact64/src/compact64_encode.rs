
///複数のUTF8文字列をまとめてURLで表現可能な6bit文字列に変換する。10進数の文字列だった場合と、
/// a-zA-Z0-9-_だけで構成されていた場合、特別扱いして可変長数値または6bit文字列として保持することで容量を節約する
pub fn encode_utf8s_into_compact64(array : &[String]) -> Result<String,String> {
    use super::get_single_item::encode_single_item;
    use super::get_base64_from_b6s::get_c64_from_b6s;

    let mut r: Vec<u8> = vec![];
    for inp in array {
        match encode_single_item(inp.as_bytes()) {
            Ok(mut v) =>{
                r.append(&mut v)
            },
            Err(_) => {},
        }
    }
    match get_c64_from_b6s(&r) {
        Some(vec) => match String::from_utf8(vec){
            Ok(s) => return Ok(s),
            Err(_) => return Err("unknown error".to_string()),
        }
        None => { return Err(format!("unknown error")) }
    }
}

