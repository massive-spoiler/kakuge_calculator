///6bitを取得しては左シフトしていく
pub fn get_u64_from_b6s(data : &[u8]) -> u64{
    let mut val : u64 = 0;
    for b in data{
        val = val << 6;
        val = val | (*b as u64);
    }
    return val;
}

pub fn encode_u64_to_b6s(mut num : u64) -> Vec<u8>{
    let mut vec : Vec<u8> = vec![];
    while num != 0{
        vec.push((num & 0b111111) as u8);
        num = num >> 6;
    }
    vec.reverse();
    return vec;
}